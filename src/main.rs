#![feature(is_some_and)]
#![feature(type_changing_struct_update)]
#![allow(unused_imports)]
#![allow(unused)]

mod auth;
mod compose;
mod error;
mod exchange;
mod global;
mod logger;
mod model;
mod mono_store;
mod route;
mod service;
mod store;

use std::{
    io::Result,
    net::{Ipv4Addr, SocketAddr, SocketAddrV4},
    path::Path,
    sync::{Arc, Mutex},
    time::Duration,
};

use async_redis_session::RedisSessionStore;
use auth::oauth::Google;
use axum::{
    error_handling::HandleErrorLayer,
    extract::DefaultBodyLimit,
    response::IntoResponse,
    routing::{delete, get, post, put},
    Router,
};
use axum_client_ip::SecureClientIpSource;
use axum_extra::extract::cookie;
use axum_sessions::SessionLayer;
use http::StatusCode;
use ring::rand::{SecureRandom, SystemRandom};
use tower::{
    buffer::BufferLayer,
    limit::RateLimitLayer,
    load_shed::error::Overloaded,
    timeout::{self, error::Elapsed, TimeoutLayer},
    BoxError, ServiceBuilder,
};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use model::mono_store::MonoStore;
use store::Postgres;

use crate::{
    auth::oauth::{FacebookLayer, GoogleLayer, MicrosoftLayer, TwitterLayer},
    compose::{error::ComposeError, requisite::RequisiteLayer, Entry, VerifyLayer},
    global::SESSION_COOKIE_NAME,
    model::state_service::{MonoStateSyncService, SyncService},
};

#[tokio::main]
async fn main() -> Result<()> {
    logger::init();

    let redis_conn_str = format!(
        "redis://:{}@127.0.0.1:6379",
        std::env::var("REDIS_PASSWORD").expect("REDIS_PASSWORD not set")
    );

    let session_store = RedisSessionStore::new(redis_conn_str).unwrap();

    let session_secret = {
        let mut session_secret = [0u8; 128];
        let secure_random = SystemRandom::new();
        secure_random.fill(&mut session_secret);

        session_secret
    };

    let requisite_key = {
        let mut requisite_key = [0u8; 64];
        let secure_random = SystemRandom::new();
        secure_random.fill(&mut requisite_key);

        requisite_key
    };

    let config_path = Path::new("config.toml");

    let store = Postgres::new(config_path).unwrap();

    let app = Router::new()
        .route("/", get(route::index))
        .nest(
            "/login",
            Router::new()
                .route(
                    "/google",
                    get(route::login::oauth::google).with_state(MonoStateSyncService::new(
                        cookie::Key::from(&requisite_key),
                        ServiceBuilder::new()
                            .layer(GoogleLayer::new(config_path).unwrap())
                            .service(Entry::mono_store(store.clone())),
                    )),
                )
                .route(
                    "/facebook",
                    get(route::login::oauth::facebook).with_state(MonoStateSyncService::new(
                        cookie::Key::from(&requisite_key),
                        ServiceBuilder::new()
                            .layer(FacebookLayer::new(config_path).unwrap())
                            .service(Entry::mono_store(store.clone())),
                    )),
                )
                .route(
                    "/twitter",
                    get(route::login::oauth::twitter).with_state(MonoStateSyncService::new(
                        cookie::Key::from(&requisite_key),
                        ServiceBuilder::new()
                            .layer(TwitterLayer::new(config_path).unwrap())
                            .service(Entry::mono_store(store.clone())),
                    )),
                )
                .route(
                    "/microsoft",
                    get(route::login::oauth::microsoft).with_state(MonoStateSyncService::new(
                        cookie::Key::from(&requisite_key),
                        ServiceBuilder::new()
                            .layer(MicrosoftLayer::new(config_path).unwrap())
                            .service(Entry::mono_store(store.clone())),
                    )),
                ),
        )
        .nest(
            "/api/v1",
            Router::new()
                .nest(
                    "/entity",
                    Router::new()
                        .route(
                            "/fetch",
                            post(route::entity::fetch).with_state(SyncService::new(
                                service::entity::Fetch::mono_store(store.clone()),
                            )),
                        )
                        .route(
                            "/find",
                            post(route::entity::find).with_state(SyncService::new(
                                service::entity::Find::mono_store(store.clone()),
                            )),
                        )
                        .route(
                            "/update",
                            put(route::entity::update).with_state(SyncService::new(
                                service::entity::Update::mono_store(store.clone()),
                            )),
                        )
                        .route(
                            "/delete",
                            delete(route::entity::delete).with_state(SyncService::new(
                                service::entity::Delete::mono_store(store.clone()),
                            )),
                        ),
                )
                .nest(
                    "/team",
                    Router::new()
                        .route(
                            "/create",
                            post(route::team::create).with_state(SyncService::new(
                                service::team::Create::mono_store(store.clone()),
                            )),
                        )
                        .route(
                            "/fetch",
                            get(route::team::fetch).with_state(SyncService::new(
                                service::team::Fetch::mono_store(store.clone()),
                            )),
                        )
                        .route(
                            "/find",
                            get(route::team::find).with_state(SyncService::new(
                                service::team::Find::mono_store(store.clone()),
                            )),
                        )
                        .route(
                            "/update",
                            put(route::team::update).with_state(SyncService::new(
                                service::team::Update::mono_store(store.clone()),
                            )),
                        )
                        .route(
                            "/delete",
                            delete(route::team::delete).with_state(SyncService::new(
                                service::team::Delete::mono_store(store.clone()),
                            )),
                        ),
                )
                .nest(
                    "/document",
                    Router::new()
                        .route(
                            "/create",
                            post(route::document::create).with_state(SyncService::new(
                                service::document::Create::mono_store(store.clone()),
                            )),
                        )
                        .route(
                            "/fetch",
                            get(route::document::fetch).with_state(SyncService::new(
                                service::document::Fetch::mono_store(store.clone()),
                            )),
                        )
                        .route(
                            "/find",
                            get(route::document::find).with_state(SyncService::new(
                                service::document::Find::mono_store(store.clone()),
                            )),
                        )
                        .route(
                            "/update",
                            put(route::document::update).with_state(SyncService::new(
                                service::document::Update::mono_store(store.clone()),
                            )),
                        )
                        .route(
                            "/delete",
                            delete(route::document::delete).with_state(SyncService::new(
                                service::document::Delete::mono_store(store.clone()),
                            )),
                        ),
                )
                .layer(
                    ServiceBuilder::new()
                        .layer(HandleErrorLayer::new(|error: BoxError| async move {
                            (
                                StatusCode::INTERNAL_SERVER_ERROR,
                                format!("Unhandled internal - {}", error),
                            )
                        }))
                        .layer(VerifyLayer::mono_store(store.clone())),
                ),
        )
        .layer(
            ServiceBuilder::new()
                .layer(HandleErrorLayer::new(|error: BoxError| async move {
                    match error {
                        err if err.is::<Elapsed>() => (
                            StatusCode::REQUEST_TIMEOUT,
                            format!("Request timeout - {}", err),
                        ),
                        err if err.is::<Overloaded>() => (
                            StatusCode::TOO_MANY_REQUESTS,
                            format!("Too many requests - {}", err),
                        ),
                        err => (
                            StatusCode::INTERNAL_SERVER_ERROR,
                            format!("Unhandled internal - {}", err),
                        ),
                    }
                }))
                .layer(DefaultBodyLimit::max(1024))
                .layer(BufferLayer::new(64))
                .layer(RateLimitLayer::new(3, Duration::from_secs(1)))
                .layer(TimeoutLayer::new(Duration::from_secs(10)))
                .layer(SecureClientIpSource::ConnectInfo.into_extension())
                .layer(
                    SessionLayer::new(session_store, &session_secret)
                        .with_cookie_name(SESSION_COOKIE_NAME)
                        .with_same_site_policy(cookie::SameSite::Lax),
                )
                .layer(RequisiteLayer::new(cookie::Key::from(&requisite_key))),
        );

    let addr = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), 8080));

    tracing::debug!("listening on {}", &addr);

    axum::Server::bind(&addr)
        .serve(app.into_make_service_with_connect_info::<SocketAddr>())
        .await
        .unwrap();

    Ok(())
}
