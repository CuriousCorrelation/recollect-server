use tower::util::BoxCloneService;

use crate::{
    compose::entry::via_document_measure::ViaDocumentMeasureLayer, model::mono_store::MonoStore,
    store::Postgres,
};

impl MonoStore<Postgres> for ViaDocumentMeasureLayer {
    fn mono_store(store: Postgres) -> Self {
        ViaDocumentMeasureLayer::builder()
            .measure_assure(BoxCloneService::new(store.clone()))
            .measure_create(BoxCloneService::new(store))
            .build()
    }
}
