use tower::util::BoxCloneService;

use crate::{
    compose::entry::via_member::ViaMemberLayer, model::mono_store::MonoStore, store::Postgres,
};

impl MonoStore<Postgres> for ViaMemberLayer {
    fn mono_store(store: Postgres) -> Self {
        ViaMemberLayer::builder()
            .member_assure(BoxCloneService::new(store.clone()))
            .member_create(BoxCloneService::new(store))
            .build()
    }
}
