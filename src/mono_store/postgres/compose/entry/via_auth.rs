use tower::util::BoxCloneService;

use crate::{
    compose::entry::via_auth::ViaAuthLayer, model::mono_store::MonoStore, store::Postgres,
};

impl MonoStore<Postgres> for ViaAuthLayer {
    fn mono_store(store: Postgres) -> Self {
        ViaAuthLayer::builder()
            .auth_assure(BoxCloneService::new(store.clone()))
            .auth_create(BoxCloneService::new(store.clone()))
            .token_create(BoxCloneService::new(store))
            .build()
    }
}
