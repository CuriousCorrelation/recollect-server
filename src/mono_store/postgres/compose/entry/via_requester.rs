use tower::util::BoxCloneService;

use crate::{
    compose::entry::via_requester::ViaRequesterLayer, model::mono_store::MonoStore, store::Postgres,
};

impl MonoStore<Postgres> for ViaRequesterLayer {
    fn mono_store(store: Postgres) -> Self {
        ViaRequesterLayer::builder()
            .requester_assure(BoxCloneService::new(store.clone()))
            .requester_create(BoxCloneService::new(store))
            .build()
    }
}
