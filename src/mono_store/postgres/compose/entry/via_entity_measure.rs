use tower::util::BoxCloneService;

use crate::{
    compose::entry::via_entity_measure::ViaEntityMeasureLayer, model::mono_store::MonoStore,
    store::Postgres,
};

impl MonoStore<Postgres> for ViaEntityMeasureLayer {
    fn mono_store(store: Postgres) -> Self {
        ViaEntityMeasureLayer::builder()
            .measure_assure(BoxCloneService::new(store.clone()))
            .measure_create(BoxCloneService::new(store))
            .build()
    }
}
