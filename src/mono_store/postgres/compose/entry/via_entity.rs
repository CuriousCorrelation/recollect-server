use tower::util::BoxCloneService;

use crate::{
    compose::entry::{via_entity::ViaEntityLayer, via_id_handle::ViaIdHandle},
    model::mono_store::MonoStore,
    store::Postgres,
};

impl MonoStore<Postgres> for ViaEntityLayer {
    fn mono_store(store: Postgres) -> Self {
        ViaEntityLayer::builder()
            .entity_fetch(BoxCloneService::new(store.clone()))
            .via_id_handle(
                ViaIdHandle::builder()
                    .object_create(BoxCloneService::new(store.clone()))
                    .namespace_create(BoxCloneService::new(store.clone()))
                    .build(),
            )
            .entity_create(BoxCloneService::new(store))
            .build()
    }
}
