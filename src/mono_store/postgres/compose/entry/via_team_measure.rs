use tower::util::BoxCloneService;

use crate::{
    compose::entry::via_team_measure::ViaTeamMeasureLayer, model::mono_store::MonoStore,
    store::Postgres,
};

impl MonoStore<Postgres> for ViaTeamMeasureLayer {
    fn mono_store(store: Postgres) -> Self {
        ViaTeamMeasureLayer::builder()
            .measure_assure(BoxCloneService::new(store.clone()))
            .measure_create(BoxCloneService::new(store))
            .build()
    }
}
