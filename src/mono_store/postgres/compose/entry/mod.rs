// ServiceBuilder::new()
//    .buffer(64)
//    .concurrency_limit(64)
//    .load_shed()
//    .rate_limit(64, Duration::from_secs(1))
//    .service(store.clone())

mod via_auth;
mod via_document_measure;
mod via_entity;
mod via_entity_measure;
mod via_member;
mod via_requester;
mod via_session;
mod via_team;
mod via_team_measure;

use tower::ServiceBuilder;

use crate::{
    compose::{
        entry::{
            via_auth::ViaAuthLayer, via_document_measure::ViaDocumentMeasureLayer,
            via_entity::ViaEntityLayer, via_entity_measure::ViaEntityMeasureLayer,
            via_member::ViaMemberLayer, via_requester::ViaRequesterLayer, via_session::ViaSession,
            via_team::ViaTeamLayer, via_team_measure::ViaTeamMeasureLayer,
        },
        Entry,
    },
    model::mono_store::MonoStore,
    store::Postgres,
};

impl MonoStore<Postgres> for Entry {
    fn mono_store(store: Postgres) -> Self {
        let inner = ServiceBuilder::new()
            .layer(ViaAuthLayer::mono_store(store.clone()))
            .layer(ViaEntityLayer::mono_store(store.clone()))
            .layer(ViaTeamLayer::mono_store(store.clone()))
            .layer(ViaMemberLayer::mono_store(store.clone()))
            .layer(ViaRequesterLayer::mono_store(store.clone()))
            .layer(ViaEntityMeasureLayer::mono_store(store.clone()))
            .layer(ViaTeamMeasureLayer::mono_store(store.clone()))
            .layer(ViaDocumentMeasureLayer::mono_store(store.clone()))
            .service(ViaSession::mono_store(store));

        Self::builder().inner(inner).build()
    }
}
