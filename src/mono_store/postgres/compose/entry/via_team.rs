use tower::util::BoxCloneService;

use crate::{
    compose::entry::{via_id_handle::ViaIdHandle, via_team::ViaTeamLayer},
    model::mono_store::MonoStore,
    store::Postgres,
};

impl MonoStore<Postgres> for ViaTeamLayer {
    fn mono_store(store: Postgres) -> Self {
        ViaTeamLayer::builder()
            .preferred_team_fetch(BoxCloneService::new(store.clone()))
            .via_id_handle(
                ViaIdHandle::builder()
                    .object_create(BoxCloneService::new(store.clone()))
                    .namespace_create(BoxCloneService::new(store.clone()))
                    .build(),
            )
            .team_create(BoxCloneService::new(store))
            .build()
    }
}
