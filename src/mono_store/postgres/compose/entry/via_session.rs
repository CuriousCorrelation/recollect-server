use tower::util::BoxCloneService;

use crate::{
    compose::entry::via_session::ViaSession, model::mono_store::MonoStore, store::Postgres,
};

impl MonoStore<Postgres> for ViaSession {
    fn mono_store(store: Postgres) -> Self {
        ViaSession::builder()
            .session_create(BoxCloneService::new(store))
            .build()
    }
}
