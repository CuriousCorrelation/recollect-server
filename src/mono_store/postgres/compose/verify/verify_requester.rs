use tower::util::BoxCloneService;

use crate::{
    compose::verify::verify_requester::VerifyRequesterLayer, model::mono_store::MonoStore,
    store::Postgres,
};

impl MonoStore<Postgres> for VerifyRequesterLayer {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .requester_fetch(BoxCloneService::new(store))
            .build()
    }
}
