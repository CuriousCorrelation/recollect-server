use tower::util::BoxCloneService;

use crate::{
    compose::verify::verify_who::VerifyWho, model::mono_store::MonoStore, store::Postgres,
};

impl MonoStore<Postgres> for VerifyWho {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .preferred_team_fetch(BoxCloneService::new(store))
            .build()
    }
}
