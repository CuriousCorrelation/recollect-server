use tower::util::BoxCloneService;

use crate::{
    compose::verify::verify_session::VerifySessionLayer, model::mono_store::MonoStore,
    store::Postgres,
};

impl MonoStore<Postgres> for VerifySessionLayer {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .session_fetch(BoxCloneService::new(store))
            .build()
    }
}
