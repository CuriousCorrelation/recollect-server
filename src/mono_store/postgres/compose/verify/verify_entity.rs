use tower::util::BoxCloneService;

use crate::{
    compose::verify::verify_entity::VerifyEntityLayer, model::mono_store::MonoStore,
    store::Postgres,
};

impl MonoStore<Postgres> for VerifyEntityLayer {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .entity_assure(BoxCloneService::new(store))
            .build()
    }
}
