use tower::ServiceBuilder;

use crate::{
    compose::{
        verify::{
            verify_entity::VerifyEntityLayer, verify_requester::VerifyRequesterLayer,
            verify_session::VerifySessionLayer, verify_who::VerifyWho, VerifyLayer,
        },
        Verify,
    },
    model::mono_store::MonoStore,
    store::Postgres,
};

mod verify_entity;
mod verify_requester;
mod verify_session;
mod verify_who;

impl MonoStore<Postgres> for VerifyLayer {
    fn mono_store(store: Postgres) -> Self {
        let verify = ServiceBuilder::new()
            .layer(VerifySessionLayer::mono_store(store.clone()))
            .layer(VerifyRequesterLayer::mono_store(store.clone()))
            .layer(VerifyEntityLayer::mono_store(store.clone()))
            .service(VerifyWho::mono_store(store));

        Self::builder().verify(verify).build()
    }
}
