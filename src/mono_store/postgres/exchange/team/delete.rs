use tower::{util::BoxCloneService, ServiceBuilder};

use crate::{exchange, model::mono_store::MonoStore, service::audit, store::Postgres};

impl MonoStore<Postgres> for audit::Create<exchange::team::Delete<Postgres>> {
    fn mono_store(store: Postgres) -> Self {
        ServiceBuilder::new()
            .layer(audit::AuditLayer::mono_store(store.clone()))
            .layer(exchange::team::DeleteLayer::mono_store(store.clone()))
            .service(store)
    }
}

impl MonoStore<Postgres> for exchange::team::DeleteLayer {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .team_assure(BoxCloneService::new(store.clone()))
            .admin_assure(BoxCloneService::new(store))
            .build()
    }
}
