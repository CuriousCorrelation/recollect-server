use tower::{util::BoxCloneService, ServiceBuilder};

use crate::{exchange, model::mono_store::MonoStore, service::audit, store::Postgres};

impl MonoStore<Postgres> for audit::Create<exchange::team::Create<Postgres>> {
    fn mono_store(store: Postgres) -> Self {
        ServiceBuilder::new()
            .layer(audit::AuditLayer::mono_store(store.clone()))
            .layer(exchange::team::CreateLayer::mono_store(store.clone()))
            .service(store)
    }
}

impl MonoStore<Postgres> for exchange::team::CreateLayer {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .object_create(BoxCloneService::new(store.clone()))
            .namespace_create(BoxCloneService::new(store.clone()))
            .measure_fetch(BoxCloneService::new(store.clone()))
            .measure_update(BoxCloneService::new(store))
            .build()
    }
}
