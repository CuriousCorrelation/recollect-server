use tower::ServiceBuilder;

use crate::{exchange, model::mono_store::MonoStore, service::audit, store::Postgres};

impl MonoStore<Postgres> for audit::Create<exchange::document::Find<Postgres>> {
    fn mono_store(store: Postgres) -> Self {
        ServiceBuilder::new()
            .layer(audit::AuditLayer::mono_store(store.clone()))
            .layer(exchange::document::FindLayer::mono_store(store.clone()))
            .service(store)
    }
}

impl MonoStore<Postgres> for exchange::document::FindLayer {
    fn mono_store(store: Postgres) -> Self {
        Self::builder().build()
    }
}
