use tower::{util::BoxCloneService, ServiceBuilder};

use crate::{exchange, model::mono_store::MonoStore, service::audit, store::Postgres};

impl MonoStore<Postgres> for audit::Create<exchange::document::Update<Postgres>> {
    fn mono_store(store: Postgres) -> Self {
        ServiceBuilder::new()
            .layer(audit::AuditLayer::mono_store(store.clone()))
            .layer(exchange::document::UpdateLayer::mono_store(store.clone()))
            .service(store)
    }
}

impl MonoStore<Postgres> for exchange::document::UpdateLayer {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .document_assure(BoxCloneService::new(store.clone()))
            .author_assure(BoxCloneService::new(store.clone()))
            .connection_assure(BoxCloneService::new(store))
            .build()
    }
}
