use tower::{util::BoxCloneService, ServiceBuilder};

use crate::{exchange, model::mono_store::MonoStore, service::audit, store::Postgres};

impl MonoStore<Postgres> for audit::Create<exchange::entity::Fetch<Postgres>> {
    fn mono_store(store: Postgres) -> Self {
        ServiceBuilder::new()
            .layer(audit::AuditLayer::mono_store(store.clone()))
            .layer(exchange::entity::FetchLayer::mono_store(store.clone()))
            .service(store)
    }
}

impl MonoStore<Postgres> for exchange::entity::FetchLayer {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .entity_assure(BoxCloneService::new(store.clone()))
            .admin_assure(BoxCloneService::new(store.clone()))
            .member_assure(BoxCloneService::new(store.clone()))
            .visibility_assure(BoxCloneService::new(store.clone()))
            .connection_assure(BoxCloneService::new(store))
            .build()
    }
}
