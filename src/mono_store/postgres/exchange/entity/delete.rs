use tower::{util::BoxCloneService, ServiceBuilder};

use crate::{exchange, model::mono_store::MonoStore, service::audit, store::Postgres};

impl MonoStore<Postgres> for audit::Create<exchange::entity::Delete<Postgres>> {
    fn mono_store(store: Postgres) -> Self {
        ServiceBuilder::new()
            .layer(audit::AuditLayer::mono_store(store.clone()))
            .layer(exchange::entity::DeleteLayer::mono_store(store.clone()))
            .service(store)
    }
}

impl MonoStore<Postgres> for exchange::entity::DeleteLayer {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .entity_assure(BoxCloneService::new(store.clone()))
            .admin_count(BoxCloneService::new(store.clone()))
            .admin_find(BoxCloneService::new(store))
            .build()
    }
}
