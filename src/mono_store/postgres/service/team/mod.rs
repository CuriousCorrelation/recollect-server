pub(crate) mod create;
pub(crate) mod delete;
pub(crate) mod fetch;
pub(crate) mod find;
pub(crate) mod update;

use crate::{exchange::team::Exchange, model::mono_store::MonoStore, store::Postgres};

impl MonoStore<Postgres> for Exchange {
    fn mono_store(_store: Postgres) -> Self {
        Self::builder().build()
    }
}
