use crate::{
    exchange::team,
    model::mono_store::MonoStore,
    service::{audit, team::Fetch},
    store::Postgres,
};

impl MonoStore<Postgres> for Fetch {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .fetch(audit::Create::<team::Fetch<Postgres>>::mono_store(
                store.clone(),
            ))
            .exchange(team::Exchange::mono_store(store))
            .build()
    }
}
