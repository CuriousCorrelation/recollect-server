use crate::{
    exchange::team,
    model::mono_store::MonoStore,
    service::{audit, team::Update},
    store::Postgres,
};

impl MonoStore<Postgres> for Update {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .update(audit::Create::<team::Update<Postgres>>::mono_store(
                store.clone(),
            ))
            .exchange(team::Exchange::mono_store(store))
            .build()
    }
}
