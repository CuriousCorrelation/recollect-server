use crate::{
    exchange::team,
    model::mono_store::MonoStore,
    service::{audit, team::Find},
    store::Postgres,
};

impl MonoStore<Postgres> for Find {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .find(audit::Create::<team::Find<Postgres>>::mono_store(
                store.clone(),
            ))
            .exchange(team::Exchange::mono_store(store))
            .build()
    }
}
