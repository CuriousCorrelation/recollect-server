use crate::{
    exchange::team,
    model::mono_store::MonoStore,
    service::{audit, team::Create},
    store::Postgres,
};

impl MonoStore<Postgres> for Create {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .create(audit::Create::<team::Create<Postgres>>::mono_store(
                store.clone(),
            ))
            .exchange(team::Exchange::mono_store(store))
            .build()
    }
}
