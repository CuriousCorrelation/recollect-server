use crate::{
    exchange::team,
    model::mono_store::MonoStore,
    service::{audit, team::Delete},
    store::Postgres,
};

impl MonoStore<Postgres> for Delete {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .delete(audit::Create::<team::Delete<Postgres>>::mono_store(
                store.clone(),
            ))
            .exchange(team::Exchange::mono_store(store))
            .build()
    }
}
