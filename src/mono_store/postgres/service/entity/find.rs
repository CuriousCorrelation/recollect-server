use crate::{
    exchange,
    model::mono_store::MonoStore,
    service::{self, audit},
    store::Postgres,
};

impl MonoStore<Postgres> for service::entity::Find {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .find(audit::Create::<exchange::entity::Find<Postgres>>::mono_store(store.clone()))
            .exchange(exchange::entity::Exchange::mono_store(store))
            .build()
    }
}
