use crate::{
    exchange::entity,
    model::mono_store::MonoStore,
    service::{audit, entity::Update},
    store::Postgres,
};

impl MonoStore<Postgres> for Update {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .update(audit::Create::<entity::Update<Postgres>>::mono_store(
                store.clone(),
            ))
            .exchange(entity::Exchange::mono_store(store))
            .build()
    }
}
