use crate::{
    exchange::entity,
    model::mono_store::MonoStore,
    service::{audit, entity::Fetch},
    store::Postgres,
};

impl MonoStore<Postgres> for Fetch {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .fetch(audit::Create::<entity::Fetch<Postgres>>::mono_store(
                store.clone(),
            ))
            .exchange(entity::Exchange::mono_store(store))
            .build()
    }
}
