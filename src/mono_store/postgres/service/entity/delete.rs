use crate::{
    exchange::entity,
    model::mono_store::MonoStore,
    service::{audit, entity::Delete},
    store::Postgres,
};

impl MonoStore<Postgres> for Delete {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .delete(audit::Create::<entity::Delete<Postgres>>::mono_store(
                store.clone(),
            ))
            .exchange(entity::Exchange::mono_store(store))
            .build()
    }
}
