use tower::util::BoxCloneService;

use crate::{
    model::mono_store::MonoStore,
    service::audit::{AuditLayer, Create},
    store::Postgres,
};

impl MonoStore<Postgres> for AuditLayer {
    fn mono_store(store: Postgres) -> Self {
        AuditLayer::builder()
            .audit_create(BoxCloneService::new(store))
            .build()
    }
}
