use crate::{
    exchange::document,
    model::mono_store::MonoStore,
    service::{audit, document::Create},
    store::Postgres,
};

impl MonoStore<Postgres> for Create {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .create(audit::Create::<document::Create<Postgres>>::mono_store(
                store.clone(),
            ))
            .exchange(document::Exchange::mono_store(store))
            .build()
    }
}
