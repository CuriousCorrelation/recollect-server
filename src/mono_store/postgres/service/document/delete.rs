use crate::{
    exchange::document,
    model::mono_store::MonoStore,
    service::{audit, document::Delete},
    store::Postgres,
};

impl MonoStore<Postgres> for Delete {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .delete(audit::Create::<document::Delete<Postgres>>::mono_store(
                store.clone(),
            ))
            .exchange(document::Exchange::mono_store(store))
            .build()
    }
}
