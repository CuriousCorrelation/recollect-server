use crate::{
    exchange::document,
    model::mono_store::MonoStore,
    service::{audit, document::Update},
    store::Postgres,
};

impl MonoStore<Postgres> for Update {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .update(audit::Create::<document::Update<Postgres>>::mono_store(
                store.clone(),
            ))
            .exchange(document::Exchange::mono_store(store))
            .build()
    }
}
