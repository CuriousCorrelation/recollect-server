use crate::{
    exchange::document,
    model::mono_store::MonoStore,
    service::{audit, document::Find},
    store::Postgres,
};

impl MonoStore<Postgres> for Find {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .find(audit::Create::<document::Find<Postgres>>::mono_store(
                store.clone(),
            ))
            .exchange(document::Exchange::mono_store(store))
            .build()
    }
}
