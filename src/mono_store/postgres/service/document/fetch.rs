use crate::{
    exchange::document,
    model::mono_store::MonoStore,
    service::{audit, document::Fetch},
    store::Postgres,
};

impl MonoStore<Postgres> for Fetch {
    fn mono_store(store: Postgres) -> Self {
        Self::builder()
            .fetch(audit::Create::<document::Fetch<Postgres>>::mono_store(
                store.clone(),
            ))
            .exchange(document::Exchange::mono_store(store))
            .build()
    }
}
