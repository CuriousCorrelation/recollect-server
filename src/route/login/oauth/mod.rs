use std::sync::{Arc, Mutex};

use axum::{
    extract::{Query, State},
    response::{IntoResponse, Redirect},
    Extension, Json,
};
use axum_extra::extract::{
    cookie::{Cookie, Key},
    PrivateCookieJar,
};
use axum_sessions::{
    extractors::{ReadableSession, WritableSession},
    SessionHandle,
};
use oauth2::{CsrfToken, PkceCodeVerifier};
use time::Duration;
use tower::ServiceExt;
use tracing::info;

use crate::{
    auth::oauth::Google,
    compose::Entry,
    error::ServerError,
    global::COOKIE_NAME,
    model::{
        auth::AuthResponse,
        oauth::{OAuthRequest, SecurityTokens},
        requisite::EmptyRequisite,
        state_service::MonoStateSyncService,
        Secret,
    },
    route::error::RouteError,
};

#[axum_macros::debug_handler]
pub(crate) async fn google(
    mut session: WritableSession,
    jar: PrivateCookieJar,
    Extension(requisite): Extension<EmptyRequisite>,
    State(oauth): State<MonoStateSyncService<Key, Google<Entry>>>,
    Query(security_tokens): Query<SecurityTokens>,
) -> Result<(PrivateCookieJar, Redirect), ServerError> {
    info!(requisite = ?requisite);

    match jar.get(COOKIE_NAME) {
        Some(session_id) => {
            info!(session_id = ?session_id, "`session_id` found in client cookie");

            info!("Redirecting to \"/\"");

            Ok((jar, Redirect::to("/")))
        }
        None => {
            if let SecurityTokens {
                state: Some(csrf_token),
                code: Some(code),
            } = &security_tokens
            {
                info!(security_tokens = ?security_tokens, "`security_tokens` found in uri query");

                let pkce_verifier = {
                    let pkce_verifier = session
                        .get::<PkceCodeVerifier>(csrf_token.secret())
                        .ok_or(RouteError::SessionPkceCode)?;

                    session.remove(csrf_token.secret());

                    Ok::<_, RouteError>(pkce_verifier)
                }?;

                info!(pkce_verifier = ?pkce_verifier, "`pkce_verifier` found in session store");

                let session = oauth
                    .service()
                    .oneshot(requisite.with_pre(OAuthRequest {
                        security_tokens,
                        pkce_verifier,
                    }))
                    .await
                    .unwrap();

                let session_id = session.id.to_string();

                info!(session_id = ?session_id, "`session_id` retrieved");

                let cookie = Cookie::build(COOKIE_NAME, session_id)
                    .secure(true)
                    .max_age(Duration::hours(24))
                    .finish();

                info!("New cookie generated");

                info!("Redirecting to \"/\"");

                Ok((jar.add(cookie), Redirect::to("/")))
            } else {
                let oauth_svc = oauth.service();

                let (auth_request, pkce_verifier) = oauth_svc.authorization_request();
                let (url, csrf_token) = auth_request.url();

                session
                    .insert(csrf_token.secret(), pkce_verifier)
                    .map_err(RouteError::InsertSessionPkceCode)?;

                info!(url = ?url, "Redirecting to");

                Ok((jar, Redirect::temporary(url.as_str())))
            }
        }
    }
}

#[axum_macros::debug_handler]
pub(crate) async fn facebook() -> impl IntoResponse {
    todo!()
}

#[axum_macros::debug_handler]
pub(crate) async fn twitter() -> impl IntoResponse {
    todo!()
}

#[axum_macros::debug_handler]
pub(crate) async fn microsoft() -> impl IntoResponse {
    todo!()
}
