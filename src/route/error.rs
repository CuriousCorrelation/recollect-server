use thiserror::Error;

use crate::{auth, compose, exchange, service};

#[derive(Debug, Error)]
pub(crate) enum RouteError {
    #[error("Exchange - {0}")]
    Exchange(#[from] exchange::error::ExchangeError),

    #[error("Service - {0}")]
    Service(#[from] service::error::ServiceError),

    #[error("Compose - {0}")]
    Compose(#[from] compose::error::ComposeError),

    #[error("Unable to insert PKCE code to session - {0}")]
    InsertSessionPkceCode(serde_json::Error),

    #[error("Unable to find PKCE code in session")]
    SessionPkceCode,

    #[error("Unable to set identity for session")]
    SessionIdentity,

    #[error("OAuth - {0}")]
    OAuth(#[from] auth::oauth::error::OAuthError),
}
