use std::sync::{Arc, Mutex};

use axum::{extract::State, response::IntoResponse, Extension, Json};
use http::StatusCode;
use tower::{Service, ServiceExt};

use crate::{
    compose::Verify,
    error::ServerError,
    model::{
        external::team::{Exclusive, Identify, Inclusive, Partial, Team},
        state_service::SyncService,
        who::Who,
        Request,
    },
    service::team::{Create, Delete, Fetch, Find, Update},
};

#[axum_macros::debug_handler]
pub(crate) async fn create(
    Extension(who): Extension<Who>,
    State(create_svc): State<SyncService<Create>>,
    Json(request): Json<Inclusive>,
) -> Result<Json<Team>, ServerError> {
    let response = create_svc
        .service()
        .oneshot(Request { who, request })
        .await
        .map_err(ServerError::Service)?;

    Ok(Json(response))
}

#[axum_macros::debug_handler]
pub(crate) async fn fetch(
    Extension(who): Extension<Who>,
    State(fetch_svc): State<SyncService<Fetch>>,
    Json(request): Json<Identify>,
) -> Result<Json<Team>, ServerError> {
    let response = fetch_svc
        .service()
        .oneshot(Request { who, request })
        .await
        .map_err(ServerError::Service)?;

    Ok(Json(response))
}

#[axum_macros::debug_handler]
pub(crate) async fn find(
    Extension(who): Extension<Who>,
    State(find_svc): State<SyncService<Find>>,
    Json(request): Json<Partial>,
) -> Result<Json<Vec<Team>>, ServerError> {
    let response = find_svc
        .service()
        .oneshot(Request { who, request })
        .await
        .map_err(ServerError::Service)?;

    Ok(Json(response))
}

#[axum_macros::debug_handler]
pub(crate) async fn update(
    Extension(who): Extension<Who>,
    State(update_svc): State<SyncService<Update>>,
    Json(request): Json<Exclusive>,
) -> Result<Json<Team>, ServerError> {
    let response = update_svc
        .service()
        .oneshot(Request { who, request })
        .await
        .map_err(ServerError::Service)?;

    Ok(Json(response))
}

#[axum_macros::debug_handler]
pub(crate) async fn delete(
    Extension(who): Extension<Who>,
    State(delete_svc): State<SyncService<Delete>>,
    Json(request): Json<Identify>,
) -> Result<Json<Team>, ServerError> {
    let response = delete_svc
        .service()
        .oneshot(Request { who, request })
        .await
        .map_err(ServerError::Service)?;

    Ok(Json(response))
}
