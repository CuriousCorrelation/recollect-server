use axum::response::IntoResponse;
use axum_sessions::extractors::ReadableSession;

pub(crate) mod document;
pub(crate) mod entity;
pub(crate) mod error;
pub(crate) mod login;
pub(crate) mod member;
pub(crate) mod team;
pub(crate) mod whoami;

pub(crate) async fn index(session: Option<ReadableSession>) -> impl IntoResponse {
    match session {
        Some(session) => format!("Hey {:#?}! You're logged in!", session.get::<String>("id")),
        None => "You're not logged in.\nVisit `/login` to do so.".to_string(),
    }
}
