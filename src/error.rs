use axum::response::{IntoResponse, Response};
use http::StatusCode;
use serde::Serialize;
use thiserror::Error;

use crate::{compose, route, service};

#[derive(Debug, Error)]
pub(crate) enum ServerError {
    #[error("Route - {0}")]
    Route(#[from] route::error::RouteError),

    #[error("Compose - {0}")]
    Compose(#[from] compose::error::ComposeError),

    #[error("Service - {0}")]
    Service(#[from] service::error::ServiceError),
}

impl IntoResponse for ServerError {
    fn into_response(self) -> Response {
        (
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("Something went wrong: {}", self),
        )
            .into_response()
    }
}

#[derive(Debug, Serialize, Error)]
pub(crate) enum AccessViolation {
    #[error("Unauthorized access")]
    Unauthorized,

    #[error("Requester is not the target of this `connection`")]
    Target,

    #[error("Requester is not a member of this `team`")]
    Member,

    #[error("Requester is not an author of `document`")]
    Author,

    #[error("Requester is not authorized")]
    Compliant,
}
