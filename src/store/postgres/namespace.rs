use base64::{engine::general_purpose, Engine};
use diesel::{
    expression_methods::{BoolExpressionMethods, ExpressionMethods, PgTextExpressionMethods},
    query_dsl::{JoinOnDsl, QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use ring::rand::{SecureRandom, SystemRandom};
use std::task::{ready, Context, Poll};
use tower::Service;

use crate::{
    model::{
        self,
        label::Label,
        namespace::{Exclusive, Identify, Inclusive, Namespace, Partial},
        ops,
        scope::Scope,
        who::Who,
        BoxFuture, Request,
    },
    store::{
        error::StoreError,
        schema::{
            ascribe, author, connection, document, entity, member, namespace, team, visibility,
        },
    },
};

use super::Postgres;

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<Identify>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Document(handle) => Identify::Document(
                        namespace::table
                            .filter(namespace::handle.eq(handle))
                            .select(namespace::handle)
                            .get_result(conn)?,
                    ),
                    Identify::Entity(handle) => Identify::Entity(
                        namespace::table
                            .filter(namespace::handle.eq(handle))
                            .select(namespace::handle)
                            .get_result(conn)?,
                    ),
                    Identify::Team(handle) => Identify::Team(
                        namespace::table
                            .filter(namespace::handle.eq(handle))
                            .select(namespace::handle)
                            .get_result(conn)?,
                    ),
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Namespace>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request: _ } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                // Generate `handle` inside `transaction`
                // to avoid invoking functions if `connection` cannot be fetched.
                let mut dest = vec![0; 16];

                let secure_random = SystemRandom::new();

                secure_random
                    .fill(&mut dest)
                    .map_err(|_| StoreError::Crypto)?;

                let mut handle = general_purpose::URL_SAFE.encode(dest);

                handle.truncate(16);

                let record = diesel::insert_into(namespace::table)
                    .values(namespace::handle.eq(handle))
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Namespace>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let handle = match request {
                    Identify::Document(handle) => handle,
                    Identify::Entity(handle) => handle,
                    Identify::Team(handle) => handle,
                };

                let record = namespace::table
                    .filter(namespace::handle.eq(handle))
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<Namespace>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who { entity_id, team_id } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                if let Some(composite_handle) = &request.handle {
                    match composite_handle {
                        model::namespace::Identify::Document(handle) => {
                            let mut query = namespace::table
                                .left_join(
                                    document::table.on(document::handle.eq(namespace::handle)),
                                )
                                .left_join(
                                    connection::table.on(document::id.eq(connection::destinate)),
                                )
                                .left_join(
                                    visibility::table.on(document::id.eq(visibility::object)),
                                )
                                .left_join(ascribe::table.on(document::id.eq(ascribe::document_id)))
                                .left_join(author::table.on(document::id.eq(author::document_id)))
                                .filter(document::deleted_at.is_null())
                                .filter(
                                    // Among documents where `entity_id` is the author,
                                    author::entity_id
                                        .eq(&entity_id)
                                        // scope is `Public`,
                                        .or(visibility::label
                                            .eq(Label::Document)
                                            .and(visibility::scope.eq(Scope::Public)))
                                        // connection originating from `entity_id`,
                                        .or(connection::originate_label
                                            .eq(Label::Entity)
                                            .and(connection::originate.eq(&entity_id))
                                            .and(connection::destinate_label.eq(Label::Document)))
                                        // scope is `TeamOnly` and team is `team_id`.
                                        .or(ascribe::team_id.eq(&team_id).and(
                                            visibility::label
                                                .eq(Label::Document)
                                                .and(visibility::scope.eq(Scope::TeamOnly)),
                                        )),
                                )
                                .into_boxed();

                            query = query.filter(namespace::handle.ilike(format!("%{}%", handle)));

                            if let Some(created_at) = request.created_at {
                                query = query.filter(namespace::created_at.eq(created_at));
                            }

                            if let Some(updated_at) = request.updated_at {
                                query = query.filter(namespace::updated_at.eq(updated_at));
                            }

                            let records = query
                                .select((
                                    namespace::handle,
                                    namespace::created_at,
                                    namespace::updated_at,
                                ))
                                .get_results(conn)?;

                            Ok::<_, StoreError>(records)
                        }
                        model::namespace::Identify::Entity(handle) => {
                            let mut query = namespace::table
                                .left_join(entity::table.on(entity::handle.eq(namespace::handle)))
                                .left_join(
                                    connection::table.on(entity::id.eq(connection::destinate)),
                                )
                                .left_join(visibility::table.on(entity::id.eq(visibility::object)))
                                .left_join(member::table.on(entity::id.eq(member::entity_id)))
                                .filter(entity::deleted_at.is_null())
                                .filter(
                                    // Among self
                                    entity::id
                                        .eq(entity_id)
                                        // scope is `TeamOnly` and team is `team_id`.
                                        .or(member::team_id.eq(&team_id).and(
                                            visibility::label
                                                .eq(Label::Entity)
                                                .and(visibility::scope.eq(Scope::TeamOnly)),
                                        ))
                                        .or(
                                            // or entities where scope is `Public`,
                                            visibility::label
                                                .eq(Label::Entity)
                                                .and(visibility::scope.eq(Scope::Public)),
                                        )
                                        // connection originating from `entity_id`
                                        .or(connection::originate_label
                                            .eq(Label::Entity)
                                            .and(connection::originate.eq(&entity_id))
                                            .and(connection::destinate_label.eq(Label::Entity))),
                                )
                                .into_boxed();

                            query = query.filter(namespace::handle.ilike(format!("%{}%", handle)));

                            if let Some(created_at) = request.created_at {
                                query = query.filter(namespace::created_at.eq(created_at));
                            }

                            if let Some(updated_at) = request.updated_at {
                                query = query.filter(namespace::updated_at.eq(updated_at));
                            }

                            let records = query
                                .select((
                                    namespace::handle,
                                    namespace::created_at,
                                    namespace::updated_at,
                                ))
                                .get_results(conn)?;

                            Ok::<_, StoreError>(records)
                        }
                        model::namespace::Identify::Team(handle) => {
                            let mut query = namespace::table
                                .left_join(team::table.on(team::handle.eq(namespace::handle)))
                                .left_join(connection::table.on(team::id.eq(connection::destinate)))
                                .left_join(visibility::table.on(team::id.eq(visibility::object)))
                                .left_join(member::table.on(team::id.eq(member::team_id)))
                                .filter(team::deleted_at.is_null())
                                .filter(
                                    // Among teams where `entity_id` is the member,
                                    member::entity_id
                                        .eq(&entity_id)
                                        // scope is `Public`,
                                        .or(visibility::label
                                            .eq(Label::Team)
                                            .and(visibility::scope.eq(Scope::Public)))
                                        // connection originating from `entity_id`.
                                        .or(connection::originate_label
                                            .eq(Label::Entity)
                                            .and(connection::originate.eq(&entity_id))
                                            .and(connection::destinate_label.eq(Label::Team))),
                                )
                                .into_boxed();

                            query = query.filter(namespace::handle.ilike(format!("%{}%", handle)));

                            if let Some(created_at) = request.created_at {
                                query = query.filter(namespace::created_at.eq(created_at));
                            }

                            if let Some(updated_at) = request.updated_at {
                                query = query.filter(namespace::updated_at.eq(updated_at));
                            }

                            let records = query
                                .select((
                                    namespace::handle,
                                    namespace::created_at,
                                    namespace::updated_at,
                                ))
                                .get_results(conn)?;

                            Ok::<_, StoreError>(records)
                        }
                    }
                } else {
                    let mut query = namespace::table.into_boxed();

                    if let Some(created_at) = request.created_at {
                        query = query.filter(namespace::created_at.eq(created_at));
                    }

                    if let Some(updated_at) = request.updated_at {
                        query = query.filter(namespace::updated_at.eq(updated_at));
                    }

                    let records = query
                        .select((
                            namespace::handle,
                            namespace::created_at,
                            namespace::updated_at,
                        ))
                        .get_results(conn)?;

                    Ok::<_, StoreError>(records)
                }
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Update<Request<Exclusive>>> for Postgres {
    type Response = Request<Namespace>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Update(req): ops::Update<Request<Exclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record =
                    diesel::update(namespace::table.filter(namespace::handle.eq(request.id)))
                        .set(namespace::handle.eq(request.handle))
                        .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Namespace>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let handle = match request {
                    Identify::Document(handle) => handle,
                    Identify::Entity(handle) => handle,
                    Identify::Team(handle) => handle,
                };

                let record = diesel::delete(namespace::table.find(handle)).get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
