use std::task::{ready, Context, Poll};

use diesel::{
    expression_methods::{BoolExpressionMethods, ExpressionMethods},
    query_dsl::{JoinOnDsl, QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use tower::Service;

use crate::{
    model::{
        author::Author,
        author::{Identify, Inclusive, Partial},
        label::Label,
        ops,
        scope::Scope,
        who::Who,
        BoxFuture, PrimaryKey, Request,
    },
    store::{
        error::StoreError,
        schema::{ascribe, author, connection, document, visibility},
    },
};

use super::Postgres;

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<PrimaryKey>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => author::table
                        .filter(author::id.eq(id))
                        .select(author::id)
                        .get_result(conn)?,

                    Identify::Unique {
                        document_id,
                        entity_id,
                    } => author::table
                        .filter(author::document_id.eq(document_id))
                        .filter(author::entity_id.eq(entity_id))
                        .select(author::id)
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Author>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = diesel::insert_into(author::table)
                    .values(request)
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Author>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => author::table.filter(author::id.eq(id)).get_result(conn)?,

                    Identify::Unique {
                        document_id,
                        entity_id,
                    } => author::table
                        .filter(author::document_id.eq(document_id))
                        .filter(author::entity_id.eq(entity_id))
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<Author>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who { entity_id, team_id } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let mut query = author::table
                    .inner_join(document::table.on(document::id.eq(author::document_id)))
                    .inner_join(visibility::table.on(visibility::object.eq(document::id)))
                    .inner_join(ascribe::table.on(ascribe::document_id.eq(document::id)))
                    .left_join(connection::table.on(connection::destinate.eq(document::id)))
                    .filter(document::deleted_at.is_null())
                    .filter(
                        // Among documents where `entity_id` is the author,
                        author::entity_id
                            .eq(entity_id)
                            // scope is `Public`,
                            .or(visibility::label
                                .eq(Label::Document)
                                .and(visibility::scope.eq(Scope::Public)))
                            // connection originating from `entity_id`,
                            .or(connection::originate_label
                                .eq(Label::Entity)
                                .and(connection::originate.eq(entity_id))
                                .and(connection::destinate_label.eq(Label::Document)))
                            // scope is `TeamOnly` and team is `team_id`.
                            .or(ascribe::team_id.eq(team_id).and(
                                visibility::label
                                    .eq(Label::Document)
                                    .and(visibility::scope.eq(Scope::TeamOnly)),
                            )),
                    )
                    .into_boxed();

                if let Some(document_id) = request.document_id {
                    query = query.filter(author::document_id.eq(document_id));
                }

                if let Some(entity_id) = request.entity_id {
                    query = query.filter(author::entity_id.eq(entity_id));
                }

                if let Some(created_at) = request.created_at {
                    query = query.filter(author::created_at.eq(created_at));
                }

                let records = query
                    .select((
                        author::id,
                        author::document_id,
                        author::entity_id,
                        author::created_at,
                    ))
                    .get_results(conn)?;

                Ok::<_, StoreError>(records)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Author>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => diesel::delete(author::table.find(id)).get_result(conn)?,

                    Identify::Unique {
                        document_id,
                        entity_id,
                    } => diesel::delete(
                        author::table
                            .filter(author::document_id.eq(document_id))
                            .filter(author::entity_id.eq(entity_id)),
                    )
                    .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
