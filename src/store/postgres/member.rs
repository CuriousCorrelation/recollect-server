use diesel::{
    expression_methods::{BoolExpressionMethods, ExpressionMethods},
    query_dsl::{JoinOnDsl, QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use std::task::{ready, Context, Poll};
use tower::Service;

use crate::{
    model::{
        label::Label,
        member::{Identify, Inclusive, Member, Partial},
        ops,
        scope::Scope,
        who::Who,
        BoxFuture, PrimaryKey, Request,
    },
    store::{
        error::StoreError,
        schema::{connection, member, visibility},
    },
};

use super::Postgres;

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<PrimaryKey>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let member = self.get();

        async move {
            let request = member?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => member::table
                        .filter(member::id.eq(id))
                        .select(member::id)
                        .get_result(conn)?,

                    Identify::Unique { entity_id, team_id } => member::table
                        .filter(member::entity_id.eq(entity_id))
                        .filter(member::team_id.eq(team_id))
                        .select(member::id)
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Member>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let member = self.get();

        async move {
            let request = member?.transaction(|conn| {
                let record = diesel::insert_into(member::table)
                    .values(request)
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Member>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let member = self.get();

        async move {
            let request = member?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => member::table.filter(member::id.eq(id)).get_result(conn)?,

                    Identify::Unique { entity_id, team_id } => member::table
                        .filter(member::entity_id.eq(entity_id))
                        .filter(member::team_id.eq(team_id))
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<Member>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id,
            team_id: _,
        } = who;

        let member = self.get();

        async move {
            let request = member?.transaction(|conn| {
                let mut query = member::table
                    .inner_join(visibility::table.on(visibility::object.eq(member::team_id)))
                    .left_join(connection::table.on(connection::destinate.eq(member::team_id)))
                    .filter(
                        // `who` can search amongst
                        // 1. `team`s where `who.entity_id` is a `member`,
                        // 2. `team`s with `Public` `scope`,
                        // 3. `team`s with `connection` originating from `entity_id`,
                        member::entity_id
                            .eq(&entity_id)
                            .or(visibility::label
                                .eq(Label::Team)
                                .and(visibility::scope.eq(Scope::Public)))
                            .or(connection::originate_label
                                .eq(Label::Entity)
                                .and(connection::originate.eq(&entity_id))
                                .and(connection::destinate_label.eq(Label::Team))),
                    )
                    .into_boxed();

                if let Some(entity_id) = request.entity_id {
                    query = query.filter(member::entity_id.eq(entity_id));
                }

                if let Some(team_id) = request.team_id {
                    query = query.filter(member::team_id.eq(team_id));
                }

                if let Some(created_at) = request.created_at {
                    query = query.filter(member::created_at.eq(created_at));
                }

                let records = query
                    .select((
                        member::id,
                        member::entity_id,
                        member::team_id,
                        member::created_at,
                    ))
                    .get_results(conn)?;

                Ok::<_, StoreError>(records)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Member>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let member = self.get();

        async move {
            let request = member?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => diesel::delete(member::table.find(id)).get_result(conn)?,

                    Identify::Unique { entity_id, team_id } => diesel::delete(
                        member::table
                            .filter(member::entity_id.eq(entity_id))
                            .filter(member::team_id.eq(team_id)),
                    )
                    .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
