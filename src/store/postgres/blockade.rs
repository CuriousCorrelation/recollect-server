use std::{
    task::{ready, Context, Poll},
    time::SystemTime,
};

use chrono::{DateTime, Utc};
use diesel::{
    expression_methods::{BoolExpressionMethods, ExpressionMethods},
    query_dsl::{JoinOnDsl, QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use tower::Service;

use crate::{
    model::{
        blockade::Blockade,
        blockade::{Exclusive, Identify, Inclusive, Partial},
        ops,
        who::Who,
        BoxFuture, PrimaryKey, Request,
    },
    store::{
        error::StoreError,
        schema::{ascribe, author, blockade, document},
    },
};

use super::Postgres;

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<PrimaryKey>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let now: DateTime<Utc> = DateTime::from(
                    diesel::select(diesel::dsl::now).get_result::<SystemTime>(conn)?,
                );

                let record = match request {
                    Identify::Id(id) => blockade::table
                        .filter(blockade::expiry_at.gt(now))
                        .filter(blockade::id.eq(id))
                        .select(blockade::id)
                        .get_result(conn)?,

                    Identify::Unique {
                        originate_label,
                        originate,
                        action,
                        destinate_label,
                        destinate,
                    } => {
                        let mut query = blockade::table
                            .filter(blockade::expiry_at.gt(now))
                            .filter(blockade::originate_label.eq(originate_label))
                            .filter(blockade::originate.eq(originate))
                            .filter(blockade::destinate_label.eq(destinate_label))
                            .filter(blockade::destinate.eq(destinate))
                            .into_boxed();

                        if let Some(action) = action {
                            query = query.filter(blockade::action.eq(action));
                        }

                        query.select(blockade::id).get_result(conn)?
                    }
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Blockade>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = diesel::insert_into(blockade::table)
                    .values(request)
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Blockade>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let now: DateTime<Utc> = DateTime::from(
                    diesel::select(diesel::dsl::now).get_result::<SystemTime>(conn)?,
                );

                let record = match request {
                    Identify::Id(id) => blockade::table
                        .filter(blockade::expiry_at.gt(now))
                        .filter(blockade::id.eq(id))
                        .get_result(conn)?,

                    Identify::Unique {
                        originate_label,
                        originate,
                        action,
                        destinate_label,
                        destinate,
                    } => {
                        let mut query = blockade::table
                            .filter(blockade::expiry_at.gt(now))
                            .filter(blockade::originate_label.eq(originate_label))
                            .filter(blockade::originate.eq(originate))
                            .filter(blockade::destinate_label.eq(destinate_label))
                            .filter(blockade::destinate.eq(destinate))
                            .into_boxed();

                        if let Some(action) = action {
                            query = query.filter(blockade::action.eq(action));
                        }

                        query.get_result(conn)?
                    }
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<Blockade>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who { entity_id, team_id } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let now: DateTime<Utc> = DateTime::from(
                    diesel::select(diesel::dsl::now).get_result::<SystemTime>(conn)?,
                );

                let mut query = blockade::table
                    .left_join(
                        document::table.on(document::id
                            .eq(blockade::originate)
                            .or(document::id.eq(blockade::destinate))),
                    )
                    .left_join(author::table.on(author::document_id.eq(document::id)))
                    .left_join(ascribe::table.on(ascribe::document_id.eq(document::id)))
                    .filter(blockade::expiry_at.gt(now))
                    .filter(
                        // The `originate` or `destinate` of this `blockade`
                        // is either `entity_id`
                        blockade::originate
                            .eq(entity_id)
                            .or(blockade::destinate.eq(entity_id))
                            // or `team_id`
                            .or(blockade::originate.eq(team_id))
                            .or(blockade::destinate.eq(team_id))
                            // or `document` where either `entity_id` is the `author`
                            .or(author::entity_id.eq(entity_id))
                            // or `document` is `ascribe`d to `team_id`
                            .or(ascribe::team_id.eq(team_id)),
                    )
                    .into_boxed();

                if let Some(originate_label) = request.originate_label {
                    query = query.filter(blockade::originate_label.eq(originate_label));
                }

                if let Some(originate) = request.originate {
                    query = query.filter(blockade::originate.eq(originate));
                }

                if let Some(action) = request.action {
                    query = query.filter(blockade::action.eq(action));
                }

                if let Some(destinate_label) = request.destinate_label {
                    query = query.filter(blockade::destinate_label.eq(destinate_label));
                }

                if let Some(destinate) = request.destinate {
                    query = query.filter(blockade::destinate.eq(destinate));
                }

                if let Some(expiry_at) = request.expiry_at {
                    query = query.filter(blockade::expiry_at.eq(expiry_at));
                }

                let query = query.select((
                    blockade::id,
                    blockade::originate_label,
                    blockade::originate,
                    blockade::action,
                    blockade::destinate_label,
                    blockade::destinate,
                    blockade::expiry_at,
                    blockade::created_at,
                ));

                tracing::debug!(
                    "query: {:#?}",
                    diesel::debug_query::<diesel::pg::Pg, _>(&query)
                );

                let records = query.get_results(conn)?;

                tracing::debug!("records: {:#?}", &records);

                Ok::<_, StoreError>(records)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Update<Request<Exclusive>>> for Postgres {
    type Response = Request<Blockade>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Update(req): ops::Update<Request<Exclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = diesel::update(blockade::table.filter(blockade::id.eq(request.id)))
                    .set(request)
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Blockade>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => diesel::delete(blockade::table.filter(blockade::id.eq(id)))
                        .get_result(conn)?,

                    Identify::Unique {
                        originate_label,
                        originate,
                        action,
                        destinate_label,
                        destinate,
                    } => {
                        let mut query = diesel::delete(blockade::table)
                            .filter(blockade::originate_label.eq(originate_label))
                            .filter(blockade::originate.eq(originate))
                            .filter(blockade::destinate_label.eq(destinate_label))
                            .filter(blockade::destinate.eq(destinate))
                            .into_boxed();

                        if let Some(action) = action {
                            query = query.filter(blockade::action.eq(action));
                        }

                        query.get_result(conn)?
                    }
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
