use diesel::{
    expression_methods::ExpressionMethods,
    query_dsl::{QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use std::task::{ready, Context, Poll};
use tower::Service;

use crate::{
    model::{
        ops,
        requester::{Identify, Inclusive, Partial, Requester},
        who::Who,
        BoxFuture, PrimaryKey, Request,
    },
    store::{error::StoreError, schema::requester},
};

use super::Postgres;

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<PrimaryKey>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let requester = self.get();

        async move {
            let request = requester?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => requester::table
                        .filter(requester::id.eq(id))
                        .select(requester::id)
                        .get_result(conn)?,

                    Identify::Secret(secret) => requester::table
                        .filter(requester::secret.eq(secret))
                        .select(requester::id)
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Requester>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let requester = self.get();

        async move {
            let request = requester?.transaction(|conn| {
                let record = diesel::insert_into(requester::table)
                    .values(request)
                    .returning((
                        requester::id,
                        requester::secret,
                        requester::host,
                        requester::scheme,
                        requester::remote_addr,
                    ))
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Requester>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let requester = self.get();

        async move {
            let request = requester?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => requester::table
                        .filter(requester::id.eq(id))
                        .select((
                            requester::id,
                            requester::secret,
                            requester::host,
                            requester::scheme,
                            requester::remote_addr,
                        ))
                        .get_result(conn)?,

                    Identify::Secret(secret) => requester::table
                        .filter(requester::secret.eq(secret))
                        .select((
                            requester::id,
                            requester::secret,
                            requester::host,
                            requester::scheme,
                            requester::remote_addr,
                        ))
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<Requester>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let requester = self.get();

        async move {
            let request = requester?.transaction(|conn| {
                let mut query = requester::table.into_boxed();

                if let Some(secret) = &request.secret {
                    query = query.filter(requester::secret.eq(secret));
                }

                if let Some(host) = &request.host {
                    query = query.filter(requester::host.eq(host));
                }

                if let Some(scheme) = &request.scheme {
                    query = query.filter(requester::scheme.eq(scheme));
                }

                if let Some(remote_addr) = &request.remote_addr {
                    query = query.filter(requester::remote_addr.eq(remote_addr));
                }

                let records = query
                    .select((
                        requester::id,
                        requester::secret,
                        requester::host,
                        requester::scheme,
                        requester::remote_addr,
                    ))
                    .get_results(conn)?;

                Ok::<_, StoreError>(records)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Requester>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let requester = self.get();

        async move {
            let request = requester?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => {
                        diesel::delete(requester::table.filter(requester::id.eq(id)))
                            .returning((
                                requester::id,
                                requester::secret,
                                requester::host,
                                requester::scheme,
                                requester::remote_addr,
                            ))
                            .get_result(conn)?
                    }

                    Identify::Secret(secret) => {
                        diesel::delete(requester::table.filter(requester::secret.eq(secret)))
                            .returning((
                                requester::id,
                                requester::secret,
                                requester::host,
                                requester::scheme,
                                requester::remote_addr,
                            ))
                            .get_result(conn)?
                    }
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
