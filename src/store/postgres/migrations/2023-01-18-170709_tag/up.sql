CREATE TABLE
  IF NOT EXISTS tag (
    id BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    document_id BIGINT NOT NULL REFERENCES document(id) ON UPDATE CASCADE ON DELETE CASCADE,
    content TEXT NOT NULL,
    UNIQUE(document_id, content),
    created_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now(),
      updated_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now()
  );

SELECT
  diesel_manage_updated_at('tag');
