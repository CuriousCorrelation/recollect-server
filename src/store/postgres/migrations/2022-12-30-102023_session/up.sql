CREATE TABLE
  IF NOT EXISTS session (
    id VARCHAR(22) PRIMARY KEY,
    secret VARCHAR(22) NOT NULL REFERENCES token(secret) ON UPDATE CASCADE ON DELETE CASCADE,
    UNIQUE(id, secret),
    UNIQUE(id, secret, created_at),
    fetched_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now(),
      created_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now()
  );
