CREATE TABLE
  IF NOT EXISTS entity (
    id BIGINT UNIQUE NOT NULL PRIMARY KEY REFERENCES object(id) ON UPDATE CASCADE ON DELETE CASCADE,
    handle VARCHAR(16) UNIQUE NOT NULL REFERENCES namespace(handle) ON UPDATE CASCADE ON DELETE CASCADE,
    secret VARCHAR(22) UNIQUE NOT NULL REFERENCES token(secret) ON UPDATE CASCADE ON DELETE CASCADE,
    name VARCHAR(64) NOT NULL,
    created_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now(),
      updated_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now(),
      deleted_at TIMESTAMP
    WITH
      TIME ZONE DEFAULT NULL
  );

SELECT
  diesel_manage_updated_at('entity');

CREATE INDEX entity_search_index ON entity USING gin (name gin_trgm_ops);
