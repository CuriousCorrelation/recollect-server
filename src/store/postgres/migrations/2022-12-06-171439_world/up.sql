CREATE TABLE
  IF NOT EXISTS world (
    id BIGINT UNIQUE NOT NULL PRIMARY KEY REFERENCES object(id) ON UPDATE CASCADE ON DELETE CASCADE,
    name VARCHAR(22) UNIQUE NOT NULL,
    created_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now()
  );
