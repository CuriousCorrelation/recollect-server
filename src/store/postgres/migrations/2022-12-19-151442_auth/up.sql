CREATE TABLE
  IF NOT EXISTS auth (
    secret VARCHAR(22) PRIMARY KEY REFERENCES token(secret),
    id TEXT NOT NULL,
    name TEXT NOT NULL,
    email TEXT UNIQUE NOT NULL,
    picture TEXT,
    provider provider NOT NULL,
    created_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now(),
      updated_at TIMESTAMP
    WITH
      TIME ZONE DEFAULT now(),
      deleted_at TIMESTAMP
    WITH
      TIME ZONE DEFAULT NULL
  );

SELECT
  diesel_manage_updated_at('auth');
