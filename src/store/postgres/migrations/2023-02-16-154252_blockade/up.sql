CREATE TABLE
  IF NOT EXISTS blockade (
    id BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    originate_label label NOT NULL,
    originate BIGINT NOT NULL REFERENCES object(id) ON UPDATE CASCADE ON DELETE CASCADE,
    action action NOT NULL,
    destinate_label label NOT NULL,
    destinate BIGINT NOT NULL REFERENCES object(id) ON UPDATE CASCADE ON DELETE CASCADE,
    UNIQUE(originate, destinate),
    expiry_at TIMESTAMP
    WITH
      TIME ZONE DEFAULT now() + INTERVAL '1 hour',
      created_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now()
  );
