CREATE TABLE
  IF NOT EXISTS measure (
    id BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    entity_id BIGINT NOT NULL REFERENCES entity(id) ON UPDATE CASCADE ON DELETE CASCADE,
    label label NOT NULL,
    current INTEGER NOT NULL,
    maximum INTEGER NOT NULL,
    UNIQUE(entity_id, label),
    UNIQUE(entity_id, label, maximum),
    created_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now(),
      updated_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now()
  );

SELECT
  diesel_manage_updated_at('measure');
