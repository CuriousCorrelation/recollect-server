CREATE TYPE action AS ENUM ('author', 'ascribe', 'write', 'read_write');
