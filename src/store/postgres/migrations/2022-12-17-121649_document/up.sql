CREATE TABLE
  IF NOT EXISTS document (
    id BIGINT UNIQUE NOT NULL PRIMARY KEY REFERENCES object(id) ON UPDATE CASCADE ON DELETE CASCADE,
    handle VARCHAR(16) UNIQUE NOT NULL REFERENCES namespace(handle) ON UPDATE CASCADE ON DELETE CASCADE,
    content JSONB NOT NULL,
    search_index TSVECTOR NOT NULL GENERATED ALWAYS AS (jsonb_to_tsvector('english', content, '["string", "numeric", "boolean"]')) STORED,
    created_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now(),
      updated_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now(),
      deleted_at TIMESTAMP
    WITH
      TIME ZONE DEFAULT NULL
  );

SELECT
  diesel_manage_updated_at('document');

CREATE INDEX documents_search_index ON document USING gin(search_index);
