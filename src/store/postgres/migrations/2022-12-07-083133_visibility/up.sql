CREATE TABLE
  IF NOT EXISTS visibility (
    id BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    label label NOT NULL,
    object BIGINT UNIQUE NOT NULL REFERENCES object(id) ON UPDATE CASCADE ON DELETE CASCADE,
    scope scope NOT NULL DEFAULT 'private',
    UNIQUE(object, scope),
    UNIQUE(label, object, scope),
    created_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now(),
      updated_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now(),
      deleted_at TIMESTAMP
    WITH
      TIME ZONE DEFAULT NULL
  );

SELECT
  diesel_manage_updated_at('visibility');
