CREATE TABLE
  IF NOT EXISTS namespace (
    handle VARCHAR(16) PRIMARY KEY,
    created_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now(),
      updated_at TIMESTAMP
    WITH
      TIME ZONE NOT NULL DEFAULT now()
  );

SELECT
  diesel_manage_updated_at('namespace');

CREATE INDEX namespace_search_index ON namespace USING gin (handle gin_trgm_ops);
