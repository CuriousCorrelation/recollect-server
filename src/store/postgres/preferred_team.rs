use diesel::{
    expression_methods::ExpressionMethods,
    query_dsl::{QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use std::task::{ready, Context, Poll};
use tower::Service;

use crate::{
    model::{
        ops,
        preferred_team::{Exclusive, Identify, Inclusive, Partial, PreferredTeam},
        who::Who,
        BoxFuture, Request,
    },
    store::{error::StoreError, schema::preferred_team},
};

use super::Postgres;

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<Identify>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let preferred_team = self.get();

        async move {
            let request = preferred_team?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => Identify::Id(
                        preferred_team::table
                            .filter(preferred_team::id.eq(id))
                            .select(preferred_team::id)
                            .get_result(conn)?,
                    ),

                    Identify::EntityId(entity_id) => Identify::EntityId(
                        preferred_team::table
                            .filter(preferred_team::entity_id.eq(entity_id))
                            .select(preferred_team::entity_id)
                            .get_result(conn)?,
                    ),

                    Identify::TeamId(team_id) => Identify::TeamId(
                        preferred_team::table
                            .filter(preferred_team::team_id.eq(team_id))
                            .select(preferred_team::team_id)
                            .get_result(conn)?,
                    ),

                    Identify::Unique { team_id, entity_id } => Identify::Id(
                        preferred_team::table
                            .filter(preferred_team::team_id.eq(team_id))
                            .filter(preferred_team::entity_id.eq(entity_id))
                            .select(preferred_team::id)
                            .get_result(conn)?,
                    ),
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<PreferredTeam>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let preferred_team = self.get();

        async move {
            let request = preferred_team?.transaction(|conn| {
                let record = diesel::insert_into(preferred_team::table)
                    .values(request)
                    .returning((
                        preferred_team::id,
                        preferred_team::entity_id,
                        preferred_team::team_id,
                        preferred_team::updated_at,
                    ))
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<PreferredTeam>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let preferred_team = self.get();

        async move {
            let request = preferred_team?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => preferred_team::table
                        .filter(preferred_team::id.eq(id))
                        .select((
                            preferred_team::id,
                            preferred_team::entity_id,
                            preferred_team::team_id,
                            preferred_team::updated_at,
                        ))
                        .get_result(conn)?,

                    Identify::EntityId(entity_id) => preferred_team::table
                        .filter(preferred_team::entity_id.eq(entity_id))
                        .select((
                            preferred_team::id,
                            preferred_team::entity_id,
                            preferred_team::team_id,
                            preferred_team::updated_at,
                        ))
                        .get_result(conn)?,

                    Identify::TeamId(team_id) => preferred_team::table
                        .filter(preferred_team::team_id.eq(team_id))
                        .select((
                            preferred_team::id,
                            preferred_team::entity_id,
                            preferred_team::team_id,
                            preferred_team::updated_at,
                        ))
                        .get_result(conn)?,

                    Identify::Unique { team_id, entity_id } => preferred_team::table
                        .filter(preferred_team::team_id.eq(team_id))
                        .filter(preferred_team::entity_id.eq(entity_id))
                        .select((
                            preferred_team::id,
                            preferred_team::entity_id,
                            preferred_team::team_id,
                            preferred_team::updated_at,
                        ))
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<PreferredTeam>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id,
            team_id: _,
        } = who;

        let preferred_team = self.get();

        async move {
            let request = preferred_team?.transaction(|conn| {
                let mut query = preferred_team::table
                    .filter(
                        // `who` can search amongst
                        // 1. `preferred_team`s where `who.entity_id` is `preferred_team::entity_id`,
                        preferred_team::entity_id.eq(entity_id),
                    )
                    .into_boxed();

                if let Some(entity_id) = request.entity_id {
                    query = query.filter(preferred_team::entity_id.eq(entity_id));
                }

                if let Some(team_id) = request.team_id {
                    query = query.filter(preferred_team::team_id.eq(team_id));
                }

                if let Some(created_at) = request.created_at {
                    query = query.filter(preferred_team::created_at.eq(created_at));
                }

                let records = query
                    .select((
                        preferred_team::id,
                        preferred_team::entity_id,
                        preferred_team::team_id,
                        preferred_team::updated_at,
                    ))
                    .get_results(conn)?;

                Ok::<_, StoreError>(records)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Update<Request<Exclusive>>> for Postgres {
    type Response = Request<PreferredTeam>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Update(req): ops::Update<Request<Exclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let preferred_team = self.get();

        async move {
            let request = preferred_team?.transaction(|conn| {
                let record =
                    diesel::update(preferred_team::table.filter(preferred_team::id.eq(request.id)))
                        .set(request)
                        .returning((
                            preferred_team::id,
                            preferred_team::entity_id,
                            preferred_team::team_id,
                            preferred_team::updated_at,
                        ))
                        .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<PreferredTeam>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let preferred_team = self.get();

        async move {
            let request = preferred_team?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => diesel::delete(preferred_team::table.find(id))
                        .returning((
                            preferred_team::id,
                            preferred_team::entity_id,
                            preferred_team::team_id,
                            preferred_team::updated_at,
                        ))
                        .get_result(conn)?,

                    Identify::EntityId(entity_id) => diesel::delete(
                        preferred_team::table.filter(preferred_team::entity_id.eq(entity_id)),
                    )
                    .returning((
                        preferred_team::id,
                        preferred_team::entity_id,
                        preferred_team::team_id,
                        preferred_team::updated_at,
                    ))
                    .get_result(conn)?,

                    Identify::TeamId(team_id) => diesel::delete(
                        preferred_team::table.filter(preferred_team::team_id.eq(team_id)),
                    )
                    .returning((
                        preferred_team::id,
                        preferred_team::entity_id,
                        preferred_team::team_id,
                        preferred_team::updated_at,
                    ))
                    .get_result(conn)?,

                    Identify::Unique { team_id, entity_id } => diesel::delete(
                        preferred_team::table
                            .filter(preferred_team::team_id.eq(team_id))
                            .filter(preferred_team::entity_id.eq(entity_id)),
                    )
                    .returning((
                        preferred_team::id,
                        preferred_team::entity_id,
                        preferred_team::team_id,
                        preferred_team::updated_at,
                    ))
                    .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
