use std::task::{ready, Context, Poll};

use diesel::{
    expression_methods::{BoolExpressionMethods, ExpressionMethods},
    query_dsl::{JoinOnDsl, QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use tower::Service;

use crate::{
    model::{
        ascribe::Ascribe,
        ascribe::{Identify, Inclusive, Partial},
        label::Label,
        ops,
        scope::Scope,
        who::Who,
        BoxFuture, PrimaryKey, Request,
    },
    store::{
        error::StoreError,
        schema::{ascribe, author, connection, document, visibility},
    },
};

use super::Postgres;

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<PrimaryKey>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => ascribe::table
                        .filter(ascribe::id.eq(id))
                        .select(ascribe::id)
                        .get_result(conn)?,

                    Identify::DocumentId(document_id) => ascribe::table
                        .filter(ascribe::document_id.eq(document_id))
                        .select(ascribe::id)
                        .get_result(conn)?,

                    Identify::Unique {
                        document_id,
                        team_id,
                    } => ascribe::table
                        .filter(ascribe::document_id.eq(document_id))
                        .filter(ascribe::team_id.eq(team_id))
                        .select(ascribe::id)
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Ascribe>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = diesel::insert_into(ascribe::table)
                    .values(request)
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Ascribe>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => {
                        ascribe::table.filter(ascribe::id.eq(id)).get_result(conn)?
                    }

                    Identify::DocumentId(document_id) => ascribe::table
                        .filter(ascribe::document_id.eq(document_id))
                        .get_result(conn)?,

                    Identify::Unique {
                        document_id,
                        team_id,
                    } => ascribe::table
                        .filter(ascribe::document_id.eq(document_id))
                        .filter(ascribe::team_id.eq(team_id))
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<Ascribe>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who { entity_id, team_id } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let mut query = ascribe::table
                    .inner_join(document::table.on(document::id.eq(ascribe::document_id)))
                    .inner_join(visibility::table.on(visibility::object.eq(document::id)))
                    .inner_join(author::table.on(author::document_id.eq(document::id)))
                    .left_join(connection::table.on(connection::destinate.eq(document::id)))
                    .filter(document::deleted_at.is_null())
                    .filter(
                        // `who` can search amongst
                        // 1. documents where `entity_id` is the author,
                        // 2. scope is `Public`,
                        // 3. connection originating from `entity_id`,
                        // 4. scope is `TeamOnly` and team is `team_id`.
                        author::entity_id
                            .eq(entity_id)
                            .or(visibility::label
                                .eq(Label::Document)
                                .and(visibility::scope.eq(Scope::Public)))
                            .or(connection::originate_label
                                .eq(Label::Entity)
                                .and(connection::originate.eq(entity_id))
                                .and(connection::destinate_label.eq(Label::Document)))
                            .or(ascribe::team_id.eq(team_id).and(
                                visibility::label
                                    .eq(Label::Document)
                                    .and(visibility::scope.eq(Scope::TeamOnly)),
                            )),
                    )
                    .into_boxed();

                if let Some(document_id) = request.document_id {
                    query = query.filter(ascribe::document_id.eq(document_id));
                }

                if let Some(team_id) = request.team_id {
                    query = query.filter(ascribe::team_id.eq(team_id));
                }

                if let Some(created_at) = request.created_at {
                    query = query.filter(ascribe::created_at.eq(created_at));
                }

                let records = query
                    .select((
                        ascribe::id,
                        ascribe::document_id,
                        ascribe::team_id,
                        ascribe::created_at,
                    ))
                    .get_results(conn)?;

                Ok::<_, StoreError>(records)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Ascribe>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => diesel::delete(ascribe::table.find(id)).get_result(conn)?,

                    Identify::DocumentId(document_id) => {
                        diesel::delete(ascribe::table.filter(ascribe::document_id.eq(document_id)))
                            .get_result(conn)?
                    }

                    Identify::Unique {
                        document_id,
                        team_id,
                    } => diesel::delete(
                        ascribe::table
                            .filter(ascribe::document_id.eq(document_id))
                            .filter(ascribe::team_id.eq(team_id)),
                    )
                    .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
