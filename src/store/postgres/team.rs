use std::{
    task::{ready, Context, Poll},
    time::SystemTime,
};

use chrono::{DateTime, Utc};
use diesel::{
    expression_methods::{BoolExpressionMethods, ExpressionMethods},
    query_dsl::{JoinOnDsl, QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use tower::Service;

use crate::{
    model::{
        label::Label,
        ops,
        scope::Scope,
        team::Team,
        team::{Exclusive, Identify, Inclusive, Partial},
        who::Who,
        BoxFuture, PrimaryKey, Request,
    },
    store::error::StoreError,
    store::schema::{connection, member, team, visibility},
};

use super::Postgres;

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<PrimaryKey>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => team::table
                        .filter(team::deleted_at.is_null())
                        .filter(team::id.eq(id))
                        .select(team::id)
                        .get_result(conn)?,

                    Identify::Handle(handle) => team::table
                        .filter(team::deleted_at.is_null())
                        .filter(team::handle.eq(handle))
                        .select(team::id)
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Team>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = diesel::insert_into(team::table)
                    .values(request)
                    .returning((team::id, team::handle, team::name))
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Team>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => team::table
                        .filter(team::deleted_at.is_null())
                        .filter(team::id.eq(id))
                        .select((team::id, team::handle, team::name))
                        .get_result(conn)?,

                    Identify::Handle(handle) => team::table
                        .filter(team::deleted_at.is_null())
                        .filter(team::handle.eq(handle))
                        .select((team::id, team::handle, team::name))
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<Team>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id,
            team_id: _,
        } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let mut query = team::table
                    .inner_join(member::table)
                    .inner_join(visibility::table.on(team::id.eq(visibility::object)))
                    .left_join(connection::table.on(team::id.eq(connection::destinate)))
                    .filter(
                        // Among teams where `entity_id` is the member,
                        member::entity_id
                            .eq(&entity_id)
                            // scope is `Public`,
                            .or(visibility::label
                                .eq(Label::Team)
                                .and(visibility::scope.eq(Scope::Public)))
                            // connection originating from `entity_id`.
                            .or(connection::originate_label
                                .eq(Label::Entity)
                                .and(connection::originate.eq(&entity_id))
                                .and(connection::destinate_label.eq(Label::Team))),
                    )
                    .into_boxed();

                if let Some(name) = &request.name {
                    query = query.filter(team::name.eq(name));
                }

                query = query.filter(team::deleted_at.is_null());

                let records = query
                    .select((team::id, team::handle, team::name))
                    .get_results(conn)?;

                Ok::<_, StoreError>(records)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Update<Request<Exclusive>>> for Postgres {
    type Response = Request<Team>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Update(req): ops::Update<Request<Exclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let entity = self.get();

        async move {
            let request = entity?.transaction(|conn| {
                let record = diesel::update(
                    team::table
                        .filter(team::id.eq(request.id))
                        .filter(team::deleted_at.is_null()),
                )
                .set(request)
                .returning((team::id, team::handle, team::name))
                .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Team>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let now: DateTime<Utc> = DateTime::from(
                    diesel::select(diesel::dsl::now).get_result::<SystemTime>(conn)?,
                );

                let record = match request {
                    Identify::Id(id) => diesel::update(
                        team::table
                            .filter(team::deleted_at.is_null())
                            .filter(team::id.eq(id)),
                    )
                    .set(team::deleted_at.eq(now))
                    .returning((team::id, team::handle, team::name))
                    .get_result(conn)?,

                    Identify::Handle(handle) => diesel::update(
                        team::table
                            .filter(team::deleted_at.is_null())
                            .filter(team::handle.eq(handle)),
                    )
                    .set(team::deleted_at.eq(now))
                    .returning((team::id, team::handle, team::name))
                    .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
