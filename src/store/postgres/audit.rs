use std::task::{ready, Context, Poll};

use diesel::{
    expression_methods::ExpressionMethods,
    query_dsl::{QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use tower::Service;

use crate::{
    model::{
        audit::Audit,
        audit::{Identify, Inclusive, Partial},
        ops,
        who::Who,
        BoxFuture, Request,
    },
    store::error::StoreError,
    store::schema::audit,
};

use super::Postgres;

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<Identify>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => Identify::Id(
                        audit::table
                            .filter(audit::id.eq(id))
                            .select(audit::id)
                            .get_result(conn)?,
                    ),
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Audit>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who { entity_id, team_id } = who;

        let audit = self.get();

        async move {
            let request = audit?.transaction(|conn| {
                let record = diesel::insert_into(audit::table)
                    .values(request)
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Audit>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => audit::table.filter(audit::id.eq(id)).get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<Audit>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let mut query = audit::table.into_boxed();

                if let Some(context) = request.context {
                    query = query.filter(audit::context.eq(context));
                }

                if let Some(version) = request.version {
                    query = query.filter(audit::version.eq(version));
                }

                if let Some(service_id) = request.service_id {
                    query = query.filter(audit::service_id.eq(service_id));
                }

                let records = query.get_results(conn)?;

                Ok::<_, StoreError>(records)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Audit>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => diesel::delete(audit::table.find(id)).get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
