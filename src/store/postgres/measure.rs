use diesel::{
    expression_methods::ExpressionMethods,
    query_dsl::{QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use std::task::{ready, Context, Poll};
use tower::Service;

use crate::{
    model::{
        measure::{Exclusive, Identify, Inclusive, Measure, Partial},
        ops,
        who::Who,
        BoxFuture, PrimaryKey, Request,
    },
    store::{error::StoreError, schema::measure},
};

use super::Postgres;

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<PrimaryKey>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let measure = self.get();

        async move {
            let request = measure?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => measure::table
                        .filter(measure::id.eq(id))
                        .filter(measure::current.le(measure::maximum))
                        .select(measure::id)
                        .get_result(conn)?,

                    Identify::Unique { entity_id, label } => measure::table
                        .filter(measure::entity_id.eq(entity_id))
                        .filter(measure::label.eq(label))
                        .filter(measure::current.le(measure::maximum))
                        .select(measure::id)
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Measure>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let measure = self.get();

        async move {
            let request = measure?.transaction(|conn| {
                let record = diesel::insert_into(measure::table)
                    .values(request)
                    .returning((measure::id, measure::current))
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Measure>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let measure = self.get();

        async move {
            let request = measure?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => measure::table
                        .filter(measure::id.eq(id))
                        .filter(measure::current.le(measure::maximum))
                        .select((measure::id, measure::current))
                        .get_result(conn)?,

                    Identify::Unique { entity_id, label } => measure::table
                        .filter(measure::entity_id.eq(entity_id))
                        .filter(measure::label.eq(label))
                        .filter(measure::current.le(measure::maximum))
                        .select((measure::id, measure::current))
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<Measure>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let measure = self.get();

        async move {
            let request = measure?.transaction(|conn| {
                let mut query = measure::table.into_boxed();

                if let Some(entity_id) = request.entity_id {
                    query = query.filter(measure::entity_id.eq(entity_id));
                }

                if let Some(label) = request.label {
                    query = query.filter(measure::label.eq(label));
                }

                if let Some(current) = request.current {
                    query = query.filter(measure::current.eq(current));
                }

                if let Some(maximum) = request.maximum {
                    query = query.filter(measure::maximum.eq(maximum));
                }

                let records = query
                    .select((measure::id, measure::current))
                    .get_results(conn)?;

                Ok::<_, StoreError>(records)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Update<Request<Exclusive>>> for Postgres {
    type Response = Request<Measure>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Update(req): ops::Update<Request<Exclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let measure = self.get();

        async move {
            let request = measure?.transaction(|conn| {
                let record = diesel::update(measure::table.filter(measure::id.eq(request.id)))
                    .set(request)
                    .returning((measure::id, measure::current))
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Measure>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let measure = self.get();

        async move {
            let request = measure?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => diesel::delete(measure::table.find(id))
                        .returning((measure::id, measure::current))
                        .get_result(conn)?,

                    Identify::Unique { entity_id, label } => diesel::delete(
                        measure::table
                            .filter(measure::entity_id.eq(entity_id))
                            .filter(measure::label.eq(label)),
                    )
                    .returning((measure::id, measure::current))
                    .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
