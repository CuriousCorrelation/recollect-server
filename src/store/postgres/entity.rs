use chrono::{DateTime, Utc};
use diesel::{
    expression_methods::{BoolExpressionMethods, ExpressionMethods, PgTextExpressionMethods},
    query_dsl::{JoinOnDsl, QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use std::{
    task::{ready, Context, Poll},
    time::SystemTime,
};
use tower::Service;

use crate::{
    model::{
        entity::{Entity, Exclusive, Identify, Inclusive, Partial},
        label::Label,
        ops,
        scope::Scope,
        who::Who,
        BoxFuture, PrimaryKey, Request,
    },
    store::{
        error::StoreError,
        schema::{connection, entity, member, visibility},
    },
};

use super::Postgres;

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<PrimaryKey>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let entity = self.get();

        async move {
            let request = entity?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => entity::table
                        .filter(entity::deleted_at.is_null())
                        .filter(entity::id.eq(id))
                        .select(entity::id)
                        .get_result(conn)?,

                    Identify::Secret(secret) => entity::table
                        .filter(entity::deleted_at.is_null())
                        .filter(entity::secret.eq(secret))
                        .select(entity::id)
                        .get_result(conn)?,

                    Identify::Handle(handle) => entity::table
                        .filter(entity::deleted_at.is_null())
                        .filter(entity::handle.eq(handle))
                        .select(entity::id)
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Entity>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let entity = self.get();

        async move {
            let request = entity?.transaction(|conn| {
                let record = diesel::insert_into(entity::table)
                    .values(request)
                    .returning((entity::id, entity::handle, entity::secret, entity::name))
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Entity>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let entity = self.get();

        async move {
            let request = entity?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => entity::table
                        .filter(entity::deleted_at.is_null())
                        .filter(entity::id.eq(id))
                        .select((entity::id, entity::handle, entity::secret, entity::name))
                        .get_result(conn)?,

                    Identify::Secret(secret) => entity::table
                        .filter(entity::deleted_at.is_null())
                        .filter(entity::secret.eq(secret))
                        .select((entity::id, entity::handle, entity::secret, entity::name))
                        .get_result(conn)?,

                    Identify::Handle(handle) => entity::table
                        .filter(entity::deleted_at.is_null())
                        .filter(entity::handle.eq(handle))
                        .select((entity::id, entity::handle, entity::secret, entity::name))
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<Entity>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who { entity_id, team_id } = who;

        let entity = self.get();

        async move {
            let request = entity?.transaction(|conn| {
                let mut query = entity::table
                    .inner_join(member::table)
                    .inner_join(visibility::table.on(entity::id.eq(visibility::object)))
                    .left_join(connection::table.on(entity::id.eq(connection::destinate)))
                    .filter(
                        // Among self
                        entity::id
                            .eq(entity_id)
                            // scope is `TeamOnly` and team is `team_id`.
                            .or(member::team_id.eq(team_id).and(
                                visibility::label
                                    .eq(Label::Entity)
                                    .and(visibility::scope.eq(Scope::TeamOnly)),
                            ))
                            .or(
                                // or entities where scope is `Public`,
                                visibility::label
                                    .eq(Label::Entity)
                                    .and(visibility::scope.eq(Scope::Public)),
                            )
                            // connection originating from `entity_id`
                            .or(connection::originate_label
                                .eq(Label::Entity)
                                .and(connection::originate.eq(entity_id))
                                .and(connection::destinate_label.eq(Label::Entity))),
                    )
                    .into_boxed();

                if let Some(name) = &request.name {
                    query = query.filter(entity::name.ilike(format!("%{}%", name)));
                }

                query = query.filter(entity::deleted_at.is_null());

                let query =
                    query.select((entity::id, entity::handle, entity::secret, entity::name));

                tracing::debug!(
                    "query: {:#?}",
                    diesel::debug_query::<diesel::pg::Pg, _>(&query)
                );

                let records = query.get_results(conn)?;

                Ok::<_, StoreError>(records)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Update<Request<Exclusive>>> for Postgres {
    type Response = Request<Entity>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Update(req): ops::Update<Request<Exclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let entity = self.get();

        async move {
            let request = entity?.transaction(|conn| {
                let record = diesel::update(
                    entity::table
                        .filter(entity::id.eq(request.id))
                        .filter(entity::deleted_at.is_null()),
                )
                .set(request)
                .returning((entity::id, entity::handle, entity::secret, entity::name))
                .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Entity>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let entity = self.get();

        async move {
            let request = entity?.transaction(|conn| {
                let now: DateTime<Utc> = DateTime::from(
                    diesel::select(diesel::dsl::now).get_result::<SystemTime>(conn)?,
                );

                let record = match request {
                    Identify::Id(id) => diesel::update(
                        entity::table
                            .filter(entity::deleted_at.is_null())
                            .filter(entity::id.eq(id)),
                    )
                    .set(entity::deleted_at.eq(now))
                    .returning((entity::id, entity::handle, entity::secret, entity::name))
                    .get_result(conn)?,

                    Identify::Secret(secret) => diesel::update(
                        entity::table
                            .filter(entity::deleted_at.is_null())
                            .filter(entity::secret.eq(secret)),
                    )
                    .set(entity::deleted_at.eq(now))
                    .returning((entity::id, entity::handle, entity::secret, entity::name))
                    .get_result(conn)?,

                    Identify::Handle(handle) => diesel::update(
                        entity::table
                            .filter(entity::deleted_at.is_null())
                            .filter(entity::handle.eq(handle)),
                    )
                    .set(entity::deleted_at.eq(now))
                    .returning((entity::id, entity::handle, entity::secret, entity::name))
                    .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
