use std::task::{ready, Context, Poll};

use diesel::{
    expression_methods::{BoolExpressionMethods, ExpressionMethods},
    query_dsl::{JoinOnDsl, QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use tower::Service;

use crate::{
    model::{
        admin::Admin,
        admin::{Identify, Inclusive, Partial},
        label::Label,
        ops,
        scope::Scope,
        who::Who,
        BoxFuture, PrimaryKey, Request,
    },
    store::error::StoreError,
    store::schema::{admin, connection, visibility},
};

use super::Postgres;

impl Service<ops::Count<Request<Partial>>> for Postgres {
    type Response = Request<i64>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Count(req): ops::Count<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who { entity_id, team_id } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let mut query = admin::table
                    .inner_join(visibility::table.on(visibility::object.eq(admin::team_id)))
                    .left_join(connection::table.on(connection::destinate.eq(admin::team_id)))
                    .filter(
                        // `who` can search amongst
                        // 1. `admin` among `who.entity_id`'s `team`,
                        // 2. `admin` where `who.entity_id` is `admin`,
                        // 3. `admin` of `Public` `team`s,
                        // 4. `admin` of `team`s with `connection` originating from `entity_id`,
                        admin::team_id
                            .eq(team_id)
                            .or(admin::entity_id.eq(entity_id))
                            .or(visibility::label
                                .eq(Label::Team)
                                .and(visibility::scope.eq(Scope::Public)))
                            .or(connection::originate_label
                                .eq(Label::Entity)
                                .and(connection::originate.eq(entity_id))
                                .and(connection::destinate_label.eq(Label::Team))),
                    )
                    .into_boxed();

                if let Some(entity_id) = request.entity_id {
                    query = query.filter(admin::entity_id.eq(entity_id));
                }

                if let Some(team_id) = request.team_id {
                    query = query.filter(admin::team_id.eq(team_id));
                }

                if let Some(created_at) = request.created_at {
                    query = query.filter(admin::created_at.eq(created_at));
                }

                let query = query.count();

                tracing::debug!(
                    "query: {:#?}",
                    diesel::debug_query::<diesel::pg::Pg, _>(&query)
                );

                let record = query.get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<PrimaryKey>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => admin::table
                        .filter(admin::id.eq(id))
                        .select(admin::id)
                        .get_result(conn)?,

                    Identify::Unique { entity_id, team_id } => admin::table
                        .filter(admin::entity_id.eq(entity_id))
                        .filter(admin::team_id.eq(team_id))
                        .select(admin::id)
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Admin>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = diesel::insert_into(admin::table)
                    .values(request)
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Admin>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => admin::table.filter(admin::id.eq(id)).get_result(conn)?,

                    Identify::Unique { entity_id, team_id } => admin::table
                        .filter(admin::entity_id.eq(entity_id))
                        .filter(admin::team_id.eq(team_id))
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<Admin>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who { entity_id, team_id } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let mut query = admin::table
                    .inner_join(visibility::table.on(visibility::object.eq(admin::team_id)))
                    .left_join(connection::table.on(connection::destinate.eq(admin::team_id)))
                    .filter(
                        // `who` can search amongst
                        // 1. `admin` among `who.entity_id`'s `team`,
                        // 2. `admin` where `who.entity_id` is `admin`,
                        // 3. `admin` of `Public` `team`s,
                        // 4. `admin` of `team`s with `connection` originating from `entity_id`,
                        admin::team_id
                            .eq(team_id)
                            .or(admin::entity_id.eq(entity_id))
                            .or(visibility::label
                                .eq(Label::Team)
                                .and(visibility::scope.eq(Scope::Public)))
                            .or(connection::originate_label
                                .eq(Label::Entity)
                                .and(connection::originate.eq(entity_id))
                                .and(connection::destinate_label.eq(Label::Team))),
                    )
                    .into_boxed();

                if let Some(entity_id) = request.entity_id {
                    query = query.filter(admin::entity_id.eq(entity_id));
                }

                if let Some(team_id) = request.team_id {
                    query = query.filter(admin::team_id.eq(team_id));
                }

                if let Some(created_at) = request.created_at {
                    query = query.filter(admin::created_at.eq(created_at));
                }

                let query = query.select((
                    admin::id,
                    admin::entity_id,
                    admin::team_id,
                    admin::created_at,
                    admin::updated_at,
                ));

                tracing::debug!(
                    "query: {:#?}",
                    diesel::debug_query::<diesel::pg::Pg, _>(&query)
                );

                let records = query.get_results(conn)?;

                Ok::<_, StoreError>(records)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Admin>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => diesel::delete(admin::table.find(id)).get_result(conn)?,

                    Identify::Unique { entity_id, team_id } => diesel::delete(
                        admin::table
                            .filter(admin::entity_id.eq(entity_id))
                            .filter(admin::team_id.eq(team_id)),
                    )
                    .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
