use std::{
    task::{ready, Context, Poll},
    time::SystemTime,
};

use chrono::{DateTime, Utc};
use diesel::{
    expression_methods::ExpressionMethods,
    query_dsl::{QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use tower::Service;

use crate::{
    model::{
        auth::Auth,
        auth::{Exclusive, Identify, Inclusive, Partial},
        ops,
        who::Who,
        BoxFuture, Request, Secret,
    },
    store::error::StoreError,
    store::schema::auth,
};

use super::Postgres;

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<Secret>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Secret(secret) => auth::table
                        .filter(auth::deleted_at.is_null())
                        .filter(auth::secret.eq(secret))
                        .select(auth::secret)
                        .get_result(conn)?,

                    Identify::Email(email) => auth::table
                        .filter(auth::deleted_at.is_null())
                        .filter(auth::email.eq(email))
                        .select(auth::secret)
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Auth>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = diesel::insert_into(auth::table)
                    .values(request)
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Auth>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Secret(secret) => auth::table
                        .filter(auth::deleted_at.is_null())
                        .filter(auth::secret.eq(secret))
                        .get_result(conn)?,

                    Identify::Email(email) => auth::table
                        .filter(auth::deleted_at.is_null())
                        .filter(auth::email.eq(email))
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<Auth>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let mut query = auth::table.into_boxed();

                if let Some(id) = &request.id {
                    query = query.filter(auth::id.eq(id));
                }

                if let Some(name) = &request.name {
                    query = query.filter(auth::name.eq(name));
                }

                if let Some(picture) = &request.picture {
                    query = query.filter(auth::picture.eq(picture));
                }

                if let Some(provider) = &request.provider {
                    query = query.filter(auth::provider.eq(provider));
                }

                query = query.filter(auth::deleted_at.is_null());

                let records = query.get_results(conn)?;

                Ok::<_, StoreError>(records)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Update<Request<Exclusive>>> for Postgres {
    type Response = Request<Auth>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Update(req): ops::Update<Request<Exclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = diesel::update(auth::table.filter(auth::secret.eq(request.secret)))
                    .set(request)
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Auth>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let now: DateTime<Utc> = DateTime::from(
                    diesel::select(diesel::dsl::now).get_result::<SystemTime>(conn)?,
                );

                let record = match request {
                    Identify::Secret(secret) => {
                        diesel::update(auth::table.filter(auth::secret.eq(secret)))
                            .set(auth::deleted_at.eq(now))
                            .get_result(conn)?
                    }

                    Identify::Email(email) => {
                        diesel::update(auth::table.filter(auth::email.eq(email)))
                            .set(auth::deleted_at.eq(now))
                            .get_result(conn)?
                    }
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
