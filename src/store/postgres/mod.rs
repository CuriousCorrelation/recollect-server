pub(crate) mod admin;
pub(crate) mod ascribe;
pub(crate) mod audit;
pub(crate) mod auth;
pub(crate) mod author;
pub(crate) mod blockade;
pub(crate) mod connection;
pub(crate) mod document;
pub(crate) mod entity;
pub(crate) mod measure;
pub(crate) mod member;
pub(crate) mod namespace;
pub(crate) mod object;
pub(crate) mod preferred_team;
pub(crate) mod requester;
pub(crate) mod session;
pub(crate) mod team;
pub(crate) mod token;
pub(crate) mod visibility;

use std::{
    path::Path,
    task::{ready, Context, Poll},
};

use diesel::{
    r2d2::{ConnectionManager, Pool, PooledConnection},
    PgConnection,
};
use serde::Deserialize;

use super::error::StoreError;

#[derive(Debug, Clone)]
pub(crate) struct Postgres {
    pool: Pool<ConnectionManager<PgConnection>>,
}

impl Postgres {
    pub fn new(path: &Path) -> Result<Self, StoreError> {
        #[derive(Deserialize)]
        struct Config {
            database: DatabaseConfig,
        }

        #[derive(Deserialize)]
        struct DatabaseConfig {
            postgres: PostgresConfig,
        }

        #[derive(Deserialize)]
        struct PostgresConfig {
            user: String,
            password: String,
            host: String,
            port: String,
            db: String,
        }

        let postgres_config = toml::from_str::<Config>(&std::fs::read_to_string(path)?)?
            .database
            .postgres;

        let database_url = format!(
            "postgres://{}:{}@{}:{}/{}",
            postgres_config.user,
            postgres_config.password,
            postgres_config.host,
            postgres_config.port,
            postgres_config.db
        );

        let manager = ConnectionManager::new(database_url);

        Ok(Self {
            pool: Pool::builder().max_size(16).build(manager)?,
        })
    }

    pub fn get(&self) -> Result<PooledConnection<ConnectionManager<PgConnection>>, StoreError> {
        Ok(self.pool.get()?)
    }

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), StoreError>> {
        match self.get() {
            Ok(_) => Poll::Ready(Ok(())),
            Err(_) => {
                cx.waker().wake_by_ref();
                Poll::Pending
            }
        }
    }
}
