use std::{
    task::{ready, Context, Poll},
    time::SystemTime,
};

use base64::{engine::general_purpose, Engine};
use chrono::{DateTime, Duration, Utc};
use diesel::{
    expression_methods::ExpressionMethods,
    query_dsl::{QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use ring::rand::{SecureRandom, SystemRandom};
use tower::Service;

use crate::{
    model::{
        ops,
        session::{Identify, Inclusive, Session},
        BoxFuture, Request,
    },
    store::error::StoreError,
    store::schema::session,
};

use super::Postgres;

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Session>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                // Generate `id` inside `transaction`
                // to avoid invoking functions if `connection` cannot be fetched.
                let mut dest = vec![0; 22];

                let secure_random = SystemRandom::new();

                secure_random
                    .fill(&mut dest)
                    .map_err(|_| StoreError::Crypto)?;

                let mut id = general_purpose::URL_SAFE.encode(dest);

                id.truncate(22);

                let record = diesel::insert_into(session::table)
                    .values((session::id.eq(id), session::secret.eq(request.secret)))
                    .returning((session::id, session::secret))
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Session>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let now: DateTime<Utc> = DateTime::from(
                    diesel::select(diesel::dsl::now).get_result::<SystemTime>(conn)?,
                );

                let id: String = match request {
                    Identify::Id(id) => session::table
                        .filter(session::fetched_at.gt(now - Duration::minutes(10)))
                        .filter(session::id.eq(id))
                        .select(session::id)
                        .get_result(conn)?,
                };

                let record = diesel::update(session::table.filter(session::id.eq(id)))
                    .set(session::fetched_at.eq(now))
                    .returning((session::id, session::secret))
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Session>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => diesel::delete(session::table.find(id))
                        .returning((session::id, session::secret))
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
