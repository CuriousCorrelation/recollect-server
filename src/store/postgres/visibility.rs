use std::{
    task::{ready, Context, Poll},
    time::SystemTime,
};

use chrono::{DateTime, Utc};
use diesel::{
    expression_methods::ExpressionMethods,
    query_dsl::{QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use tower::Service;

use crate::{
    model::{
        ops,
        visibility::Visibility,
        visibility::{Exclusive, Identify, Inclusive, Partial},
        who::Who,
        BoxFuture, PrimaryKey, Request,
    },
    store::error::StoreError,
    store::schema::visibility,
};

use super::Postgres;

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<PrimaryKey>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => visibility::table
                        .filter(visibility::id.eq(id))
                        .select(visibility::id)
                        .get_result(conn)?,

                    Identify::Object(object) => visibility::table
                        .filter(visibility::object.eq(object))
                        .select(visibility::object)
                        .get_result(conn)?,

                    Identify::Unique {
                        object,
                        scope,
                        label,
                    } => {
                        let mut query = visibility::table
                            .filter(visibility::object.eq(object))
                            .filter(visibility::scope.eq(scope))
                            .into_boxed();

                        if let Some(label) = label {
                            query = query.filter(visibility::label.eq(label));
                        }

                        query.select(visibility::id).get_result(conn)?
                    }
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Visibility>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = diesel::insert_into(visibility::table)
                    .values(request)
                    .returning((
                        visibility::id,
                        visibility::label,
                        visibility::object,
                        visibility::scope,
                        visibility::updated_at,
                    ))
                    .get_result(conn)?;

                tracing::debug!("record: {record:#?}");

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Visibility>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => visibility::table
                        .filter(visibility::id.eq(id))
                        .select((
                            visibility::id,
                            visibility::label,
                            visibility::object,
                            visibility::scope,
                            visibility::updated_at,
                        ))
                        .get_result(conn)?,

                    Identify::Object(object) => visibility::table
                        .filter(visibility::object.eq(object))
                        .select((
                            visibility::id,
                            visibility::label,
                            visibility::object,
                            visibility::scope,
                            visibility::updated_at,
                        ))
                        .get_result(conn)?,

                    Identify::Unique {
                        object,
                        scope,
                        label,
                    } => {
                        let mut query = visibility::table
                            .filter(visibility::object.eq(object))
                            .filter(visibility::scope.eq(scope))
                            .into_boxed();

                        if let Some(label) = label {
                            query = query.filter(visibility::label.eq(label));
                        }

                        query
                            .select((
                                visibility::id,
                                visibility::label,
                                visibility::object,
                                visibility::scope,
                                visibility::updated_at,
                            ))
                            .get_result(conn)?
                    }
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<Visibility>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let mut query = visibility::table.into_boxed();

                if let Some(scope) = request.scope {
                    query = query.filter(visibility::scope.eq(scope));
                }

                let records = query
                    .select((
                        visibility::id,
                        visibility::label,
                        visibility::object,
                        visibility::scope,
                        visibility::updated_at,
                    ))
                    .get_results(conn)?;

                Ok::<_, StoreError>(records)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Update<Request<Exclusive>>> for Postgres {
    type Response = Request<Visibility>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Update(req): ops::Update<Request<Exclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record =
                    diesel::update(visibility::table.filter(visibility::id.eq(request.id)))
                        .set(request)
                        .returning((
                            visibility::id,
                            visibility::label,
                            visibility::object,
                            visibility::scope,
                            visibility::updated_at,
                        ))
                        .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Visibility>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let now: DateTime<Utc> = DateTime::from(
                    diesel::select(diesel::dsl::now).get_result::<SystemTime>(conn)?,
                );

                let record = match request {
                    Identify::Id(id) => diesel::update(
                        visibility::table
                            .filter(visibility::deleted_at.is_null())
                            .filter(visibility::id.eq(id)),
                    )
                    .set(visibility::deleted_at.eq(now))
                    .returning((
                        visibility::id,
                        visibility::label,
                        visibility::object,
                        visibility::scope,
                        visibility::updated_at,
                    ))
                    .get_result(conn)?,

                    Identify::Object(object) => {
                        diesel::update(visibility::table.filter(visibility::object.eq(object)))
                            .set(visibility::deleted_at.eq(now))
                            .returning((
                                visibility::id,
                                visibility::label,
                                visibility::object,
                                visibility::scope,
                                visibility::updated_at,
                            ))
                            .get_result(conn)?
                    }

                    Identify::Unique {
                        object,
                        scope,
                        label,
                    } => {
                        let mut query = diesel::update(visibility::table)
                            .filter(visibility::object.eq(object))
                            .filter(visibility::scope.eq(scope))
                            .into_boxed();

                        if let Some(label) = label {
                            query = query.filter(visibility::label.eq(label));
                        }

                        query
                            .set(visibility::deleted_at.eq(now))
                            .returning((
                                visibility::id,
                                visibility::label,
                                visibility::object,
                                visibility::scope,
                                visibility::updated_at,
                            ))
                            .get_result(conn)?
                    }
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
