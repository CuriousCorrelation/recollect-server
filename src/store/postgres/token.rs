use std::task::{ready, Context, Poll};

use base64::{engine::general_purpose, Engine};
use diesel::{
    expression_methods::ExpressionMethods,
    query_dsl::{QueryDsl, RunQueryDsl},
    Connection,
};
use futures::FutureExt;
use ring::rand::{SecureRandom, SystemRandom};
use tower::Service;

use crate::{
    model::{
        ops,
        token::Token,
        token::{Identify, Inclusive},
        BoxFuture, Request,
    },
    store::error::StoreError,
    store::schema::token,
};

use super::Postgres;

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Token>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request: _ } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                // Generate `secret` inside `transaction`
                // to avoid invoking functions if `connection` cannot be fetched.
                let mut dest = vec![0; 22];

                let secure_random = SystemRandom::new();

                secure_random
                    .fill(&mut dest)
                    .map_err(|_| StoreError::Crypto)?;

                let mut secret = general_purpose::URL_SAFE.encode(dest);

                secret.truncate(22);

                let record = diesel::insert_into(token::table)
                    .values(token::secret.eq(secret))
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Token>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Secret(secret) => token::table
                        .filter(token::secret.eq(secret))
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Token>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let connection = self.get();

        async move {
            let request = connection?.transaction(|conn| {
                let record = match request {
                    Identify::Secret(secret) => {
                        diesel::delete(token::table.find(secret)).get_result(conn)?
                    }
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
