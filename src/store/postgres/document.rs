use std::{
    task::{ready, Context, Poll},
    time::SystemTime,
};

use chrono::{DateTime, Utc};
use diesel::{
    expression_methods::{BoolExpressionMethods, ExpressionMethods},
    query_dsl::{JoinOnDsl, QueryDsl, RunQueryDsl},
    Connection,
};
use diesel_full_text_search::{websearch_to_tsquery, TsVectorExtensions};
use futures::FutureExt;
use tower::Service;

use crate::{
    model::{
        document::{Document, Exclusive, Identify, Inclusive, Partial},
        label::Label,
        ops,
        scope::Scope,
        who::Who,
        BoxFuture, PrimaryKey, Request,
    },
    store::{
        error::StoreError,
        schema::{ascribe, author, connection, document, visibility},
    },
};

use super::Postgres;

impl Service<ops::Assure<Request<Identify>>> for Postgres {
    type Response = Request<PrimaryKey>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Assure(req): ops::Assure<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let document = self.get();

        async move {
            let request = document?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => {
                        let query = document::table
                            .filter(document::deleted_at.is_null())
                            .filter(document::id.eq(id))
                            .select(document::id);

                        tracing::debug!(
                            "query: {:#?}",
                            diesel::debug_query::<diesel::pg::Pg, _>(&query)
                        );

                        query.get_result(conn)?
                    }

                    Identify::Handle(handle) => {
                        let query = document::table
                            .filter(document::deleted_at.is_null())
                            .filter(document::handle.eq(handle))
                            .select(document::id);

                        tracing::debug!(
                            "query: {:#?}",
                            diesel::debug_query::<diesel::pg::Pg, _>(&query)
                        );

                        query.get_result(conn)?
                    }
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Create<Request<Inclusive>>> for Postgres {
    type Response = Request<Document>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Create(req): ops::Create<Request<Inclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let document = self.get();

        async move {
            let request = document?.transaction(|conn| {
                let record = diesel::insert_into(document::table)
                    .values(request)
                    .returning((
                        document::id,
                        document::handle,
                        document::content,
                        document::created_at,
                        document::updated_at,
                    ))
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Fetch<Request<Identify>>> for Postgres {
    type Response = Request<Document>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Fetch(req): ops::Fetch<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who {
            entity_id: _,
            team_id: _,
        } = who;

        let document = self.get();

        async move {
            let request = document?.transaction(|conn| {
                let record = match request {
                    Identify::Id(id) => document::table
                        .filter(document::deleted_at.is_null())
                        .filter(document::id.eq(id))
                        .select((
                            document::id,
                            document::handle,
                            document::content,
                            document::created_at,
                            document::updated_at,
                        ))
                        .get_result(conn)?,

                    Identify::Handle(handle) => document::table
                        .filter(document::deleted_at.is_null())
                        .filter(document::handle.eq(handle))
                        .select((
                            document::id,
                            document::handle,
                            document::content,
                            document::created_at,
                            document::updated_at,
                        ))
                        .get_result(conn)?,
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Find<Request<Partial>>> for Postgres {
    type Response = Request<Vec<Document>>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Find(req): ops::Find<Request<Partial>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;
        let Who { entity_id, team_id } = who;

        let document = self.get();

        async move {
            let request = document?.transaction(|conn| {
                let mut query = document::table
                    .inner_join(author::table)
                    .inner_join(ascribe::table)
                    .inner_join(visibility::table.on(visibility::object.eq(document::id)))
                    .left_join(connection::table.on(connection::destinate.eq(document::id)))
                    .filter(document::deleted_at.is_null())
                    .filter(
                        // Among documents where `entity_id` is the author,
                        author::entity_id
                            .eq(&entity_id)
                            // scope is `Public`,
                            .or(visibility::label
                                .eq(Label::Document)
                                .and(visibility::scope.eq(Scope::Public)))
                            // connection originating from `entity_id`,
                            .or(connection::originate_label
                                .eq(Label::Entity)
                                .and(connection::originate.eq(&entity_id))
                                .and(connection::destinate_label.eq(Label::Document)))
                            // scope is `TeamOnly` and team is `team_id`.
                            .or(ascribe::team_id.eq(&team_id).and(
                                visibility::label
                                    .eq(Label::Document)
                                    .and(visibility::scope.eq(Scope::TeamOnly)),
                            )),
                    )
                    .into_boxed();

                if let Some(content) = &request.content {
                    query =
                        query.filter(document::search_index.matches(websearch_to_tsquery(content)));
                }

                if let Some(created_at) = request.created_at {
                    query = query.filter(document::created_at.eq(created_at));
                }

                if let Some(updated_at) = request.updated_at {
                    query = query.filter(document::updated_at.eq(updated_at));
                }

                let records = query
                    .select((
                        document::id,
                        document::handle,
                        document::content,
                        document::created_at,
                        document::updated_at,
                    ))
                    .get_results(conn)?;

                Ok::<_, StoreError>(records)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Update<Request<Exclusive>>> for Postgres {
    type Response = Request<Document>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Update(req): ops::Update<Request<Exclusive>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let document = self.get();

        async move {
            let request = document?.transaction(|conn| {
                let record = diesel::update(document::table.filter(document::id.eq(request.id)))
                    .set(request)
                    .returning((
                        document::id,
                        document::handle,
                        document::content,
                        document::created_at,
                        document::updated_at,
                    ))
                    .get_result(conn)?;

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}

impl Service<ops::Delete<Request<Identify>>> for Postgres {
    type Response = Request<Document>;

    type Error = StoreError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, ops::Delete(req): ops::Delete<Request<Identify>>) -> Self::Future {
        tracing::debug!("request: {:#?}", req);

        let Request { who, request } = req;

        let document = self.get();

        async move {
            let request = document?.transaction(|conn| {
                let now: DateTime<Utc> = DateTime::from(
                    diesel::select(diesel::dsl::now).get_result::<SystemTime>(conn)?,
                );

                let record = match request {
                    Identify::Id(id) => diesel::update(document::table.filter(document::id.eq(id)))
                        .set(document::deleted_at.eq(now))
                        .returning((
                            document::id,
                            document::handle,
                            document::content,
                            document::created_at,
                            document::updated_at,
                        ))
                        .get_result(conn)?,

                    Identify::Handle(handle) => {
                        diesel::update(document::table.filter(document::handle.eq(handle)))
                            .set(document::deleted_at.eq(now))
                            .returning((
                                document::id,
                                document::handle,
                                document::content,
                                document::created_at,
                                document::updated_at,
                            ))
                            .get_result(conn)?
                    }
                };

                Ok::<_, StoreError>(record)
            })?;

            Ok(Request { who, request })
        }
        .boxed()
    }
}
