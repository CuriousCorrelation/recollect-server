use serde::{Serialize, Serializer};
use thiserror::Error;

#[derive(Debug, Error)]
pub(crate) enum StoreError {
    #[error("SQL - {0}")]
    Sql(diesel::result::Error),

    #[error("ORM - {0}")]
    R2d2(#[from] diesel::r2d2::PoolError),

    #[error("Crypto error")]
    Crypto,

    #[error("Not found")]
    NotFound,

    #[error("Failed to read local database config - {0}")]
    LocalConfig(#[from] std::io::Error),

    #[error("Failed to parse local database config - {0}")]
    Parse(#[from] toml::de::Error),
}

impl From<diesel::result::Error> for StoreError {
    fn from(e: diesel::result::Error) -> Self {
        match e {
            diesel::result::Error::NotFound => Self::NotFound,
            err => Self::Sql(err),
        }
    }
}

impl Serialize for StoreError {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match *self {
            StoreError::Sql(ref s) => {
                serializer.serialize_newtype_variant("StoreError", 0, "Sql", &s.to_string())
            }
            StoreError::R2d2(ref r) => {
                serializer.serialize_newtype_variant("StoreError", 1, "R2d2", &r.to_string())
            }
            StoreError::Crypto => {
                serializer.serialize_newtype_variant("StoreError", 2, "Crypto", "crypto")
            }
            StoreError::NotFound => {
                serializer.serialize_newtype_variant("StoreError", 3, "NotFound", "not_found")
            }
            StoreError::LocalConfig(ref l) => {
                serializer.serialize_newtype_variant("StoreError", 4, "LocalConfig", &l.to_string())
            }
            StoreError::Parse(ref p) => {
                serializer.serialize_newtype_variant("StoreError", 5, "Parse", &p.to_string())
            }
        }
    }
}

pub(crate) type StoreResult<T> = std::result::Result<T, StoreError>;
