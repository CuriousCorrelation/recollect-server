// @generated automatically by Diesel CLI.

pub mod sql_types {
    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "access"))]
    pub struct Access;

    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "action"))]
    pub struct Action;

    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "label"))]
    pub struct Label;

    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "provider"))]
    pub struct Provider;

    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "scope"))]
    pub struct Scope;
}

diesel::table! {
    admin (id) {
        id -> Int8,
        entity_id -> Int8,
        team_id -> Int8,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

diesel::table! {
    ascribe (id) {
        id -> Int8,
        document_id -> Int8,
        team_id -> Int8,
        created_at -> Timestamptz,
    }
}

diesel::table! {
    audit (id) {
        id -> Int8,
        context -> Text,
        version -> Text,
        service_id -> Uuid,
        source -> Jsonb,
        response -> Jsonb,
        timestamp -> Timestamptz,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::Provider;

    auth (secret) {
        #[max_length = 22]
        secret -> Varchar,
        id -> Text,
        name -> Text,
        email -> Text,
        picture -> Nullable<Text>,
        provider -> Provider,
        created_at -> Timestamptz,
        updated_at -> Nullable<Timestamptz>,
        deleted_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    author (id) {
        id -> Int8,
        document_id -> Int8,
        entity_id -> Int8,
        created_at -> Timestamptz,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::Label;
    use super::sql_types::Action;

    blockade (id) {
        id -> Int8,
        originate_label -> Label,
        originate -> Int8,
        action -> Action,
        destinate_label -> Label,
        destinate -> Int8,
        expiry_at -> Nullable<Timestamptz>,
        created_at -> Timestamptz,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::Label;
    use super::sql_types::Access;

    connection (id) {
        id -> Int8,
        originate_label -> Label,
        originate -> Int8,
        access -> Access,
        destinate_label -> Label,
        destinate -> Int8,
        expiry_at -> Nullable<Timestamptz>,
        created_at -> Timestamptz,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use diesel_full_text_search::TsVector;

    document (id) {
        id -> Int8,
        #[max_length = 16]
        handle -> Varchar,
        content -> Jsonb,
        search_index -> TsVector,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        deleted_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    entity (id) {
        id -> Int8,
        #[max_length = 16]
        handle -> Varchar,
        #[max_length = 22]
        secret -> Varchar,
        #[max_length = 64]
        name -> Varchar,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        deleted_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::Label;

    measure (id) {
        id -> Int8,
        entity_id -> Int8,
        label -> Label,
        current -> Int4,
        maximum -> Int4,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

diesel::table! {
    member (id) {
        id -> Int8,
        entity_id -> Int8,
        team_id -> Int8,
        created_at -> Timestamptz,
    }
}

diesel::table! {
    namespace (handle) {
        #[max_length = 16]
        handle -> Varchar,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

diesel::table! {
    object (id) {
        id -> Int8,
        created_at -> Timestamptz,
    }
}

diesel::table! {
    preferred_team (id) {
        id -> Int8,
        entity_id -> Int8,
        team_id -> Int8,
        created_at -> Timestamptz,
        updated_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    requester (id) {
        id -> Int8,
        #[max_length = 22]
        secret -> Varchar,
        host -> Nullable<Text>,
        scheme -> Nullable<Text>,
        remote_addr -> Nullable<Text>,
        created_at -> Timestamptz,
    }
}

diesel::table! {
    session (id) {
        #[max_length = 22]
        id -> Varchar,
        #[max_length = 22]
        secret -> Varchar,
        fetched_at -> Timestamptz,
        created_at -> Timestamptz,
    }
}

diesel::table! {
    tag (id) {
        id -> Int8,
        document_id -> Int8,
        content -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

diesel::table! {
    team (id) {
        id -> Int8,
        #[max_length = 16]
        handle -> Varchar,
        name -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        deleted_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    token (secret) {
        #[max_length = 22]
        secret -> Varchar,
        created_at -> Timestamptz,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::Label;
    use super::sql_types::Scope;

    visibility (id) {
        id -> Int8,
        label -> Label,
        object -> Int8,
        scope -> Scope,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        deleted_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    world (id) {
        id -> Int8,
        #[max_length = 22]
        name -> Varchar,
        created_at -> Timestamptz,
    }
}

diesel::joinable!(admin -> entity (entity_id));
diesel::joinable!(admin -> team (team_id));
diesel::joinable!(ascribe -> document (document_id));
diesel::joinable!(ascribe -> team (team_id));
diesel::joinable!(auth -> token (secret));
diesel::joinable!(author -> document (document_id));
diesel::joinable!(author -> entity (entity_id));
diesel::joinable!(document -> namespace (handle));
diesel::joinable!(document -> object (id));
diesel::joinable!(entity -> namespace (handle));
diesel::joinable!(entity -> object (id));
diesel::joinable!(entity -> token (secret));
diesel::joinable!(measure -> entity (entity_id));
diesel::joinable!(member -> entity (entity_id));
diesel::joinable!(member -> team (team_id));
diesel::joinable!(preferred_team -> entity (entity_id));
diesel::joinable!(preferred_team -> team (team_id));
diesel::joinable!(requester -> token (secret));
diesel::joinable!(session -> token (secret));
diesel::joinable!(tag -> document (document_id));
diesel::joinable!(team -> namespace (handle));
diesel::joinable!(team -> object (id));
diesel::joinable!(visibility -> object (object));
diesel::joinable!(world -> object (id));

diesel::allow_tables_to_appear_in_same_query!(
    admin,
    ascribe,
    audit,
    auth,
    author,
    blockade,
    connection,
    document,
    entity,
    measure,
    member,
    namespace,
    object,
    preferred_team,
    requester,
    session,
    tag,
    team,
    token,
    visibility,
    world,
);
