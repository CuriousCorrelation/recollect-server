use std::task::{ready, Context, Poll};

use futures::{future::OptionFuture, FutureExt};
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::service::{EntityAssure, TeamAssure},
    model::{entity, external, member, ops, team, AuditField, BoxFuture, Request},
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Find<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::member::find"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
    team_assure: TeamAssure<StoreError>,
}

impl<S> Service<Request<external::member::Partial>> for Find<S>
where
    S: Service<ops::Find<Request<member::Partial>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.team_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request:
                external::member::Partial {
                    entity,
                    team,
                    created_at,
                },
        }: Request<external::member::Partial>,
    ) -> Self::Future {
        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let team_id: OptionFuture<_> = team
                .map(|handle| async move {
                    team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(handle),
                        }))
                        .await
                })
                .into();

            let team_id = team_id
                .await
                .transpose()
                .map_err(ExchangeError::Store)?
                .map(|r| r.request);

            let entity_id: OptionFuture<_> = entity
                .map(|handle| async move {
                    entity_assure
                        .call(ops::Assure(Request {
                            who,
                            request: entity::Identify::Handle(handle),
                        }))
                        .await
                })
                .into();

            let entity_id = entity_id
                .await
                .transpose()
                .map_err(ExchangeError::Store)?
                .map(|r| r.request);

            let request = member::Partial {
                team_id,
                entity_id,
                created_at,
            };

            inner
                .call(ops::Find(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
