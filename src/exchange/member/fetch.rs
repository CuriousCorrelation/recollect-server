use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{EntityAssure, MemberAssure, TeamAssure, VisibilityAssure},
    model::{
        entity, external, label::Label, member, ops, scope::Scope, team, visibility, AuditField,
        BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Fetch<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::member::fetch"))]
    context: AuditField,
    inner: S,
    member_assure: MemberAssure<StoreError>,
    team_assure: TeamAssure<StoreError>,
    entity_assure: EntityAssure<StoreError>,
    visibility_assure: VisibilityAssure<StoreError>,
}

impl<S> Service<Request<external::member::Identify>> for Fetch<S>
where
    S: Service<ops::Fetch<Request<member::Identify>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.member_assure.poll_ready(cx))?;
        ready!(self.team_assure.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.visibility_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request { who, request }: Request<external::member::Identify>,
    ) -> Self::Future {
        let member_assure = self.member_assure.clone();
        let mut member_assure = std::mem::replace(&mut self.member_assure, member_assure);

        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let visibility_assure = self.visibility_assure.clone();
        let mut visibility_assure =
            std::mem::replace(&mut self.visibility_assure, visibility_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match request {
                external::member::Identify::Unique { entity, team } => {
                    // `who` can fetch `member`ship status of `team` when either
                    // 1. `team` is `public`, or
                    // 2. `who` is a `member` of `team`,
                    let team_id = team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(team),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if visibility_assure
                        .call(ops::Assure(Request {
                            who,
                            request: visibility::Identify::Unique {
                                object: team_id,
                                scope: Scope::Public,
                                label: Some(Label::Team),
                            },
                        }))
                        .await
                        .is_ok()
                        || member_assure
                            .call(ops::Assure(Request {
                                who,
                                request: member::Identify::Unique {
                                    entity_id: who.entity_id,
                                    team_id,
                                },
                            }))
                            .await
                            .is_ok()
                    {
                        let entity_id = entity_assure
                            .call(ops::Assure(Request {
                                who,
                                request: entity::Identify::Handle(entity),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        let id = member_assure
                            .call(ops::Assure(Request {
                                who,
                                request: member::Identify::Unique { entity_id, team_id },
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        Ok(member::Identify::Id(id))
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Fetch(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
