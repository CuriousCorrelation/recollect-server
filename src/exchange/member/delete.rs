use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{AdminAssure, EntityAssure, MemberAssure, TeamAssure},
    model::{admin, entity, external, member, ops, team, AuditField, BoxFuture, Request},
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Delete<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::member::delete"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
    team_assure: TeamAssure<StoreError>,
    member_assure: MemberAssure<StoreError>,
    admin_assure: AdminAssure<StoreError>,
}

impl<S> Service<Request<external::member::Identify>> for Delete<S>
where
    S: Service<ops::Delete<Request<member::Identify>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.team_assure.poll_ready(cx))?;
        ready!(self.member_assure.poll_ready(cx))?;
        ready!(self.admin_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request { who, request }: Request<external::member::Identify>,
    ) -> Self::Future {
        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let admin_assure = self.admin_assure.clone();
        let mut admin_assure = std::mem::replace(&mut self.admin_assure, admin_assure);

        let member_assure = self.member_assure.clone();
        let mut member_assure = std::mem::replace(&mut self.member_assure, member_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match request {
                external::member::Identify::Unique { entity, team } => {
                    // `who` can revoke `member`ship from `team` when
                    // 1. `who` is the target of revokation,
                    // 2. `who` is the `admin` of `team`
                    let team_id = team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(team),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    let entity_id = entity_assure
                        .call(ops::Assure(Request {
                            who,
                            request: entity::Identify::Handle(entity),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if entity_id == who.entity_id
                        || admin_assure
                            .call(ops::Assure(Request {
                                who,
                                request: admin::Identify::Unique {
                                    entity_id: who.entity_id,
                                    team_id,
                                },
                            }))
                            .await
                            .is_ok()
                    {
                        let id = member_assure
                            .call(ops::Assure(Request {
                                who,
                                request: member::Identify::Unique { entity_id, team_id },
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        Ok(member::Identify::Id(id))
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Delete(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
