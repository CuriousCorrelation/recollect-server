use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{BlockadeAssure, EntityAssure, TeamAssure},
    model::{
        action::Action, blockade, entity, external, label::Label, member, ops, team, AuditField,
        BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Create<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::member::create"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
    team_assure: TeamAssure<StoreError>,
    blockade_assure: BlockadeAssure<StoreError>,
}

impl<S> Service<Request<external::member::Inclusive>> for Create<S>
where
    S: Service<ops::Create<Request<member::Inclusive>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.team_assure.poll_ready(cx))?;
        ready!(self.blockade_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request: external::member::Inclusive { entity, team },
        }: Request<external::member::Inclusive>,
    ) -> Self::Future {
        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let blockade_assure = self.blockade_assure.clone();
        let mut blockade_assure = std::mem::replace(&mut self.blockade_assure, blockade_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let team_id = team_assure
                .call(ops::Assure(Request {
                    who,
                    request: team::Identify::Handle(team),
                }))
                .await
                .map_err(ExchangeError::Store)?
                .request;

            let entity_id = entity_assure
                .call(ops::Assure(Request {
                    who,
                    request: entity::Identify::Handle(entity),
                }))
                .await
                .map_err(ExchangeError::Store)?
                .request;

            let request = if blockade_assure
                .call(ops::Assure(Request {
                    who,
                    request: blockade::Identify::Unique {
                        originate_label: Label::Team,
                        originate: team_id,
                        action: Some(Action::Ascribe),
                        destinate_label: Label::Entity,
                        destinate: entity_id,
                    },
                }))
                .await
                .is_ok()
            {
                Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
            } else {
                Ok(member::Inclusive { entity_id, team_id })
            }?;

            inner
                .call(ops::Create(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
