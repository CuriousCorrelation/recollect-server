use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::EntityAssure,
    model::{
        entity, external,
        ops::{self},
        AuditField, BoxFuture, Request,
    },
    store::error::StoreError,
};
#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct UpdateLayer {
    entity_assure: EntityAssure<StoreError>,
}

impl<S> Layer<S> for UpdateLayer {
    type Service = Update<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Update::builder()
            .inner(inner)
            .entity_assure(self.entity_assure.clone())
            .build()
    }
}

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Update<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::entity::update"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
}

impl<S> Service<Request<external::entity::Exclusive>> for Update<S>
where
    S: Service<ops::Update<Request<entity::Exclusive>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request { who, request }: Request<external::entity::Exclusive>,
    ) -> Self::Future {
        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match request.identify {
                external::entity::Identify::Handle(handle) => {
                    let id = entity_assure
                        .call(ops::Assure(Request {
                            who,
                            request: entity::Identify::Handle(handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    // Only one way `entity` info can be modified,
                    // 1. `who.entity_id` corrosponds to `handle`.
                    let corrosponds = who.entity_id == id;

                    if corrosponds {
                        Ok(entity::Exclusive {
                            id,
                            handle: request.handle,
                            name: request.name,
                        })
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Update(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
