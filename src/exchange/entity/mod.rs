pub(crate) mod delete;
pub(crate) mod fetch;
pub(crate) mod find;
pub(crate) mod update;

pub(crate) use delete::{Delete, DeleteLayer};
pub(crate) use fetch::{Fetch, FetchLayer};
pub(crate) use find::{Find, FindLayer};
pub(crate) use update::{Update, UpdateLayer};

use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::Service;
use typed_builder::TypedBuilder;

use crate::model::{entity, external, AuditField, BoxFuture, Request};

use super::error::ExchangeError;

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Exchange {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::admin::create"))]
    context: AuditField,
}

impl Service<Request<entity::Entity>> for Exchange {
    type Response = external::entity::Entity;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: Request<entity::Entity>) -> Self::Future {
        let entity::Entity { handle, name, .. } = req.request;

        async move { Ok(external::entity::Entity { handle, name }) }.boxed()
    }
}
