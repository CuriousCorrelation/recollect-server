use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{AdminAssure, ConnectionAssure, EntityAssure, MemberAssure, VisibilityAssure},
    model::{
        admin, connection, entity, external,
        label::Label,
        member,
        ops::{self, Assure},
        scope::Scope,
        visibility, AuditField, BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct FetchLayer {
    entity_assure: EntityAssure<StoreError>,
    admin_assure: AdminAssure<StoreError>,
    member_assure: MemberAssure<StoreError>,
    visibility_assure: VisibilityAssure<StoreError>,
    connection_assure: ConnectionAssure<StoreError>,
}

impl<S> Layer<S> for FetchLayer {
    type Service = Fetch<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Fetch::builder()
            .inner(inner)
            .entity_assure(self.entity_assure.clone())
            .admin_assure(self.admin_assure.clone())
            .member_assure(self.member_assure.clone())
            .visibility_assure(self.visibility_assure.clone())
            .connection_assure(self.connection_assure.clone())
            .build()
    }
}

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Fetch<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::entity::fetch"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
    admin_assure: AdminAssure<StoreError>,
    member_assure: MemberAssure<StoreError>,
    visibility_assure: VisibilityAssure<StoreError>,
    connection_assure: ConnectionAssure<StoreError>,
}

impl<S> Service<Request<external::entity::Identify>> for Fetch<S>
where
    S: Service<ops::Fetch<Request<entity::Identify>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.admin_assure.poll_ready(cx))?;
        ready!(self.member_assure.poll_ready(cx))?;
        ready!(self.visibility_assure.poll_ready(cx))?;
        ready!(self.connection_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request { who, request }: Request<external::entity::Identify>,
    ) -> Self::Future {
        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let member_assure = self.member_assure.clone();
        let mut member_assure = std::mem::replace(&mut self.member_assure, member_assure);

        let admin_assure = self.admin_assure.clone();
        let mut admin_assure = std::mem::replace(&mut self.admin_assure, admin_assure);

        let visibility_assure = self.visibility_assure.clone();
        let mut visibility_assure =
            std::mem::replace(&mut self.visibility_assure, visibility_assure);

        let connection_assure = self.connection_assure.clone();
        let mut connection_assure =
            std::mem::replace(&mut self.connection_assure, connection_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match request {
                external::entity::Identify::Handle(handle) => {
                    let id = entity_assure
                        .call(Assure(Request {
                            who,
                            request: entity::Identify::Handle(handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;
                    // Four ways a `who` can fetch `entity`:
                    // 1. either `who.entity_id` corrosponds to `handle`, or
                    let corrosponds = who.entity_id == id;
                    // 2. `who` is the `admin` of `team` `who` is logged into and `entity` is a `member`
                    let is_admin_and_is_member = admin_assure
                        .call(Assure(Request {
                            who,
                            request: admin::Identify::Unique {
                                entity_id: who.entity_id,
                                team_id: who.team_id,
                            },
                        }))
                        .await
                        .is_ok()
                        && member_assure
                            .call(Assure(Request {
                                who,
                                request: member::Identify::Unique {
                                    entity_id: id,
                                    team_id: who.team_id,
                                },
                            }))
                            .await
                            .is_ok();
                    // 3. `handle` has `Public` visibility, or
                    let is_public = visibility_assure
                        .call(Assure(Request {
                            who,
                            request: visibility::Identify::Unique {
                                object: id,
                                scope: Scope::Public,
                                label: Some(Label::Entity),
                            },
                        }))
                        .await
                        .is_ok();
                    // 4. `entity` has some `connection` to `who.entity_id`, or
                    let has_connection = connection_assure
                        .call(Assure(Request {
                            who,
                            request: connection::Identify::Unique {
                                originate_label: Label::Entity,
                                originate: who.entity_id,
                                // None means any.
                                access: None,
                                destinate_label: Label::Entity,
                                destinate: id,
                            },
                        }))
                        .await
                        .is_ok();
                    // 5. `entity` has `TeamOnly` visibility and is a member of `who.team_id`.
                    let is_team_only_and_is_member = visibility_assure
                        .call(Assure(Request {
                            who,
                            request: visibility::Identify::Unique {
                                object: id,
                                scope: Scope::TeamOnly,
                                label: Some(Label::Entity),
                            },
                        }))
                        .await
                        .is_ok()
                        && member_assure
                            .call(Assure(Request {
                                who,
                                request: member::Identify::Unique {
                                    entity_id: id,
                                    team_id: who.team_id,
                                },
                            }))
                            .await
                            .is_ok();

                    if corrosponds
                        || is_public
                        || has_connection
                        || is_team_only_and_is_member
                        || is_admin_and_is_member
                    {
                        Ok(entity::Identify::Id(id))
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Fetch(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
