use std::task::{ready, Context, Poll};

use futures::{future::try_join_all, FutureExt};
use rayon::iter::{IntoParallelIterator, ParallelIterator};
use tower::{Layer, Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{AdminCount, AdminFind, EntityAssure},
    model::{admin, entity, external, ops, AuditField, BoxFuture, PrimaryKey, Request},
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct DeleteLayer {
    entity_assure: EntityAssure<StoreError>,
    admin_count: AdminCount<StoreError>,
    admin_find: AdminFind<StoreError>,
}

impl<S> Layer<S> for DeleteLayer {
    type Service = Delete<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Delete::builder()
            .inner(inner)
            .entity_assure(self.entity_assure.clone())
            .admin_count(self.admin_count.clone())
            .admin_find(self.admin_find.clone())
            .build()
    }
}

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Delete<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::entity::delete"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
    admin_count: AdminCount<StoreError>,
    admin_find: AdminFind<StoreError>,
}

impl<S> Service<Request<external::entity::Identify>> for Delete<S>
where
    S: Service<ops::Delete<Request<entity::Identify>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.admin_count.poll_ready(cx))?;
        ready!(self.admin_find.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request { who, request }: Request<external::entity::Identify>,
    ) -> Self::Future {
        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let admin_find = self.admin_find.clone();
        let mut admin_find = std::mem::replace(&mut self.admin_find, admin_find);

        let admin_count = self.admin_count.clone();
        let mut admin_count = std::mem::replace(&mut self.admin_count, admin_count);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match request {
                external::entity::Identify::Handle(handle) => {
                    let id = entity_assure
                        .call(ops::Assure(Request {
                            who,
                            request: entity::Identify::Handle(handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    // One way a `who` can delete `entity`:
                    // 1. `who.entity_id` corrosponds to `handle`
                    let corrosponds = who.entity_id == id;
                    // 2. Check if all `team`s; where `id` is `admin`, has at least 2 `admin`s.
                    let teams_where_id_is_admin = admin_find
                        .call(ops::Find(Request {
                            who,
                            request: admin::Partial {
                                entity_id: Some(id),
                                team_id: None,
                                created_at: None,
                            },
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request
                        .into_par_iter()
                        .map(|admin| admin.team_id)
                        .collect::<Vec<PrimaryKey>>();

                    let has_at_least_two_admins = try_join_all(
                        teams_where_id_is_admin
                            .into_iter()
                            .map(|team_id| {
                                let mut admin_count = admin_count.clone();

                                async move {
                                    let count = admin_count
                                        .call(ops::Count(Request {
                                            who,
                                            request: admin::Partial {
                                                entity_id: None,
                                                team_id: Some(team_id),
                                                created_at: None,
                                            },
                                        }))
                                        .await
                                        .map_err(ExchangeError::Store)?
                                        .request;

                                    Ok::<_, ExchangeError>(count)
                                }
                            })
                            .collect::<Vec<_>>(),
                    );

                    if corrosponds
                        && has_at_least_two_admins
                            .await?
                            .into_par_iter()
                            .all(|x| x >= 2)
                    {
                        Ok(entity::Identify::Id(id))
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Delete(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
