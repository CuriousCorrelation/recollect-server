use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{
        AscribeAssure, AuthorAssure, ConnectionAssure, DocumentAssure, TeamAssure, VisibilityAssure,
    },
    model::{
        ascribe, author, connection, document, external, label::Label, ops, scope::Scope, team,
        visibility, AuditField, BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Fetch<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::admin::create"))]
    context: AuditField,
    inner: S,
    team_assure: TeamAssure<StoreError>,
    document_assure: DocumentAssure<StoreError>,
    visibility_assure: VisibilityAssure<StoreError>,
    author_assure: AuthorAssure<StoreError>,
    ascribe_assure: AscribeAssure<StoreError>,
    connection_assure: ConnectionAssure<StoreError>,
}

impl<S> Service<Request<external::ascribe::Identify>> for Fetch<S>
where
    S: Service<ops::Fetch<Request<ascribe::Identify>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx));
        ready!(self.team_assure.poll_ready(cx));
        ready!(self.document_assure.poll_ready(cx));
        ready!(self.visibility_assure.poll_ready(cx));
        ready!(self.author_assure.poll_ready(cx));
        ready!(self.ascribe_assure.poll_ready(cx));
        ready!(self.connection_assure.poll_ready(cx));

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request { who, request }: Request<external::ascribe::Identify>,
    ) -> Self::Future {
        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let visibility_assure = self.visibility_assure.clone();
        let mut visibility_assure =
            std::mem::replace(&mut self.visibility_assure, visibility_assure);

        let author_assure = self.author_assure.clone();
        let mut author_assure = std::mem::replace(&mut self.author_assure, author_assure);

        let ascribe_assure = self.ascribe_assure.clone();
        let mut ascribe_assure = std::mem::replace(&mut self.ascribe_assure, ascribe_assure);

        let connection_assure = self.connection_assure.clone();
        let mut connection_assure =
            std::mem::replace(&mut self.connection_assure, connection_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match request {
                external::ascribe::Identify::Unique { document, team } => {
                    let document_id = document_assure
                        .call(ops::Assure(Request {
                            who,
                            request: document::Identify::Handle(document),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    let team_id = team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(team),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    // `who` can fetch `ascribe` of `document` if
                    // 1. `document` is public
                    // 2. `who` is the `author` of `document`
                    // 3. `who` belongs to `team` `document` is `ascribe`d to
                    // 4. `who` has a `connection` to `document`
                    // 5. `who`s `team` has a `connection` to `document`
                    // 6. `visibility` is `TeamOnly` and `who` is member of `ascribe`d team
                    if visibility_assure
                        .call(ops::Assure(Request {
                            who,
                            request: visibility::Identify::Unique {
                                object: document_id,
                                scope: Scope::Public,
                                label: Some(Label::Document),
                            },
                        }))
                        .await
                        .is_ok()
                        || author_assure
                            .call(ops::Assure(Request {
                                who,
                                request: author::Identify::Unique {
                                    document_id,
                                    entity_id: who.entity_id,
                                },
                            }))
                            .await
                            .is_ok()
                        || ascribe_assure
                            .call(ops::Assure(Request {
                                who,
                                request: ascribe::Identify::Unique {
                                    document_id,
                                    team_id: who.team_id,
                                },
                            }))
                            .await
                            .is_ok()
                        || connection_assure
                            .call(ops::Assure(Request {
                                who,
                                request: connection::Identify::Unique {
                                    originate_label: Label::Entity,
                                    originate: who.entity_id,
                                    // None means any.
                                    access: None,
                                    destinate_label: Label::Document,
                                    destinate: document_id,
                                },
                            }))
                            .await
                            .is_ok()
                        || connection_assure
                            .call(ops::Assure(Request {
                                who,
                                request: connection::Identify::Unique {
                                    originate_label: Label::Team,
                                    originate: who.team_id,
                                    // None means any.
                                    access: None,
                                    destinate_label: Label::Document,
                                    destinate: document_id,
                                },
                            }))
                            .await
                            .is_ok()
                    {
                        Ok(ascribe::Identify::Unique {
                            document_id,
                            team_id,
                        })
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Fetch(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
