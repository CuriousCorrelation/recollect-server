use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    model::service::{DocumentMask, TeamMask},
    model::{ascribe, document, external, ops, team, AuditField, BoxFuture, Request},
    store::error::StoreError,
};

use super::error::ExchangeError;

pub(crate) mod create;
pub(crate) mod delete;
pub(crate) mod fetch;
pub(crate) mod find;

pub(crate) use create::Create;
pub(crate) use delete::Delete;
pub(crate) use fetch::Fetch;
pub(crate) use find::Find;

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Exchange {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::admin::create"))]
    context: AuditField,
    team_mask: TeamMask<StoreError>,
    document_mask: DocumentMask<StoreError>,
}

impl Service<Request<ascribe::Ascribe>> for Exchange {
    type Response = external::ascribe::Ascribe;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.team_mask.poll_ready(cx))?;
        ready!(self.document_mask.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request:
                ascribe::Ascribe {
                    id: _,
                    document_id,
                    team_id,
                    created_at,
                },
        }: Request<ascribe::Ascribe>,
    ) -> Self::Future {
        let document_mask = self.document_mask.clone();
        let mut document_mask = std::mem::replace(&mut self.document_mask, document_mask);

        let team_mask = self.team_mask.clone();
        let mut team_mask = std::mem::replace(&mut self.team_mask, team_mask);

        async move {
            let team = team_mask
                .call(ops::Mask(Request {
                    who,
                    request: team::Identify::Id(team_id),
                }))
                .await
                .map_err(ExchangeError::Store)?
                .request;

            let document = document_mask
                .call(ops::Mask(Request {
                    who,
                    request: document::Identify::Id(document_id),
                }))
                .await
                .map_err(ExchangeError::Store)?
                .request;

            Ok(external::ascribe::Ascribe {
                document,
                team,
                created_at,
            })
        }
        .boxed()
    }
}
