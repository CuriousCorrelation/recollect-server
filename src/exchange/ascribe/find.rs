use std::task::{ready, Context, Poll};

use futures::{future::OptionFuture, FutureExt};
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::service::{DocumentAssure, TeamAssure},
    model::{ascribe, document, external, ops, team, AuditField, BoxFuture, Request},
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Find<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::admin::create"))]
    context: AuditField,
    inner: S,
    team_assure: TeamAssure<StoreError>,
    document_assure: DocumentAssure<StoreError>,
}

impl<S> Service<Request<external::ascribe::Partial>> for Find<S>
where
    S: Service<ops::Find<Request<ascribe::Partial>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx));
        ready!(self.team_assure.poll_ready(cx));
        ready!(self.document_assure.poll_ready(cx));

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request:
                external::ascribe::Partial {
                    document,
                    team,
                    created_at,
                },
        }: Request<external::ascribe::Partial>,
    ) -> Self::Future {
        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let document_id: OptionFuture<_> = document
                .map(|handle| async move {
                    document_assure
                        .call(ops::Assure(Request {
                            who,
                            request: document::Identify::Handle(handle),
                        }))
                        .await
                })
                .into();

            let document_id = document_id
                .await
                .transpose()
                .map_err(ExchangeError::Store)?
                .map(|r| r.request);

            let team_id: OptionFuture<_> = team
                .map(|handle| async move {
                    team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(handle),
                        }))
                        .await
                })
                .into();

            let team_id = team_id
                .await
                .transpose()
                .map_err(ExchangeError::Store)?
                .map(|r| r.request);

            let request = ascribe::Partial {
                document_id,
                team_id,
                created_at,
            };

            inner
                .call(ops::Find(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
