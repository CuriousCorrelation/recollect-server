use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::service::{DocumentAssure, EntityAssure, TeamAssure},
    model::{
        connection, document, entity, external, label::Label, ops, team, AuditField, BoxFuture,
        Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Find<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::connection::find"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
    team_assure: TeamAssure<StoreError>,
    document_assure: DocumentAssure<StoreError>,
}

impl<S> Service<Request<external::connection::Partial>> for Find<S>
where
    S: Service<ops::Find<Request<connection::Partial>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.team_assure.poll_ready(cx))?;
        ready!(self.document_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request:
                external::connection::Partial {
                    originate,
                    access,
                    destinate,
                    expiry_at,
                },
        }: Request<external::connection::Partial>,
    ) -> Self::Future {
        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        let from = originate;
        let to = destinate;

        async move {
            let request = match (from, to) {
                (None, None) => Ok::<_, ExchangeError>(connection::Partial {
                    originate_label: None,
                    originate: None,
                    access,
                    destinate_label: None,
                    destinate: None,
                    expiry_at,
                }),
                (None, Some(to)) => match to {
                    external::composite_identify::CompositeIdentify::Entity(entity) => match entity
                    {
                        external::entity::Identify::Handle(handle) => {
                            let destinate = entity_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: entity::Identify::Handle(handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let destinate_label = Some(Label::Entity);
                            let destinate = Some(destinate);

                            Ok(connection::Partial {
                                originate_label: None,
                                originate: None,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        }
                    },
                    external::composite_identify::CompositeIdentify::Team(team) => match team {
                        external::team::Identify::Handle(handle) => {
                            let destinate = team_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: team::Identify::Handle(handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let destinate_label = Some(Label::Team);
                            let destinate = Some(destinate);

                            Ok(connection::Partial {
                                originate_label: None,
                                originate: None,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        }
                    },
                    external::composite_identify::CompositeIdentify::Document(document) => {
                        match document {
                            external::document::Identify::Handle(handle) => {
                                let destinate = document_assure
                                    .call(ops::Assure(Request {
                                        who,
                                        request: document::Identify::Handle(handle),
                                    }))
                                    .await
                                    .map_err(ExchangeError::Store)?
                                    .request;

                                let destinate_label = Some(Label::Document);
                                let destinate = Some(destinate);

                                Ok(connection::Partial {
                                    originate_label: None,
                                    originate: None,
                                    access,
                                    destinate_label,
                                    destinate,
                                    expiry_at,
                                })
                            }
                        }
                    }
                },
                (Some(from), None) => match from {
                    external::composite_identify::CompositeIdentify::Entity(entity) => match entity
                    {
                        external::entity::Identify::Handle(handle) => {
                            let destinate = entity_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: entity::Identify::Handle(handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let destinate_label = Some(Label::Entity);
                            let destinate = Some(destinate);

                            Ok(connection::Partial {
                                originate_label: None,
                                originate: None,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        }
                    },
                    external::composite_identify::CompositeIdentify::Team(team) => match team {
                        external::team::Identify::Handle(handle) => {
                            let destinate = team_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: team::Identify::Handle(handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let destinate_label = Some(Label::Team);
                            let destinate = Some(destinate);

                            Ok(connection::Partial {
                                originate_label: None,
                                originate: None,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        }
                    },
                    external::composite_identify::CompositeIdentify::Document(document) => {
                        match document {
                            external::document::Identify::Handle(handle) => {
                                let destinate = document_assure
                                    .call(ops::Assure(Request {
                                        who,
                                        request: document::Identify::Handle(handle),
                                    }))
                                    .await
                                    .map_err(ExchangeError::Store)?
                                    .request;

                                let destinate_label = Some(Label::Document);
                                let destinate = Some(destinate);

                                Ok(connection::Partial {
                                    originate_label: None,
                                    originate: None,
                                    access,
                                    destinate_label,
                                    destinate,
                                    expiry_at,
                                })
                            }
                        }
                    }
                },
                (Some(from), Some(to)) => match (from, to) {
                    (
                        external::composite_identify::CompositeIdentify::Entity(entity_1),
                        external::composite_identify::CompositeIdentify::Entity(entity_2),
                    ) => match (entity_1, entity_2) {
                        (
                            external::entity::Identify::Handle(entity_1_handle),
                            external::entity::Identify::Handle(entity_2_handle),
                        ) => {
                            let originate = entity_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: entity::Identify::Handle(entity_1_handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let destinate = entity_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: entity::Identify::Handle(entity_2_handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let originate_label = Some(Label::Entity);
                            let originate = Some(originate);

                            let destinate_label = Some(Label::Entity);
                            let destinate = Some(destinate);

                            Ok(connection::Partial {
                                originate_label,
                                originate,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        }
                    },
                    (
                        external::composite_identify::CompositeIdentify::Entity(entity),
                        external::composite_identify::CompositeIdentify::Team(team),
                    ) => match (entity, team) {
                        (
                            external::entity::Identify::Handle(entity_handle),
                            external::team::Identify::Handle(team_handle),
                        ) => {
                            let originate = entity_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: entity::Identify::Handle(entity_handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let destinate = team_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: team::Identify::Handle(team_handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let originate_label = Some(Label::Entity);
                            let originate = Some(originate);

                            let destinate_label = Some(Label::Team);
                            let destinate = Some(destinate);

                            Ok(connection::Partial {
                                originate_label,
                                originate,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        }
                    },
                    (
                        external::composite_identify::CompositeIdentify::Entity(entity),
                        external::composite_identify::CompositeIdentify::Document(document),
                    ) => match (entity, document) {
                        (
                            external::entity::Identify::Handle(entity_handle),
                            external::document::Identify::Handle(document_handle),
                        ) => {
                            let originate = entity_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: entity::Identify::Handle(entity_handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let destinate = document_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: document::Identify::Handle(document_handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let originate_label = Some(Label::Entity);
                            let originate = Some(originate);

                            let destinate_label = Some(Label::Document);
                            let destinate = Some(destinate);

                            Ok(connection::Partial {
                                originate_label,
                                originate,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        }
                    },
                    (
                        external::composite_identify::CompositeIdentify::Team(_team),
                        external::composite_identify::CompositeIdentify::Entity(_entity),
                    ) => unreachable!(
                        "A `Team` cannot have `Read`, `ReadWrite`, ... `connection` to `Entity`"
                    ),
                    (
                        external::composite_identify::CompositeIdentify::Team(team_1),
                        external::composite_identify::CompositeIdentify::Team(team_2),
                    ) => match (team_1, team_2) {
                        (
                            external::team::Identify::Handle(team_1_handle),
                            external::team::Identify::Handle(team_2_handle),
                        ) => {
                            let originate = team_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: team::Identify::Handle(team_1_handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let destinate = team_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: team::Identify::Handle(team_2_handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let originate_label = Some(Label::Team);
                            let originate = Some(originate);

                            let destinate_label = Some(Label::Team);
                            let destinate = Some(destinate);

                            Ok(connection::Partial {
                                originate_label,
                                originate,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        }
                    },
                    (
                        external::composite_identify::CompositeIdentify::Team(team),
                        external::composite_identify::CompositeIdentify::Document(document),
                    ) => match (team, document) {
                        (
                            external::team::Identify::Handle(team_handle),
                            external::document::Identify::Handle(document_handle),
                        ) => {
                            let originate = team_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: team::Identify::Handle(team_handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let destinate = document_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: document::Identify::Handle(document_handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let originate_label = Some(Label::Team);
                            let originate = Some(originate);

                            let destinate_label = Some(Label::Document);
                            let destinate = Some(destinate);

                            Ok(connection::Partial {
                                originate_label,
                                originate,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        }
                    },
                    (
                        external::composite_identify::CompositeIdentify::Document(_document),
                        external::composite_identify::CompositeIdentify::Entity(_entity),
                    ) => unreachable!(
                    "A `Document` cannot have `Read`, `ReadWrite`, ... `connection` to `Entity`"
                ),
                    (
                        external::composite_identify::CompositeIdentify::Document(_document),
                        external::composite_identify::CompositeIdentify::Team(_team),
                    ) => unreachable!(
                        "A `Document` cannot have `Read`, `ReadWrite`, ... `connection` to `Team`"
                    ),
                    (
                        external::composite_identify::CompositeIdentify::Document(document_1),
                        external::composite_identify::CompositeIdentify::Document(document_2),
                    ) => match (document_1, document_2) {
                        (
                            external::document::Identify::Handle(document_1_handle),
                            external::document::Identify::Handle(document_2_handle),
                        ) => {
                            let originate = document_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: document::Identify::Handle(document_1_handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let destinate = document_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: document::Identify::Handle(document_2_handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            let originate_label = Some(Label::Document);
                            let originate = Some(originate);

                            let destinate_label = Some(Label::Document);
                            let destinate = Some(destinate);

                            Ok(connection::Partial {
                                originate_label,
                                originate,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        }
                    },
                },
            }?;

            inner
                .call(ops::Find(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
