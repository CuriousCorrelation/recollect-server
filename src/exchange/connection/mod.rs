pub(crate) mod create;
pub(crate) mod delete;
pub(crate) mod fetch;
pub(crate) mod find;
pub(crate) mod update;

pub(crate) use create::Create;
pub(crate) use delete::Delete;
pub(crate) use fetch::Fetch;
pub(crate) use find::Find;
pub(crate) use update::Update;

use std::task::{ready, Context, Poll};

use futures::FutureExt;

use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::service::{DocumentMask, EntityMask, TeamMask},
    model::{
        connection, document, entity,
        external::{self, composite_identify::CompositeIdentify},
        label::Label,
        ops, team, AuditField, BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Exchange {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::admin::create"))]
    context: AuditField,

    team_mask: TeamMask<StoreError>,
    entity_mask: EntityMask<StoreError>,
    document_mask: DocumentMask<StoreError>,
}

impl Service<Request<connection::Connection>> for Exchange {
    type Response = external::connection::Connection;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.team_mask.poll_ready(cx))?;
        ready!(self.entity_mask.poll_ready(cx))?;
        ready!(self.document_mask.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request:
                connection::Connection {
                    id: _,
                    originate_label,
                    originate,
                    access,
                    destinate_label,
                    destinate,
                    expiry_at,
                    created_at,
                },
        }: Request<connection::Connection>,
    ) -> Self::Future {
        let team_mask = self.team_mask.clone();
        let mut team_mask = std::mem::replace(&mut self.team_mask, team_mask);

        let entity_mask = self.entity_mask.clone();
        let mut entity_mask = std::mem::replace(&mut self.entity_mask, entity_mask);

        let document_mask = self.document_mask.clone();
        let mut document_mask = std::mem::replace(&mut self.document_mask, document_mask);

        async move {
            let originate = match originate_label {
                Label::Team => CompositeIdentify::Team(external::team::Identify::Handle(
                    team_mask
                        .call(ops::Mask(Request {
                            who,
                            request: team::Identify::Id(originate),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request,
                )),
                Label::Entity => CompositeIdentify::Entity(external::entity::Identify::Handle(
                    entity_mask
                        .call(ops::Mask(Request {
                            who,
                            request: entity::Identify::Id(originate),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request,
                )),
                Label::Document => {
                    CompositeIdentify::Document(external::document::Identify::Handle(
                        document_mask
                            .call(ops::Mask(Request {
                                who,
                                request: document::Identify::Id(originate),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request,
                    ))
                }
            };

            let destinate = match destinate_label {
                Label::Team => CompositeIdentify::Team(external::team::Identify::Handle(
                    team_mask
                        .call(ops::Mask(Request {
                            who,
                            request: team::Identify::Id(destinate),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request,
                )),
                Label::Entity => CompositeIdentify::Entity(external::entity::Identify::Handle(
                    entity_mask
                        .call(ops::Mask(Request {
                            who,
                            request: entity::Identify::Id(destinate),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request,
                )),
                Label::Document => {
                    CompositeIdentify::Document(external::document::Identify::Handle(
                        document_mask
                            .call(ops::Mask(Request {
                                who,
                                request: document::Identify::Id(destinate),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request,
                    ))
                }
            };

            Ok(external::connection::Connection {
                originate,
                access,
                destinate,
                expiry_at,
                created_at,
            })
        }
        .boxed()
    }
}
