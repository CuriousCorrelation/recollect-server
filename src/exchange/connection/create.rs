use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{
        AuthorAssure, ConnectionAssure, DocumentAssure, EntityAssure, MemberAssure, TeamAssure,
    },
    model::{
        author, connection, document, entity, external, label::Label, member, ops, team,
        AuditField, BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Create<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::connection::create"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
    team_assure: TeamAssure<StoreError>,
    document_assure: DocumentAssure<StoreError>,
    connection_assure: ConnectionAssure<StoreError>,
    member_assure: MemberAssure<StoreError>,
    author_assure: AuthorAssure<StoreError>,
}

impl<S> Service<Request<external::connection::Inclusive>> for Create<S>
where
    S: Service<ops::Create<Request<connection::Inclusive>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.team_assure.poll_ready(cx))?;
        ready!(self.document_assure.poll_ready(cx))?;
        ready!(self.connection_assure.poll_ready(cx))?;
        ready!(self.member_assure.poll_ready(cx))?;
        ready!(self.author_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request:
                external::connection::Inclusive {
                    originate,
                    access,
                    destinate,
                    expiry_at,
                },
        }: Request<external::connection::Inclusive>,
    ) -> Self::Future {
        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let connection_assure = self.connection_assure.clone();
        let mut connection_assure =
            std::mem::replace(&mut self.connection_assure, connection_assure);

        let member_assure = self.member_assure.clone();
        let mut member_assure = std::mem::replace(&mut self.member_assure, member_assure);

        let author_assure = self.author_assure.clone();
        let mut author_assure = std::mem::replace(&mut self.author_assure, author_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let from = originate;
            let to = destinate;

            let request = match (from, to) {
                (
                    external::composite_identify::CompositeIdentify::Entity(entity_1),
                    external::composite_identify::CompositeIdentify::Entity(entity_2),
                ) => match (entity_1, entity_2) {
                    (
                        external::entity::Identify::Handle(entity_1_handle),
                        external::entity::Identify::Handle(entity_2_handle),
                    ) => {
                        let originate = entity_assure
                            .call(ops::Assure(Request {
                                who,
                                request: entity::Identify::Handle(entity_1_handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        let destinate = entity_assure
                            .call(ops::Assure(Request {
                                who,
                                request: entity::Identify::Handle(entity_2_handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        if destinate == who.entity_id {
                            let originate_label = Label::Entity;
                            let destinate_label = Label::Entity;

                            Ok(connection::Inclusive {
                                originate_label,
                                originate,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        } else {
                            Err(ExchangeError::AccessViolation(AccessViolation::Target))
                        }
                    }
                },
                (
                    external::composite_identify::CompositeIdentify::Entity(entity),
                    external::composite_identify::CompositeIdentify::Team(team),
                ) => match (entity, team) {
                    (
                        external::entity::Identify::Handle(entity_handle),
                        external::team::Identify::Handle(team_handle),
                    ) => {
                        let originate = entity_assure
                            .call(ops::Assure(Request {
                                who,
                                request: entity::Identify::Handle(entity_handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        let destinate = team_assure
                            .call(ops::Assure(Request {
                                who,
                                request: team::Identify::Handle(team_handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        if member_assure
                            .call(ops::Assure(Request {
                                who,
                                request: member::Identify::Unique {
                                    team_id: destinate,
                                    entity_id: who.entity_id,
                                },
                            }))
                            .await
                            .is_ok()
                        {
                            let originate_label = Label::Entity;
                            let destinate_label = Label::Team;

                            Ok(connection::Inclusive {
                                originate_label,
                                originate,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        } else {
                            Err(ExchangeError::AccessViolation(AccessViolation::Member))
                        }
                    }
                },
                (
                    external::composite_identify::CompositeIdentify::Entity(entity),
                    external::composite_identify::CompositeIdentify::Document(document),
                ) => match (entity, document) {
                    (
                        external::entity::Identify::Handle(entity_handle),
                        external::document::Identify::Handle(document_handle),
                    ) => {
                        let originate = entity_assure
                            .call(ops::Assure(Request {
                                who,
                                request: entity::Identify::Handle(entity_handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        tracing::debug!("originate: {:#?}", &originate);

                        let destinate = document_assure
                            .call(ops::Assure(Request {
                                who,
                                request: document::Identify::Handle(document_handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        tracing::debug!("destinate: {:#?}", &destinate);

                        let is_author = author_assure
                            .call(ops::Assure(Request {
                                who,
                                request: author::Identify::Unique {
                                    document_id: destinate,
                                    entity_id: who.entity_id,
                                },
                            }))
                            .await
                            .is_ok();

                        tracing::debug!("is_author: {:#?}", &is_author);

                        if is_author {
                            let originate_label = Label::Entity;
                            let destinate_label = Label::Document;

                            Ok(connection::Inclusive {
                                originate_label,
                                originate,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        } else {
                            Err(ExchangeError::AccessViolation(AccessViolation::Author))
                        }
                    }
                },
                (
                    external::composite_identify::CompositeIdentify::Team(_team),
                    external::composite_identify::CompositeIdentify::Entity(_entity),
                ) => unreachable!(
                    "A `Team` cannot have `Read`, `ReadWrite`, ... `connection` to `Entity`"
                ),
                (
                    external::composite_identify::CompositeIdentify::Team(team_1),
                    external::composite_identify::CompositeIdentify::Team(team_2),
                ) => match (team_1, team_2) {
                    (
                        external::team::Identify::Handle(team_1_handle),
                        external::team::Identify::Handle(team_2_handle),
                    ) => {
                        let originate = team_assure
                            .call(ops::Assure(Request {
                                who,
                                request: team::Identify::Handle(team_1_handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        let destinate = team_assure
                            .call(ops::Assure(Request {
                                who,
                                request: team::Identify::Handle(team_2_handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        if member_assure
                            .call(ops::Assure(Request {
                                who,
                                request: member::Identify::Unique {
                                    team_id: destinate,
                                    entity_id: who.entity_id,
                                },
                            }))
                            .await
                            .is_ok()
                        {
                            let originate_label = Label::Team;
                            let destinate_label = Label::Team;

                            Ok(connection::Inclusive {
                                originate_label,
                                originate,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        } else {
                            Err(ExchangeError::AccessViolation(AccessViolation::Member))
                        }
                    }
                },
                (
                    external::composite_identify::CompositeIdentify::Team(team),
                    external::composite_identify::CompositeIdentify::Document(document),
                ) => match (team, document) {
                    (
                        external::team::Identify::Handle(team_handle),
                        external::document::Identify::Handle(document_handle),
                    ) => {
                        let originate = team_assure
                            .call(ops::Assure(Request {
                                who,
                                request: team::Identify::Handle(team_handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        let destinate = document_assure
                            .call(ops::Assure(Request {
                                who,
                                request: document::Identify::Handle(document_handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        if author_assure
                            .call(ops::Assure(Request {
                                who,
                                request: author::Identify::Unique {
                                    document_id: destinate,
                                    entity_id: who.entity_id,
                                },
                            }))
                            .await
                            .is_ok()
                        {
                            let originate_label = Label::Team;
                            let destinate_label = Label::Document;

                            Ok(connection::Inclusive {
                                originate_label,
                                originate,
                                access,
                                destinate_label,
                                destinate,
                                expiry_at,
                            })
                        } else {
                            Err(ExchangeError::AccessViolation(AccessViolation::Author))
                        }
                    }
                },
                (
                    external::composite_identify::CompositeIdentify::Document(_document),
                    external::composite_identify::CompositeIdentify::Entity(_entity),
                ) => unreachable!(
                    "A `Document` cannot have `Read`, `ReadWrite`, ... `connection` to `Entity`"
                ),
                (
                    external::composite_identify::CompositeIdentify::Document(_document),
                    external::composite_identify::CompositeIdentify::Team(_team),
                ) => unreachable!(
                    "A `Document` cannot have `Read`, `ReadWrite`, ... `connection` to `Team`"
                ),
                (
                    external::composite_identify::CompositeIdentify::Document(document_1),
                    external::composite_identify::CompositeIdentify::Document(document_2),
                ) => match (document_1, document_2) {
                    (
                        external::document::Identify::Handle(document_1_handle),
                        external::document::Identify::Handle(document_2_handle),
                    ) => {
                        let originate = document_assure
                            .call(ops::Assure(Request {
                                who,
                                request: document::Identify::Handle(document_1_handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        let destinate = document_assure
                            .call(ops::Assure(Request {
                                who,
                                request: document::Identify::Handle(document_2_handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        if author_assure
                            .call(ops::Assure(Request {
                                who,
                                request: author::Identify::Unique {
                                    document_id: destinate,
                                    entity_id: who.entity_id,
                                },
                            }))
                            .await
                            .is_ok()
                        {
                            let originate_label = Label::Document;
                            let destinate_label = Label::Document;

                            if author_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: author::Identify::Unique {
                                        document_id: originate,
                                        entity_id: who.entity_id,
                                    },
                                }))
                                .await
                                .is_ok()
                            {
                                Ok(connection::Inclusive {
                                    originate_label,
                                    originate,
                                    access,
                                    destinate_label,
                                    destinate,
                                    expiry_at,
                                })
                            } else {
                                let _connection = connection_assure
                                    .call(ops::Assure(Request {
                                        who,
                                        request: connection::Identify::Unique {
                                            originate_label: Label::Entity,
                                            originate: who.entity_id,
                                            access: None,
                                            destinate_label: Label::Document,
                                            destinate: originate,
                                        },
                                    }))
                                    .await
                                    .map_err(ExchangeError::Store)?
                                    .request;

                                Ok(connection::Inclusive {
                                    originate_label,
                                    originate,
                                    access,
                                    destinate_label,
                                    destinate,
                                    expiry_at,
                                })
                            }
                        } else {
                            Err(ExchangeError::AccessViolation(AccessViolation::Author))
                        }
                    }
                },
            }?;

            inner
                .call(ops::Create(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
