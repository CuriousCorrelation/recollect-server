use serde::Serialize;
use thiserror::Error;

use crate::{error, store::error::StoreError};

#[derive(Debug, Serialize, Error)]
pub(crate) enum ExchangeError {
    #[error("Store - {0}")]
    Store(#[from] StoreError),

    #[error("SerDe - {0}")]
    #[serde(serialize_with = "serde_json_error")]
    Serde(#[from] serde_json::Error),

    #[error("Access violation - {0}")]
    AccessViolation(error::AccessViolation),
}

fn serde_json_error<S>(serde: &serde_json::Error, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    serializer.serialize_str(&serde.to_string())
}

pub(super) type ExchangeResult<T> = std::result::Result<T, ExchangeError>;
