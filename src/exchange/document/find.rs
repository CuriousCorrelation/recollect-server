use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service};
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::{document, external, ops, AuditField, BoxFuture, Request},
};

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct FindLayer {}

impl<S> Layer<S> for FindLayer {
    type Service = Find<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Find::builder().inner(inner).build()
    }
}

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Find<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::document::find"))]
    context: AuditField,
    inner: S,
}

impl<S> Service<Request<external::document::Partial>> for Find<S>
where
    S: Service<ops::Find<Request<document::Partial>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request:
                external::document::Partial {
                    content,
                    created_at,
                    updated_at,
                },
        }: Request<external::document::Partial>,
    ) -> Self::Future {
        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = document::Partial {
                content,
                created_at,
                updated_at,
            };

            inner
                .call(ops::Find(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
