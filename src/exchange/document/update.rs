use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{AuthorAssure, ConnectionAssure, DocumentAssure},
    model::{
        access::Access, author, connection, document, external, label::Label, ops, AuditField,
        BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct UpdateLayer {
    document_assure: DocumentAssure<StoreError>,
    author_assure: AuthorAssure<StoreError>,
    connection_assure: ConnectionAssure<StoreError>,
}

impl<S> Layer<S> for UpdateLayer {
    type Service = Update<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Update::builder()
            .inner(inner)
            .document_assure(self.document_assure.clone())
            .author_assure(self.author_assure.clone())
            .connection_assure(self.connection_assure.clone())
            .build()
    }
}

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Update<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::document::update"))]
    context: AuditField,
    inner: S,
    document_assure: DocumentAssure<StoreError>,
    author_assure: AuthorAssure<StoreError>,
    connection_assure: ConnectionAssure<StoreError>,
}

impl<S> Service<Request<external::document::Exclusive>> for Update<S>
where
    S: Service<ops::Update<Request<document::Exclusive>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.document_assure.poll_ready(cx))?;
        ready!(self.author_assure.poll_ready(cx))?;
        ready!(self.connection_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request: external::document::Exclusive { identify, content },
        }: Request<external::document::Exclusive>,
    ) -> Self::Future {
        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let author_assure = self.author_assure.clone();
        let mut author_assure = std::mem::replace(&mut self.author_assure, author_assure);

        let connection_assure = self.connection_assure.clone();
        let mut connection_assure =
            std::mem::replace(&mut self.connection_assure, connection_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match identify {
                external::document::Identify::Handle(handle) => {
                    let id = document_assure
                        .call(ops::Assure(Request {
                            who,
                            request: document::Identify::Handle(handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    // Two ways a `document` can be modified by a `who`:
                    // 1. either `who.entity_id` the author, or
                    // 2. `who.entity_id` has `ReadWrite` access.
                    if author_assure
                        .call(ops::Assure(Request {
                            who,
                            request: author::Identify::Unique {
                                document_id: id,
                                entity_id: who.entity_id,
                            },
                        }))
                        .await
                        .is_ok()
                        || connection_assure
                            .call(ops::Assure(Request {
                                who,
                                request: connection::Identify::Unique {
                                    originate_label: Label::Entity,
                                    originate: who.entity_id,
                                    access: Some(Access::ReadWrite),
                                    destinate_label: Label::Document,
                                    destinate: id,
                                },
                            }))
                            .await
                            .is_ok()
                    {
                        Ok(document::Exclusive {
                            id,
                            content,
                            handle,
                        })
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Update(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
