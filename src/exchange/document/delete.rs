use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{AuthorAssure, DocumentAssure},
    model::{author, document, external, ops, AuditField, BoxFuture, Request},
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct DeleteLayer {
    document_assure: DocumentAssure<StoreError>,
    author_assure: AuthorAssure<StoreError>,
}

impl<S> Layer<S> for DeleteLayer {
    type Service = Delete<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Delete::builder()
            .inner(inner)
            .document_assure(self.document_assure.clone())
            .author_assure(self.author_assure.clone())
            .build()
    }
}

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Delete<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::document::delete"))]
    context: AuditField,
    inner: S,
    document_assure: DocumentAssure<StoreError>,
    author_assure: AuthorAssure<StoreError>,
}

impl<S> Service<Request<external::document::Identify>> for Delete<S>
where
    S: Service<ops::Delete<Request<document::Identify>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.document_assure.poll_ready(cx))?;
        ready!(self.author_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request { who, request }: Request<external::document::Identify>,
    ) -> Self::Future {
        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let author_assure = self.author_assure.clone();
        let mut author_assure = std::mem::replace(&mut self.author_assure, author_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match request {
                external::document::Identify::Handle(handle) => {
                    let id = document_assure
                        .call(ops::Assure(Request {
                            who,
                            request: document::Identify::Handle(handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    // One way a `document` can be deleted by a `who`:
                    // 1. `who.entity_id` the author
                    if author_assure
                        .call(ops::Assure(Request {
                            who,
                            request: author::Identify::Unique {
                                document_id: id,
                                entity_id: who.entity_id,
                            },
                        }))
                        .await
                        .is_ok()
                    {
                        Ok(document::Identify::Id(id))
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Delete(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
