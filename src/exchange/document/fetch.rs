use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{
        AscribeFetch, AuthorAssure, BlockadeAssure, ConnectionAssure, DocumentAssure,
        VisibilityAssure,
    },
    model::{
        action::Action, ascribe, author, blockade, connection, document, external, label::Label,
        ops, scope::Scope, visibility, AuditField, BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct FetchLayer {
    document_assure: DocumentAssure<StoreError>,
    visibility_assure: VisibilityAssure<StoreError>,
    author_assure: AuthorAssure<StoreError>,
    connection_assure: ConnectionAssure<StoreError>,
    ascribe_fetch: AscribeFetch<StoreError>,
    blockade_assure: BlockadeAssure<StoreError>,
}

impl<S> Layer<S> for FetchLayer {
    type Service = Fetch<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Fetch::builder()
            .inner(inner)
            .document_assure(self.document_assure.clone())
            .visibility_assure(self.visibility_assure.clone())
            .author_assure(self.author_assure.clone())
            .connection_assure(self.connection_assure.clone())
            .ascribe_fetch(self.ascribe_fetch.clone())
            .blockade_assure(self.blockade_assure.clone())
            .build()
    }
}

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Fetch<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::document::fetch"))]
    context: AuditField,
    inner: S,
    document_assure: DocumentAssure<StoreError>,
    visibility_assure: VisibilityAssure<StoreError>,
    author_assure: AuthorAssure<StoreError>,
    connection_assure: ConnectionAssure<StoreError>,
    ascribe_fetch: AscribeFetch<StoreError>,
    blockade_assure: BlockadeAssure<StoreError>,
}

impl<S> Service<Request<external::document::Identify>> for Fetch<S>
where
    S: Service<ops::Fetch<Request<document::Identify>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.document_assure.poll_ready(cx))?;
        ready!(self.visibility_assure.poll_ready(cx))?;
        ready!(self.author_assure.poll_ready(cx))?;
        ready!(self.connection_assure.poll_ready(cx))?;
        ready!(self.ascribe_fetch.poll_ready(cx))?;
        ready!(self.blockade_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request { who, request }: Request<external::document::Identify>,
    ) -> Self::Future {
        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let visibility_assure = self.visibility_assure.clone();
        let mut visibility_assure =
            std::mem::replace(&mut self.visibility_assure, visibility_assure);

        let author_assure = self.author_assure.clone();
        let mut author_assure = std::mem::replace(&mut self.author_assure, author_assure);

        let connection_assure = self.connection_assure.clone();
        let mut connection_assure =
            std::mem::replace(&mut self.connection_assure, connection_assure);

        let ascribe_fetch = self.ascribe_fetch.clone();
        let mut ascribe_fetch = std::mem::replace(&mut self.ascribe_fetch, ascribe_fetch);

        let blockade_assure = self.blockade_assure.clone();
        let mut blockade_assure = std::mem::replace(&mut self.blockade_assure, blockade_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match request {
                external::document::Identify::Handle(handle) => {
                    let id = document_assure
                        .call(ops::Assure(Request {
                            who,
                            request: document::Identify::Handle(handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    let ascribe = ascribe_fetch
                        .call(ops::Fetch(Request {
                            who,
                            request: ascribe::Identify::DocumentId(id),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    // Three ways a `document` can be read by a `who`:
                    // 1. either the `document` is `public`, or
                    let is_public = visibility_assure
                        .call(ops::Assure(Request {
                            who,
                            request: visibility::Identify::Unique {
                                object: id,
                                scope: Scope::Public,
                                label: Some(Label::Document),
                            },
                        }))
                        .await
                        .is_ok();
                    // 2. `who.entity_id` the author, or
                    let is_author = author_assure
                        .call(ops::Assure(Request {
                            who,
                            request: author::Identify::Unique {
                                document_id: id,
                                entity_id: who.entity_id,
                            },
                        }))
                        .await
                        .is_ok();
                    // 3. `document` is `TeamOnly` and `who` belongs to `ascribe` `team`.
                    let is_team_only = ascribe.document_id == id
                        && ascribe.team_id == who.team_id
                        && visibility_assure
                            .call(ops::Assure(Request {
                                who,
                                request: visibility::Identify::Unique {
                                    object: id,
                                    scope: Scope::TeamOnly,
                                    label: Some(Label::Document),
                                },
                            }))
                            .await
                            .is_ok();
                    // 4. `who.entity_id` has at least `Read` access
                    let is_connected_directly = connection_assure
                        .call(ops::Assure(Request {
                            who,
                            request: connection::Identify::Unique {
                                originate_label: Label::Entity,
                                originate: who.entity_id,
                                // None means any.
                                access: None,
                                destinate_label: Label::Document,
                                destinate: id,
                            },
                        }))
                        .await
                        .is_ok();
                    // 5. `who.entity_id` has connection to `document`'s `ascribe`d `team`
                    let is_connected_to_team_via_entity = connection_assure
                        .call(ops::Assure(Request {
                            who,
                            request: connection::Identify::Unique {
                                originate_label: Label::Entity,
                                originate: who.entity_id,
                                // None means any.
                                access: None,
                                destinate_label: Label::Team,
                                destinate: ascribe.team_id,
                            },
                        }))
                        .await
                        .is_ok();

                    // 6. `who.team_id` has connection to `document`'s `ascribe`d `team`
                    let is_connected_to_team_via_team = connection_assure
                        .call(ops::Assure(Request {
                            who,
                            request: connection::Identify::Unique {
                                originate_label: Label::Team,
                                originate: who.team_id,
                                // None means any.
                                access: None,
                                destinate_label: Label::Team,
                                destinate: ascribe.team_id,
                            },
                        }))
                        .await
                        .is_ok();
                    // 7. And there's no `blockade` on `who`
                    let is_not_blocked = blockade_assure
                        .call(ops::Assure(Request {
                            who,
                            request: blockade::Identify::Unique {
                                originate_label: Label::Entity,
                                originate: who.entity_id,
                                action: Some(Action::ReadWrite),
                                destinate_label: Label::Document,
                                destinate: id,
                            },
                        }))
                        .await
                        .is_err()
                        && blockade_assure
                            .call(ops::Assure(Request {
                                who,
                                request: blockade::Identify::Unique {
                                    originate_label: Label::Team,
                                    originate: who.team_id,
                                    action: Some(Action::ReadWrite),
                                    destinate_label: Label::Document,
                                    destinate: id,
                                },
                            }))
                            .await
                            .is_err();

                    if (is_public
                        || is_author
                        || is_team_only
                        || is_connected_directly
                        || is_connected_to_team_via_entity
                        || is_connected_to_team_via_team)
                        && is_not_blocked
                    {
                        Ok(document::Identify::Id(id))
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Fetch(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
