use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::service::{DocumentAssure, EntityAssure, TeamAssure},
    model::{
        document, entity, external, label::Label, ops, team, visibility, AuditField, BoxFuture,
        Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Fetch<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::visibility::fetch"))]
    context: AuditField,
    inner: S,
    team_assure: TeamAssure<StoreError>,
    entity_assure: EntityAssure<StoreError>,
    document_assure: DocumentAssure<StoreError>,
}

impl<S> Service<Request<external::visibility::Identify>> for Fetch<S>
where
    S: Service<ops::Fetch<Request<visibility::Identify>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.team_assure.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.document_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request { who, request }: Request<external::visibility::Identify>,
    ) -> Self::Future {
        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            // Anyone can view `visibility` status of an object.
            // NOTE: Perhaps to permissive?
            // One reason to keep it permissive is viewing a say `Private` `document`
            // should tell user that the document is set to private, maybe try contacting the author,
            // or a document is set to `TeamOnly`, so maybe try contacting a team member.
            let request = match request {
                external::visibility::Identify::Object(object) => {
                    let object = match object {
                        external::composite_identify::CompositeIdentify::Entity(
                            external::entity::Identify::Handle(handle),
                        ) => {
                            entity_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: entity::Identify::Handle(handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request
                        }
                        external::composite_identify::CompositeIdentify::Team(
                            external::team::Identify::Handle(handle),
                        ) => {
                            team_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: team::Identify::Handle(handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request
                        }
                        external::composite_identify::CompositeIdentify::Document(
                            external::document::Identify::Handle(handle),
                        ) => {
                            document_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: document::Identify::Handle(handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request
                        }
                    };

                    Ok::<_, ExchangeError>(visibility::Identify::Object(object))
                }
                external::visibility::Identify::Unique { object, scope } => {
                    let (object, label) = match object {
                        external::composite_identify::CompositeIdentify::Entity(
                            external::entity::Identify::Handle(handle),
                        ) => (
                            entity_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: entity::Identify::Handle(handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request,
                            Label::Entity,
                        ),
                        external::composite_identify::CompositeIdentify::Team(
                            external::team::Identify::Handle(handle),
                        ) => (
                            team_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: team::Identify::Handle(handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request,
                            Label::Team,
                        ),
                        external::composite_identify::CompositeIdentify::Document(
                            external::document::Identify::Handle(handle),
                        ) => (
                            document_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: document::Identify::Handle(handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request,
                            Label::Document,
                        ),
                    };

                    let label = Some(label);

                    Ok::<_, ExchangeError>(visibility::Identify::Unique {
                        object,
                        scope,
                        label,
                    })
                }
            }?;

            inner
                .call(ops::Fetch(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
