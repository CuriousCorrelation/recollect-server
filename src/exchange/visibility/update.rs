use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{AuthorAssure, DocumentAssure, EntityAssure, TeamAssure, VisibilityAssure},
    model::{
        author, document, entity, external, label::Label, ops, team, visibility, AuditField,
        BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Update<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::visibility::update"))]
    context: AuditField,
    inner: S,
    visibility_assure: VisibilityAssure<StoreError>,
    team_assure: TeamAssure<StoreError>,
    entity_assure: EntityAssure<StoreError>,
    document_assure: DocumentAssure<StoreError>,
    author_assure: AuthorAssure<StoreError>,
}

impl<S> Service<Request<external::visibility::Exclusive>> for Update<S>
where
    S: Service<ops::Update<Request<visibility::Exclusive>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.visibility_assure.poll_ready(cx))?;
        ready!(self.team_assure.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.document_assure.poll_ready(cx))?;
        ready!(self.author_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request: external::visibility::Exclusive { object, scope },
        }: Request<external::visibility::Exclusive>,
    ) -> Self::Future {
        let visibility_assure = self.visibility_assure.clone();
        let mut visibility_assure =
            std::mem::replace(&mut self.visibility_assure, visibility_assure);

        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let author_assure = self.author_assure.clone();
        let mut author_assure = std::mem::replace(&mut self.author_assure, author_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = {
                let (object, label) = match object {
                    external::composite_identify::CompositeIdentify::Entity(
                        external::entity::Identify::Handle(handle),
                    ) => {
                        // Only one way `visibility` of `entity` can be modified by `who`:
                        // 1. `who.entity_id` is the target `entity`
                        let id = entity_assure
                            .call(ops::Assure(Request {
                                who,
                                request: entity::Identify::Handle(handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        if who.entity_id == id {
                            Ok((id, Label::Entity))
                        } else {
                            Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                        }
                    }
                    external::composite_identify::CompositeIdentify::Team(
                        external::team::Identify::Handle(handle),
                    ) => {
                        // Only one way `visibility` of `team` can be modified by `who`:
                        // 1. `who.team_id` is the target and `who` is logged into `team_id`.
                        let id = team_assure
                            .call(ops::Assure(Request {
                                who,
                                request: team::Identify::Handle(handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        if who.team_id == id {
                            Ok((id, Label::Team))
                        } else {
                            Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                        }
                    }
                    external::composite_identify::CompositeIdentify::Document(
                        external::document::Identify::Handle(handle),
                    ) => {
                        // Only one way `visibility` of `document` can be modified by `who`:
                        // 1. `who` is the `author` of `document`
                        let id = document_assure
                            .call(ops::Assure(Request {
                                who,
                                request: document::Identify::Handle(handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        if author_assure
                            .call(ops::Assure(Request {
                                who,
                                request: author::Identify::Unique {
                                    document_id: id,
                                    entity_id: who.entity_id,
                                },
                            }))
                            .await
                            .is_ok()
                        {
                            Ok((id, Label::Document))
                        } else {
                            Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                        }
                    }
                }?;

                let id = visibility_assure
                    .call(ops::Assure(Request {
                        who,
                        request: visibility::Identify::Object(object),
                    }))
                    .await
                    .map_err(ExchangeError::Store)?
                    .request;

                Ok::<_, ExchangeError>(visibility::Exclusive {
                    id,
                    label: Some(label),
                    scope,
                })
            }?;

            inner
                .call(ops::Update(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
