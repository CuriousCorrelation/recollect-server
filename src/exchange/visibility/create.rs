use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{AdminAssure, AuthorAssure, DocumentAssure, EntityAssure, TeamAssure},
    model::{
        admin, author, document, entity, external, label::Label, ops, team, visibility, AuditField,
        BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Create<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::visibility::create"))]
    context: AuditField,
    inner: S,
    team_assure: TeamAssure<StoreError>,
    entity_assure: EntityAssure<StoreError>,
    document_assure: DocumentAssure<StoreError>,
    admin_assure: AdminAssure<StoreError>,
    author_assure: AuthorAssure<StoreError>,
}

impl<S> Service<Request<external::visibility::Inclusive>> for Create<S>
where
    S: Service<ops::Create<Request<visibility::Inclusive>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.team_assure.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.document_assure.poll_ready(cx))?;
        ready!(self.admin_assure.poll_ready(cx))?;
        ready!(self.author_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request: external::visibility::Inclusive { object, scope },
        }: Request<external::visibility::Inclusive>,
    ) -> Self::Future {
        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let admin_assure = self.admin_assure.clone();
        let mut admin_assure = std::mem::replace(&mut self.admin_assure, admin_assure);

        let author_assure = self.author_assure.clone();
        let mut author_assure = std::mem::replace(&mut self.author_assure, author_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match object {
                external::composite_identify::CompositeIdentify::Entity(entity) => match entity {
                    external::entity::Identify::Handle(handle) => {
                        let id = entity_assure
                            .call(ops::Assure(Request {
                                who,
                                request: entity::Identify::Handle(handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        if who.entity_id == id {
                            Ok(visibility::Inclusive {
                                label: Label::Entity,
                                object: id,
                                scope,
                            })
                        } else {
                            Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                        }
                    }
                },
                external::composite_identify::CompositeIdentify::Team(team) => match team {
                    external::team::Identify::Handle(handle) => {
                        let id = team_assure
                            .call(ops::Assure(Request {
                                who,
                                request: team::Identify::Handle(handle),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        if admin_assure
                            .call(ops::Assure(Request {
                                who,
                                request: admin::Identify::Unique {
                                    entity_id: who.entity_id,
                                    team_id: id,
                                },
                            }))
                            .await
                            .is_ok()
                        {
                            Ok(visibility::Inclusive {
                                label: Label::Team,
                                object: id,
                                scope,
                            })
                        } else {
                            Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                        }
                    }
                },
                external::composite_identify::CompositeIdentify::Document(document) => {
                    match document {
                        external::document::Identify::Handle(handle) => {
                            let id = document_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: document::Identify::Handle(handle),
                                }))
                                .await
                                .map_err(ExchangeError::Store)?
                                .request;

                            if author_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: author::Identify::Unique {
                                        entity_id: who.entity_id,
                                        document_id: id,
                                    },
                                }))
                                .await
                                .is_ok()
                            {
                                Ok(visibility::Inclusive {
                                    label: Label::Document,
                                    object: id,
                                    scope,
                                })
                            } else {
                                Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                            }
                        }
                    }
                }
            }?;

            inner
                .call(ops::Create(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
