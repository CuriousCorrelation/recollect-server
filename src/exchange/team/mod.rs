pub(crate) mod create;
pub(crate) mod delete;
pub(crate) mod fetch;
pub(crate) mod find;
pub(crate) mod update;

pub(crate) use create::{Create, CreateLayer};
pub(crate) use delete::{Delete, DeleteLayer};
pub(crate) use fetch::{Fetch, FetchLayer};
pub(crate) use find::{Find, FindLayer};
pub(crate) use update::{Update, UpdateLayer};

use std::task::{ready, Context, Poll};

use futures::FutureExt;

use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::{external, team, AuditField, BoxFuture, Request},
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Exchange {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::admin::create"))]
    context: AuditField,
}

impl Service<Request<team::Team>> for Exchange {
    type Response = external::team::Team;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who: _,
            request:
                team::Team {
                    id: _,
                    handle,
                    name,
                },
        }: Request<team::Team>,
    ) -> Self::Future {
        async move { Ok(external::team::Team { handle, name }) }.boxed()
    }
}
