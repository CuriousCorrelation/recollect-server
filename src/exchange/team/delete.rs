use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{AdminAssure, TeamAssure},
    model::{admin, external, ops, team, AuditField, BoxFuture, Request},
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct DeleteLayer {
    team_assure: TeamAssure<StoreError>,
    admin_assure: AdminAssure<StoreError>,
}

impl<S> Layer<S> for DeleteLayer {
    type Service = Delete<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Delete::builder()
            .inner(inner)
            .team_assure(self.team_assure.clone())
            .admin_assure(self.admin_assure.clone())
            .build()
    }
}

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Delete<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::team::delete"))]
    context: AuditField,
    inner: S,
    team_assure: TeamAssure<StoreError>,
    admin_assure: AdminAssure<StoreError>,
}

impl<S> Service<Request<external::team::Identify>> for Delete<S>
where
    S: Service<ops::Delete<Request<team::Identify>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.team_assure.poll_ready(cx))?;
        ready!(self.admin_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request { who, request }: Request<external::team::Identify>,
    ) -> Self::Future {
        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let admin_assure = self.admin_assure.clone();
        let mut admin_assure = std::mem::replace(&mut self.admin_assure, admin_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            // `who` can delete `team` if
            // 1. `who` is `admin` of `team`
            let request = match request {
                external::team::Identify::Handle(handle) => {
                    let id = team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if admin_assure
                        .call(ops::Assure(Request {
                            who,
                            request: admin::Identify::Unique {
                                entity_id: who.entity_id,
                                team_id: id,
                            },
                        }))
                        .await
                        .is_ok()
                    {
                        Ok(team::Identify::Id(id))
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Delete(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
