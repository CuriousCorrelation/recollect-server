use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::{
        action::Action,
        blockade, connection, external,
        label::Label,
        member, ops,
        scope::Scope,
        service::{BlockadeAssure, ConnectionAssure, MemberAssure, TeamAssure, VisibilityAssure},
        team, visibility, AuditField, BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct FetchLayer {
    team_assure: TeamAssure<StoreError>,
    visibility_assure: VisibilityAssure<StoreError>,
    member_assure: MemberAssure<StoreError>,
    connection_assure: ConnectionAssure<StoreError>,
    blockade_assure: BlockadeAssure<StoreError>,
}

impl<S> Layer<S> for FetchLayer {
    type Service = Fetch<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Fetch::builder()
            .inner(inner)
            .team_assure(self.team_assure.clone())
            .visibility_assure(self.visibility_assure.clone())
            .member_assure(self.member_assure.clone())
            .connection_assure(self.connection_assure.clone())
            .blockade_assure(self.blockade_assure.clone())
            .build()
    }
}

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Fetch<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::team::fetch"))]
    context: AuditField,
    inner: S,
    team_assure: TeamAssure<StoreError>,
    visibility_assure: VisibilityAssure<StoreError>,
    member_assure: MemberAssure<StoreError>,
    connection_assure: ConnectionAssure<StoreError>,
    blockade_assure: BlockadeAssure<StoreError>,
}

impl<S> Service<Request<external::team::Identify>> for Fetch<S>
where
    S: Service<ops::Fetch<Request<team::Identify>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.team_assure.poll_ready(cx))?;
        ready!(self.visibility_assure.poll_ready(cx))?;
        ready!(self.member_assure.poll_ready(cx))?;
        ready!(self.connection_assure.poll_ready(cx))?;
        ready!(self.blockade_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request { who, request }: Request<external::team::Identify>,
    ) -> Self::Future {
        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let visibility_assure = self.visibility_assure.clone();
        let mut visibility_assure =
            std::mem::replace(&mut self.visibility_assure, visibility_assure);

        let member_assure = self.member_assure.clone();
        let mut member_assure = std::mem::replace(&mut self.member_assure, member_assure);

        let connection_assure = self.connection_assure.clone();
        let mut connection_assure =
            std::mem::replace(&mut self.connection_assure, connection_assure);

        let blockade_assure = self.blockade_assure.clone();
        let mut blockade_assure = std::mem::replace(&mut self.blockade_assure, blockade_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match request {
                external::team::Identify::Handle(handle) => {
                    let id = team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    // Three ways a `team` can be read by a `who`:
                    // 1. either the `team` is `public`, or
                    // 2. `who.entity_id` the member, or
                    // 3. `who.entity_id` has at least `Read` access
                    // 4. And there's no `blockade` on `who`
                    if (visibility_assure
                        .call(ops::Assure(Request {
                            who,
                            request: visibility::Identify::Unique {
                                object: id,
                                scope: Scope::Public,
                                label: Some(Label::Team),
                            },
                        }))
                        .await
                        .is_ok()
                        || member_assure
                            .call(ops::Assure(Request {
                                who,
                                request: member::Identify::Unique {
                                    team_id: id,
                                    entity_id: who.entity_id,
                                },
                            }))
                            .await
                            .is_ok()
                        || connection_assure
                            .call(ops::Assure(Request {
                                who,
                                request: connection::Identify::Unique {
                                    originate_label: Label::Entity,
                                    originate: who.entity_id,
                                    // None means any.
                                    access: None,
                                    destinate_label: Label::Team,
                                    destinate: id,
                                },
                            }))
                            .await
                            .is_ok())
                        && (blockade_assure
                            .call(ops::Assure(Request {
                                who,
                                request: blockade::Identify::Unique {
                                    originate_label: Label::Entity,
                                    originate: who.entity_id,
                                    action: Some(Action::ReadWrite),
                                    destinate_label: Label::Team,
                                    destinate: id,
                                },
                            }))
                            .await
                            .is_err()
                            && blockade_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: blockade::Identify::Unique {
                                        originate_label: Label::Team,
                                        originate: who.team_id,
                                        action: Some(Action::ReadWrite),
                                        destinate_label: Label::Team,
                                        destinate: id,
                                    },
                                }))
                                .await
                                .is_err())
                    {
                        Ok(team::Identify::Id(id))
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Fetch(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
