use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{ConnectionAssure, MemberAssure, TeamAssure},
    model::{
        access::Access, connection, external, label::Label, member, ops, team, AuditField,
        BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct UpdateLayer {
    team_assure: TeamAssure<StoreError>,
    member_assure: MemberAssure<StoreError>,
    connection_assure: ConnectionAssure<StoreError>,
}

impl<S> Layer<S> for UpdateLayer {
    type Service = Update<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Update::builder()
            .inner(inner)
            .team_assure(self.team_assure.clone())
            .member_assure(self.member_assure.clone())
            .connection_assure(self.connection_assure.clone())
            .build()
    }
}

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Update<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::team::update"))]
    context: AuditField,
    inner: S,
    team_assure: TeamAssure<StoreError>,
    member_assure: MemberAssure<StoreError>,
    connection_assure: ConnectionAssure<StoreError>,
}

impl<S> Service<Request<external::team::Exclusive>> for Update<S>
where
    S: Service<ops::Update<Request<team::Exclusive>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.team_assure.poll_ready(cx))?;
        ready!(self.member_assure.poll_ready(cx))?;
        ready!(self.connection_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request: external::team::Exclusive { identify, name },
        }: Request<external::team::Exclusive>,
    ) -> Self::Future {
        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let member_assure = self.member_assure.clone();
        let mut member_assure = std::mem::replace(&mut self.member_assure, member_assure);

        let connection_assure = self.connection_assure.clone();
        let mut connection_assure =
            std::mem::replace(&mut self.connection_assure, connection_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match identify {
                external::team::Identify::Handle(handle) => {
                    let id = team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    // Two ways a `team` can be modified by a `who`:
                    // 1. either `who.entity_id` is member of `id`, or
                    // 2. `who.entity_id` has `ReadWrite` access.
                    if member_assure
                        .call(ops::Assure(Request {
                            who,
                            request: member::Identify::Unique {
                                entity_id: who.entity_id,
                                team_id: id,
                            },
                        }))
                        .await
                        .is_ok()
                        || connection_assure
                            .call(ops::Assure(Request {
                                who,
                                request: connection::Identify::Unique {
                                    originate_label: Label::Entity,
                                    originate: who.entity_id,
                                    access: Some(Access::ReadWrite),
                                    destinate_label: Label::Team,
                                    destinate: id,
                                },
                            }))
                            .await
                            .is_ok()
                    {
                        Ok(team::Exclusive { id, name })
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Update(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
