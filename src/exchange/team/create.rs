use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::service::{MeasureFetch, MeasureUpdate, NamespaceCreate, ObjectCreate},
    model::{
        external, label::Label, measure, namespace, object, ops, team, AuditField, BoxFuture,
        Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct CreateLayer {
    object_create: ObjectCreate<StoreError>,
    namespace_create: NamespaceCreate<StoreError>,
    measure_fetch: MeasureFetch<StoreError>,
    measure_update: MeasureUpdate<StoreError>,
}

impl<S> Layer<S> for CreateLayer {
    type Service = Create<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Create::builder()
            .inner(inner)
            .object_create(self.object_create.clone())
            .namespace_create(self.namespace_create.clone())
            .measure_fetch(self.measure_fetch.clone())
            .measure_update(self.measure_update.clone())
            .build()
    }
}

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Create<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::team::create"))]
    context: AuditField,
    inner: S,
    object_create: ObjectCreate<StoreError>,
    namespace_create: NamespaceCreate<StoreError>,
    measure_fetch: MeasureFetch<StoreError>,
    measure_update: MeasureUpdate<StoreError>,
}

impl<S> Service<Request<external::team::Inclusive>> for Create<S>
where
    S: Service<ops::Create<Request<team::Inclusive>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.object_create.poll_ready(cx))?;
        ready!(self.namespace_create.poll_ready(cx))?;
        ready!(self.measure_fetch.poll_ready(cx))?;
        ready!(self.measure_update.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request: external::team::Inclusive { name },
        }: Request<external::team::Inclusive>,
    ) -> Self::Future {
        let object_create = self.object_create.clone();
        let mut object_create = std::mem::replace(&mut self.object_create, object_create);

        let namespace_create = self.namespace_create.clone();
        let mut namespace_create = std::mem::replace(&mut self.namespace_create, namespace_create);

        let measure_fetch = self.measure_fetch.clone();
        let mut measure_fetch = std::mem::replace(&mut self.measure_fetch, measure_fetch);

        let measure_update = self.measure_update.clone();
        let mut measure_update = std::mem::replace(&mut self.measure_update, measure_update);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            // Make sure `who` hasn't reached `team` creation limit.
            let measure = measure_fetch
                .call(ops::Fetch(Request {
                    who,
                    request: measure::Identify::Unique {
                        entity_id: who.entity_id,
                        label: Label::Team,
                    },
                }))
                .await
                .map_err(ExchangeError::Store)?
                .request;

            // Increase measure count.
            let _measure = measure_update
                .call(ops::Update(Request {
                    who,
                    request: measure::Exclusive {
                        id: measure.id,
                        current: Some(measure.current + 1),
                        maximum: None,
                    },
                }))
                .await
                .map_err(ExchangeError::Store)?
                .request;

            let team_object = object_create
                .call(ops::Create(Request {
                    who,
                    request: object::Inclusive {},
                }))
                .await
                .map_err(ExchangeError::Store)?
                .request;

            let team_id = team_object.id;

            let team_namespace = namespace_create
                .call(ops::Create(Request {
                    who,
                    request: namespace::Inclusive {},
                }))
                .await
                .map_err(ExchangeError::Store)?
                .request;

            let team_handle = team_namespace.handle;

            let request = team::Inclusive {
                id: team_id,
                handle: team_handle,
                name,
            };

            inner
                .call(ops::Create(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
