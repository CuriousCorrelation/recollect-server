use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{AdminAssure, AuthorAssure, DocumentAssure, EntityAssure, TeamAssure},
    model::{
        admin, author, document, entity, external, namespace, ops, team, AuditField, BoxFuture,
        Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Update<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::namespace::update"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
    team_assure: TeamAssure<StoreError>,
    admin_assure: AdminAssure<StoreError>,
    document_assure: DocumentAssure<StoreError>,
    author_assure: AuthorAssure<StoreError>,
}

impl<S> Service<Request<external::namespace::Exclusive>> for Update<S>
where
    S: Service<ops::Update<Request<namespace::Exclusive>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.team_assure.poll_ready(cx))?;
        ready!(self.admin_assure.poll_ready(cx))?;
        ready!(self.document_assure.poll_ready(cx))?;
        ready!(self.author_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request:
                external::namespace::Exclusive {
                    id: current_handle,
                    handle: new_handle,
                },
        }: Request<external::namespace::Exclusive>,
    ) -> Self::Future {
        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let admin_assure = self.admin_assure.clone();
        let mut admin_assure = std::mem::replace(&mut self.admin_assure, admin_assure);

        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let author_assure = self.author_assure.clone();
        let mut author_assure = std::mem::replace(&mut self.author_assure, author_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match current_handle {
                external::namespace::Identify::Document(current_handle) => {
                    let id = document_assure
                        .call(ops::Assure(Request {
                            who,
                            request: document::Identify::Handle(current_handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if author_assure
                        .call(ops::Assure(Request {
                            who,
                            request: author::Identify::Unique {
                                document_id: id,
                                entity_id: who.entity_id,
                            },
                        }))
                        .await
                        .is_ok()
                    {
                        Ok(namespace::Exclusive {
                            id: current_handle,
                            handle: new_handle,
                        })
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
                external::namespace::Identify::Entity(current_handle) => {
                    let id = entity_assure
                        .call(ops::Assure(Request {
                            who,
                            request: entity::Identify::Handle(current_handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if id == who.entity_id {
                        Ok(namespace::Exclusive {
                            id: current_handle,
                            handle: new_handle,
                        })
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
                external::namespace::Identify::Team(current_handle) => {
                    let id = team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(current_handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if admin_assure
                        .call(ops::Assure(Request {
                            who,
                            request: admin::Identify::Unique {
                                entity_id: who.entity_id,
                                team_id: id,
                            },
                        }))
                        .await
                        .is_ok()
                    {
                        Ok(namespace::Exclusive {
                            id: current_handle,
                            handle: new_handle,
                        })
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Update(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
