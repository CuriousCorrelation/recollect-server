use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{
        AscribeAssure, AuthorAssure, ConnectionAssure, DocumentAssure, EntityAssure, MemberAssure,
        TeamAssure, VisibilityAssure,
    },
    model::{
        ascribe, author, connection, document, entity, external, label::Label, member, namespace,
        ops, scope::Scope, team, visibility, AuditField, BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Fetch<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::namespace::fetch"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
    document_assure: DocumentAssure<StoreError>,
    team_assure: TeamAssure<StoreError>,
    visibility_assure: VisibilityAssure<StoreError>,
    ascribe_assure: AscribeAssure<StoreError>,
    author_assure: AuthorAssure<StoreError>,
    connection_assure: ConnectionAssure<StoreError>,
    member_assure: MemberAssure<StoreError>,
}

impl<S> Service<Request<external::namespace::Identify>> for Fetch<S>
where
    S: Service<ops::Fetch<Request<namespace::Identify>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx).map_err(Into::into)
    }

    fn call(
        &mut self,
        Request { who, request }: Request<external::namespace::Identify>,
    ) -> Self::Future {
        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let visibility_assure = self.visibility_assure.clone();
        let mut visibility_assure =
            std::mem::replace(&mut self.visibility_assure, visibility_assure);

        let ascribe_assure = self.ascribe_assure.clone();
        let mut ascribe_assure = std::mem::replace(&mut self.ascribe_assure, ascribe_assure);

        let author_assure = self.author_assure.clone();
        let mut author_assure = std::mem::replace(&mut self.author_assure, author_assure);

        let connection_assure = self.connection_assure.clone();
        let mut connection_assure =
            std::mem::replace(&mut self.connection_assure, connection_assure);

        let member_assure = self.member_assure.clone();
        let mut member_assure = std::mem::replace(&mut self.member_assure, member_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match request {
                external::namespace::Identify::Document(handle) => {
                    let id = document_assure
                        .call(ops::Assure(Request {
                            who,
                            request: document::Identify::Handle(handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if visibility_assure
                        .call(ops::Assure(Request {
                            who,
                            request: visibility::Identify::Unique {
                                object: id,
                                scope: Scope::Public,
                                label: Some(Label::Document),
                            },
                        }))
                        .await
                        .is_ok()
                        || author_assure
                            .call(ops::Assure(Request {
                                who,
                                request: author::Identify::Unique {
                                    document_id: id,
                                    entity_id: who.entity_id,
                                },
                            }))
                            .await
                            .is_ok()
                        || connection_assure
                            .call(ops::Assure(Request {
                                who,
                                request: connection::Identify::Unique {
                                    originate_label: Label::Entity,
                                    originate: who.entity_id,
                                    // None means any.
                                    access: None,
                                    destinate_label: Label::Document,
                                    destinate: id,
                                },
                            }))
                            .await
                            .is_ok()
                        || (ascribe_assure
                            .call(ops::Assure(Request {
                                who,
                                request: ascribe::Identify::Unique {
                                    document_id: id,
                                    team_id: who.team_id,
                                },
                            }))
                            .await
                            .is_ok()
                            && visibility_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: visibility::Identify::Unique {
                                        object: id,
                                        scope: Scope::TeamOnly,
                                        label: Some(Label::Document),
                                    },
                                }))
                                .await
                                .is_ok())
                    {
                        Ok(namespace::Identify::Document(handle))
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
                external::namespace::Identify::Entity(handle) => {
                    let id = entity_assure
                        .call(ops::Assure(Request {
                            who,
                            request: entity::Identify::Handle(handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if who.entity_id == id
                        || visibility_assure
                            .call(ops::Assure(Request {
                                who,
                                request: visibility::Identify::Unique {
                                    object: id,
                                    scope: Scope::Public,
                                    label: Some(Label::Entity),
                                },
                            }))
                            .await
                            .is_ok()
                        || connection_assure
                            .call(ops::Assure(Request {
                                who,
                                request: connection::Identify::Unique {
                                    originate_label: Label::Entity,
                                    originate: who.entity_id,
                                    // None means any.
                                    access: None,
                                    destinate_label: Label::Entity,
                                    destinate: id,
                                },
                            }))
                            .await
                            .is_ok()
                        || (visibility_assure
                            .call(ops::Assure(Request {
                                who,
                                request: visibility::Identify::Unique {
                                    object: id,
                                    scope: Scope::TeamOnly,
                                    label: Some(Label::Entity),
                                },
                            }))
                            .await
                            .is_ok()
                            && member_assure
                                .call(ops::Assure(Request {
                                    who,
                                    request: member::Identify::Unique {
                                        entity_id: id,
                                        team_id: who.team_id,
                                    },
                                }))
                                .await
                                .is_ok())
                    {
                        Ok(namespace::Identify::Entity(handle))
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
                external::namespace::Identify::Team(handle) => {
                    let id = team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if visibility_assure
                        .call(ops::Assure(Request {
                            who,
                            request: visibility::Identify::Unique {
                                object: id,
                                scope: Scope::Public,
                                label: Some(Label::Team),
                            },
                        }))
                        .await
                        .is_ok()
                        || member_assure
                            .call(ops::Assure(Request {
                                who,
                                request: member::Identify::Unique {
                                    team_id: id,
                                    entity_id: who.entity_id,
                                },
                            }))
                            .await
                            .is_ok()
                        || connection_assure
                            .call(ops::Assure(Request {
                                who,
                                request: connection::Identify::Unique {
                                    originate_label: Label::Entity,
                                    originate: who.entity_id,
                                    // None means any.
                                    access: None,
                                    destinate_label: Label::Team,
                                    destinate: id,
                                },
                            }))
                            .await
                            .is_ok()
                    {
                        Ok(namespace::Identify::Team(handle))
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Fetch(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
