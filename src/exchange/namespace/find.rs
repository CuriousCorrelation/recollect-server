use std::task::{Context, Poll};

use futures::FutureExt;
use tower::Service;
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::{external, namespace, ops, AuditField, BoxFuture, Request},
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Find<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::namespace::find"))]
    context: AuditField,
    inner: S,
}

impl<S> Service<Request<external::namespace::Partial>> for Find<S>
where
    S: Service<ops::Find<Request<namespace::Partial>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx).map_err(Into::into)
    }

    fn call(
        &mut self,
        Request {
            who,
            request: external::namespace::Partial { handle },
        }: Request<external::namespace::Partial>,
    ) -> Self::Future {
        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let handle = handle.map(|handle| match handle {
                external::namespace::Identify::Document(handle) => {
                    namespace::Identify::Document(handle)
                }
                external::namespace::Identify::Entity(handle) => {
                    namespace::Identify::Entity(handle)
                }
                external::namespace::Identify::Team(handle) => namespace::Identify::Team(handle),
            });

            let request = namespace::Partial {
                handle,
                created_at: None,
                updated_at: None,
            };

            inner
                .call(ops::Find(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
