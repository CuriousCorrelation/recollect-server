pub(crate) mod fetch;
pub(crate) mod find;
pub(crate) mod update;

pub(crate) use fetch::Fetch;
pub(crate) use find::Find;
pub(crate) use update::Update;

use std::task::{ready, Context, Poll};

use futures::FutureExt;

use tower::Service;
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::{external, namespace, AuditField, BoxFuture, Request},
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Exchange {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::admin::create"))]
    context: AuditField,
}

impl Service<Request<namespace::Namespace>> for Exchange {
    type Response = external::namespace::Namespace;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            request: namespace::Namespace { handle, .. },
            ..
        }: Request<namespace::Namespace>,
    ) -> Self::Future {
        async move { Ok(external::namespace::Namespace { handle }) }.boxed()
    }
}
