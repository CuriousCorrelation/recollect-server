use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::service::{DocumentAssure, EntityAssure},
    model::{author, document, entity, external, ops, AuditField, BoxFuture, Request},
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Create<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::admin::create"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
    document_assure: DocumentAssure<StoreError>,
}

impl<S> Service<Request<external::author::Inclusive>> for Create<S>
where
    S: Service<ops::Create<Request<author::Inclusive>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx));
        ready!(self.entity_assure.poll_ready(cx));
        ready!(self.document_assure.poll_ready(cx));

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request: external::author::Inclusive { document, entity },
        }: Request<external::author::Inclusive>,
    ) -> Self::Future {
        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            // There's no reason to block `who` tring to `author` a `document`.
            let document_id = document_assure
                .call(ops::Assure(Request {
                    who,
                    request: document::Identify::Handle(document),
                }))
                .await
                .map_err(ExchangeError::Store)?
                .request;

            let entity_id = entity_assure
                .call(ops::Assure(Request {
                    who,
                    request: entity::Identify::Handle(entity),
                }))
                .await
                .map_err(ExchangeError::Store)?
                .request;

            let request = author::Inclusive {
                document_id,
                entity_id,
            };

            inner
                .call(ops::Create(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
