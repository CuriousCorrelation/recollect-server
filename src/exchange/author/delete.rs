use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{AuthorAssure, DocumentAssure, EntityAssure},
    model::{author, document, entity, external, ops, AuditField, BoxFuture, Request},
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Delete<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::admin::create"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
    author_assure: AuthorAssure<StoreError>,
    document_assure: DocumentAssure<StoreError>,
}

impl<S> Service<Request<external::author::Identify>> for Delete<S>
where
    S: Service<ops::Delete<Request<author::Identify>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.author_assure.poll_ready(cx))?;
        ready!(self.document_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request { who, request }: Request<external::author::Identify>,
    ) -> Self::Future {
        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let author_assure = self.author_assure.clone();
        let mut author_assure = std::mem::replace(&mut self.author_assure, author_assure);

        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let request = match request {
                external::author::Identify::Unique { document, entity } => {
                    let document_id = document_assure
                        .call(ops::Assure(Request {
                            who,
                            request: document::Identify::Handle(document),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    let entity_id = entity_assure
                        .call(ops::Assure(Request {
                            who,
                            request: entity::Identify::Handle(entity),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if entity_id == who.entity_id
                        && author_assure
                            .call(ops::Assure(Request {
                                who,
                                request: author::Identify::Unique {
                                    document_id,
                                    entity_id,
                                },
                            }))
                            .await
                            .is_ok()
                    {
                        Ok(author::Identify::Unique {
                            document_id,
                            entity_id,
                        })
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Delete(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
