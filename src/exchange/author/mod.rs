pub(crate) mod create;
pub(crate) mod delete;
pub(crate) mod fetch;
pub(crate) mod find;

pub(crate) use create::Create;
pub(crate) use delete::Delete;
pub(crate) use fetch::Fetch;
pub(crate) use find::Find;

use std::task::{ready, Context, Poll};

use futures::FutureExt;

use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::service::{DocumentMask, EntityMask},
    model::{author, document, entity, external, ops, AuditField, BoxFuture, Request},
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Exchange {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::admin::create"))]
    context: AuditField,
    entity_mask: EntityMask<StoreError>,
    document_mask: DocumentMask<StoreError>,
}

impl Service<Request<author::Author>> for Exchange {
    type Response = external::author::Author;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.entity_mask.poll_ready(cx))?;
        ready!(self.document_mask.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request:
                author::Author {
                    id: _,
                    document_id,
                    entity_id,
                    created_at,
                },
        }: Request<author::Author>,
    ) -> Self::Future {
        let entity_mask = self.entity_mask.clone();
        let mut entity_mask = std::mem::replace(&mut self.entity_mask, entity_mask);

        let document_mask = self.document_mask.clone();
        let mut document_mask = std::mem::replace(&mut self.document_mask, document_mask);

        async move {
            let entity = entity_mask
                .call(ops::Mask(Request {
                    who,
                    request: entity::Identify::Id(entity_id),
                }))
                .await
                .map_err(ExchangeError::Store)?
                .request;

            let document = document_mask
                .call(ops::Mask(Request {
                    who,
                    request: document::Identify::Id(document_id),
                }))
                .await
                .map_err(ExchangeError::Store)?
                .request;

            Ok(external::author::Author {
                entity,
                document,
                created_at,
            })
        }
        .boxed()
    }
}
