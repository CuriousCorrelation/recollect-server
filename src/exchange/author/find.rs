use std::task::{ready, Context, Poll};

use futures::{future::OptionFuture, FutureExt};
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::service::{DocumentAssure, EntityAssure},
    model::{author, document, entity, external, ops, AuditField, BoxFuture, Request},
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Find<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::admin::create"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
    document_assure: DocumentAssure<StoreError>,
}

impl<S> Service<Request<external::author::Partial>> for Find<S>
where
    S: Service<ops::Find<Request<author::Partial>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.document_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request:
                external::author::Partial {
                    document,
                    entity,
                    created_at,
                },
        }: Request<external::author::Partial>,
    ) -> Self::Future {
        let document_assure = self.document_assure.clone();
        let mut document_assure = std::mem::replace(&mut self.document_assure, document_assure);

        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let document_id: OptionFuture<_> = document
                .map(|handle| async move {
                    document_assure
                        .call(ops::Assure(Request {
                            who,
                            request: document::Identify::Handle(handle),
                        }))
                        .await
                })
                .into();

            let document_id = document_id
                .await
                .transpose()
                .map_err(ExchangeError::Store)?
                .map(|r| r.request);

            let entity_id: OptionFuture<_> = entity
                .map(|handle| async move {
                    entity_assure
                        .call(ops::Assure(Request {
                            who,
                            request: entity::Identify::Handle(handle),
                        }))
                        .await
                })
                .into();

            let entity_id = entity_id
                .await
                .transpose()
                .map_err(ExchangeError::Store)?
                .map(|r| r.request);

            let request = author::Partial {
                entity_id,
                document_id,
                created_at,
            };

            inner
                .call(ops::Find(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
