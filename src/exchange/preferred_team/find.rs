use std::task::{Context, Poll};

use futures::{future::OptionFuture, FutureExt};
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::service::{EntityAssure, TeamAssure},
    model::{entity, external, ops, preferred_team, team, AuditField, BoxFuture, Request},
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Find<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::preferred_team::find"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
    team_assure: TeamAssure<StoreError>,
}

impl<S> Service<Request<external::preferred_team::Partial>> for Find<S>
where
    S: Service<ops::Find<Request<preferred_team::Partial>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx).map_err(Into::into)
    }

    fn call(
        &mut self,
        Request {
            who,
            request:
                external::preferred_team::Partial {
                    entity,
                    team,
                    created_at,
                },
        }: Request<external::preferred_team::Partial>,
    ) -> Self::Future {
        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            let team_id: OptionFuture<_> = team
                .map(|handle| async move {
                    team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(handle),
                        }))
                        .await
                })
                .into();

            let team_id = team_id
                .await
                .transpose()
                .map_err(ExchangeError::Store)?
                .map(|r| r.request);

            let entity_id: OptionFuture<_> = entity
                .map(|handle| async move {
                    entity_assure
                        .call(ops::Assure(Request {
                            who,
                            request: entity::Identify::Handle(handle),
                        }))
                        .await
                })
                .into();

            let entity_id = entity_id
                .await
                .transpose()
                .map_err(ExchangeError::Store)?
                .map(|r| r.request);

            let request = preferred_team::Partial {
                entity_id,
                team_id,
                created_at,
            };

            inner
                .call(ops::Find(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
