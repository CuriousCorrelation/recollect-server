use std::task::{Context, Poll};

use futures::{future::OptionFuture, FutureExt};
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{EntityAssure, PreferredTeamAssure, TeamAssure},
    model::{entity, external, ops, preferred_team, team, AuditField, BoxFuture, Request},
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Update<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::preferred_team::update"))]
    context: AuditField,
    inner: S,
    entity_assure: EntityAssure<StoreError>,
    team_assure: TeamAssure<StoreError>,
    preferred_team_assure: PreferredTeamAssure<StoreError>,
}

impl<S> Service<Request<external::preferred_team::Exclusive>> for Update<S>
where
    S: Service<ops::Update<Request<preferred_team::Exclusive>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx).map_err(Into::into)
    }

    fn call(
        &mut self,
        Request {
            who,
            request:
                external::preferred_team::Exclusive {
                    identify,
                    entity,
                    team,
                },
        }: Request<external::preferred_team::Exclusive>,
    ) -> Self::Future {
        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let preferred_team_assure = self.preferred_team_assure.clone();
        let mut preferred_team_assure =
            std::mem::replace(&mut self.preferred_team_assure, preferred_team_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            // `who` can update team preferrence if
            // 1. `who` is trying to update their own team preferrence
            let request = match identify {
                external::preferred_team::Identify::Entity(entity_handle) => {
                    let entity_id = entity_assure
                        .call(ops::Assure(Request {
                            who,
                            request: entity::Identify::Handle(entity_handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if who.entity_id == entity_id {
                        let id = preferred_team_assure
                            .call(ops::Assure(Request {
                                who,
                                request: preferred_team::Identify::EntityId(entity_id),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        let team_id: OptionFuture<_> = team
                            .map(|handle| async move {
                                team_assure
                                    .call(ops::Assure(Request {
                                        who,
                                        request: team::Identify::Handle(handle),
                                    }))
                                    .await
                            })
                            .into();

                        let team_id = team_id
                            .await
                            .transpose()
                            .map_err(ExchangeError::Store)?
                            .map(|r| r.request);

                        let entity_id: OptionFuture<_> = entity
                            .map(|handle| async move {
                                entity_assure
                                    .call(ops::Assure(Request {
                                        who,
                                        request: entity::Identify::Handle(handle),
                                    }))
                                    .await
                            })
                            .into();

                        let entity_id = entity_id
                            .await
                            .transpose()
                            .map_err(ExchangeError::Store)?
                            .map(|r| r.request);

                        Ok(preferred_team::Exclusive {
                            id,
                            entity_id,
                            team_id,
                        })
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
                external::preferred_team::Identify::Team(team_handle) => {
                    let team_id = team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(team_handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if who.team_id == team_id {
                        let id = preferred_team_assure
                            .call(ops::Assure(Request {
                                who,
                                request: preferred_team::Identify::TeamId(team_id),
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        let team_id: OptionFuture<_> = team
                            .map(|handle| async move {
                                team_assure
                                    .call(ops::Assure(Request {
                                        who,
                                        request: team::Identify::Handle(handle),
                                    }))
                                    .await
                            })
                            .into();

                        let team_id = team_id
                            .await
                            .transpose()
                            .map_err(ExchangeError::Store)?
                            .map(|r| r.request);

                        let entity_id: OptionFuture<_> = entity
                            .map(|handle| async move {
                                entity_assure
                                    .call(ops::Assure(Request {
                                        who,
                                        request: entity::Identify::Handle(handle),
                                    }))
                                    .await
                            })
                            .into();

                        let entity_id = entity_id
                            .await
                            .transpose()
                            .map_err(ExchangeError::Store)?
                            .map(|r| r.request);

                        Ok(preferred_team::Exclusive {
                            id,
                            entity_id,
                            team_id,
                        })
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
                external::preferred_team::Identify::Unique {
                    entity: entity_handle,
                    team: team_handle,
                } => {
                    let team_id = team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(team_handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    let entity_id = entity_assure
                        .call(ops::Assure(Request {
                            who,
                            request: entity::Identify::Handle(entity_handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if who.entity_id == entity_id {
                        let id = preferred_team_assure
                            .call(ops::Assure(Request {
                                who,
                                request: preferred_team::Identify::Unique { team_id, entity_id },
                            }))
                            .await
                            .map_err(ExchangeError::Store)?
                            .request;

                        let team_id: OptionFuture<_> = team
                            .map(|handle| async move {
                                team_assure
                                    .call(ops::Assure(Request {
                                        who,
                                        request: team::Identify::Handle(handle),
                                    }))
                                    .await
                            })
                            .into();

                        let team_id = team_id
                            .await
                            .transpose()
                            .map_err(ExchangeError::Store)?
                            .map(|r| r.request);

                        let entity_id: OptionFuture<_> = entity
                            .map(|handle| async move {
                                entity_assure
                                    .call(ops::Assure(Request {
                                        who,
                                        request: entity::Identify::Handle(handle),
                                    }))
                                    .await
                            })
                            .into();

                        let entity_id = entity_id
                            .await
                            .transpose()
                            .map_err(ExchangeError::Store)?
                            .map(|r| r.request);

                        Ok(preferred_team::Exclusive {
                            id,
                            entity_id,
                            team_id,
                        })
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Update(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
