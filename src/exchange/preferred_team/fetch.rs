use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    exchange::error::ExchangeError,
    model::service::{EntityAssure, TeamAssure},
    model::{entity, external, ops, preferred_team, team, AuditField, BoxFuture, Request},
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Fetch<S> {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::preferred_team::fetch"))]
    context: AuditField,
    inner: S,
    team_assure: TeamAssure<StoreError>,
    entity_assure: EntityAssure<StoreError>,
}

impl<S> Service<Request<external::preferred_team::Identify>> for Fetch<S>
where
    S: Service<ops::Fetch<Request<preferred_team::Identify>>> + Clone + Send + 'static,
    ExchangeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;
        ready!(self.team_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request { who, request }: Request<external::preferred_team::Identify>,
    ) -> Self::Future {
        let team_assure = self.team_assure.clone();
        let mut team_assure = std::mem::replace(&mut self.team_assure, team_assure);

        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        async move {
            // `who` can fetch team preferrence if
            // 1. `who` is trying to fetch their own team preferrence
            let request = match request {
                external::preferred_team::Identify::Entity(entity_handle) => {
                    let entity_id = entity_assure
                        .call(ops::Assure(Request {
                            who,
                            request: entity::Identify::Handle(entity_handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if who.entity_id == entity_id {
                        Ok(preferred_team::Identify::Unique {
                            team_id: who.team_id,
                            entity_id,
                        })
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
                external::preferred_team::Identify::Team(team_handle) => {
                    let team_id = team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(team_handle),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if who.team_id == team_id {
                        Ok(preferred_team::Identify::Unique {
                            team_id,
                            entity_id: who.entity_id,
                        })
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
                external::preferred_team::Identify::Unique { entity, team } => {
                    let team_id = team_assure
                        .call(ops::Assure(Request {
                            who,
                            request: team::Identify::Handle(team),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    let entity_id = entity_assure
                        .call(ops::Assure(Request {
                            who,
                            request: entity::Identify::Handle(entity),
                        }))
                        .await
                        .map_err(ExchangeError::Store)?
                        .request;

                    if who.entity_id == entity_id {
                        Ok(preferred_team::Identify::Unique { team_id, entity_id })
                    } else {
                        Err(ExchangeError::AccessViolation(AccessViolation::Compliant))
                    }
                }
            }?;

            inner
                .call(ops::Fetch(Request { who, request }))
                .await
                .map_err(Into::into)
        }
        .boxed()
    }
}
