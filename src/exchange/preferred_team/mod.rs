pub(crate) mod delete;
pub(crate) mod fetch;
pub(crate) mod find;
pub(crate) mod update;

pub(crate) use delete::Delete;
pub(crate) use fetch::Fetch;
pub(crate) use find::Find;
pub(crate) use update::Update;

use std::task::{ready, Context, Poll};

use futures::FutureExt;

use tower::{Service, ServiceExt};
use typed_builder::TypedBuilder;

use crate::{
    exchange::error::ExchangeError,
    model::service::{EntityMask, TeamMask},
    model::{entity, external, ops, preferred_team, team, AuditField, BoxFuture, Request},
    store::error::StoreError,
};

#[derive(Debug, Clone, TypedBuilder, server_macro::ServiceContext)]
pub(crate) struct Exchange {
    #[builder(default = uuid::Uuid::new_v4())]
    id: uuid::Uuid,
    #[builder(default = AuditField::from_str_truncate("0.0.1"))]
    version: AuditField,
    #[builder(default = AuditField::from_str_truncate("exchange::admin::create"))]
    context: AuditField,

    entity_mask: EntityMask<StoreError>,
    team_mask: TeamMask<StoreError>,
}

impl Service<Request<preferred_team::PreferredTeam>> for Exchange {
    type Response = external::preferred_team::PreferredTeam;

    type Error = ExchangeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.entity_mask.poll_ready(cx))?;
        ready!(self.team_mask.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        Request {
            who,
            request:
                preferred_team::PreferredTeam {
                    id: _,
                    entity_id,
                    team_id,
                    updated_at,
                },
        }: Request<preferred_team::PreferredTeam>,
    ) -> Self::Future {
        let entity_mask = self.entity_mask.clone();
        let mut entity_mask = std::mem::replace(&mut self.entity_mask, entity_mask);

        let team_mask = self.team_mask.clone();
        let mut team_mask = std::mem::replace(&mut self.team_mask, team_mask);

        async move {
            let entity = entity_mask
                .call(ops::Mask(Request {
                    who,
                    request: entity::Identify::Id(entity_id),
                }))
                .await
                .map_err(ExchangeError::Store)?
                .request;

            let team = team_mask
                .call(ops::Mask(Request {
                    who,
                    request: team::Identify::Id(team_id),
                }))
                .await
                .map_err(ExchangeError::Store)?
                .request;

            Ok(external::preferred_team::PreferredTeam {
                entity,
                team,
                updated_at,
            })
        }
        .boxed()
    }
}
