use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::Service;

use crate::model::BoxFuture;

use self::error::ExchangeError;

pub(crate) mod admin;
pub(crate) mod ascribe;
pub(crate) mod author;
pub(crate) mod blockade;
pub(crate) mod connection;
pub(crate) mod document;
pub(crate) mod entity;
pub(crate) mod error;
pub(crate) mod member;
pub(crate) mod namespace;
pub(crate) mod preferred_team;
pub(crate) mod team;
pub(crate) mod visibility;
