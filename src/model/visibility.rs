use chrono::{DateTime, Utc};
use diesel::{AsChangeset, Identifiable, Insertable, Queryable};
use serde::Serialize;

use crate::store::schema::visibility;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    label::Label,
    scope::Scope,
    AuditField, PrimaryKey,
};

#[derive(Debug, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Id(PrimaryKey),
    Object(PrimaryKey),
    Unique {
        object: PrimaryKey,
        scope: Scope,
        label: Option<Label>,
    },
}

#[derive(Debug, Serialize)]
pub(crate) struct Partial {
    pub(crate) scope: Option<Scope>,
}

#[derive(Debug, Serialize, AsChangeset)]
#[diesel(table_name = visibility)]
pub(crate) struct Exclusive {
    pub(crate) id: PrimaryKey,
    pub(crate) label: Option<Label>,
    pub(crate) scope: Option<Scope>,
}

#[derive(Debug, Serialize, Insertable)]
#[diesel(table_name = visibility)]
pub(crate) struct Inclusive {
    pub(crate) label: Label,
    pub(crate) object: PrimaryKey,
    pub(crate) scope: Scope,
}

#[derive(Debug, Serialize, Queryable, Identifiable)]
#[diesel(table_name = visibility)]
pub(crate) struct VisibilityTable {
    pub(crate) id: PrimaryKey,
    pub(crate) label: Label,
    pub(crate) object: PrimaryKey,
    pub(crate) scope: Scope,
    pub(crate) created_at: DateTime<Utc>,
    pub(crate) updated_at: DateTime<Utc>,
    pub(crate) deleted_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize, Queryable, Identifiable)]
#[diesel(table_name = visibility)]
pub(crate) struct Visibility {
    pub(crate) id: PrimaryKey,
    pub(crate) label: Label,
    pub(crate) object: PrimaryKey,
    pub(crate) scope: Scope,
    pub(crate) updated_at: DateTime<Utc>,
}

impl InternalIdentifier for Visibility {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.id.to_string()))
    }
}
