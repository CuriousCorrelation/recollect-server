use oauth2::{AuthorizationCode, CsrfToken, PkceCodeVerifier};
use serde::Deserialize;

use super::{auth_user::AuthUser, provider::Provider};

#[derive(Debug, Clone, Deserialize)]
pub(crate) struct SecurityTokens {
    pub(crate) state: Option<CsrfToken>,
    pub(crate) code: Option<AuthorizationCode>,
}

#[derive(Debug)]
pub(crate) struct OAuthRequest {
    pub(crate) security_tokens: SecurityTokens,
    pub(crate) pkce_verifier: PkceCodeVerifier,
}
