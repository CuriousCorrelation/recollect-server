use chrono::{DateTime, Utc};
use diesel::{Identifiable, Insertable, Queryable};
use serde_json::Value;
use typed_builder::TypedBuilder;
use uuid::Uuid;

use crate::store::schema::audit;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    AuditField, PrimaryKey,
};

#[derive(Debug)]
pub(crate) enum Identify {
    Id(PrimaryKey),
}

#[derive(Debug)]
pub(crate) struct Partial {
    pub(crate) context: Option<AuditField>,
    pub(crate) version: Option<AuditField>,
    pub(crate) service_id: Option<Uuid>,
}

#[derive(Debug, TypedBuilder, Insertable)]
#[diesel(table_name = audit)]
pub(crate) struct Inclusive {
    #[builder(setter(into))]
    pub(crate) context: AuditField,
    #[builder(setter(into))]
    pub(crate) version: AuditField,
    pub(crate) service_id: Uuid,
    pub(crate) source: Value,
    pub(crate) response: Value,
}

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct InclusiveContext {
    #[builder(setter(into))]
    pub(crate) context: AuditField,
    #[builder(setter(into))]
    pub(crate) version: AuditField,
    pub(crate) service_id: Uuid,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = audit)]
#[allow(dead_code)]
pub(crate) struct AuditTable {
    pub(crate) id: PrimaryKey,
    pub(crate) context: AuditField,
    pub(crate) version: AuditField,
    pub(crate) service_id: Uuid,
    pub(crate) source: Value,
    pub(crate) response: Value,
    pub(crate) timestamp: DateTime<Utc>,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = audit)]
pub(crate) struct Audit {
    pub(crate) id: PrimaryKey,
    pub(crate) context: AuditField,
    pub(crate) version: AuditField,
    pub(crate) service_id: Uuid,
    pub(crate) source: Value,
    pub(crate) response: Value,
    pub(crate) timestamp: DateTime<Utc>,
}

impl InternalIdentifier for Audit {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.id.to_string()))
    }
}
