pub(crate) struct Create<T>(pub(crate) T);

pub(crate) struct Update<T>(pub(crate) T);

pub(crate) struct Fetch<T>(pub(crate) T);

pub(crate) struct Find<T>(pub(crate) T);

pub(crate) struct Delete<T>(pub(crate) T);

pub(crate) struct Assure<T>(pub(crate) T);

pub(crate) struct Mask<T>(pub(crate) T);

pub(crate) struct Count<T>(pub(crate) T);
