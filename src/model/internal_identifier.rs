use serde::Serialize;

use super::AuditField;

#[derive(Serialize)]
pub(crate) enum Identifier {
    Mono(AuditField),
    Mult(Vec<AuditField>),
}

pub(crate) trait InternalIdentifier {
    fn internal_identifier(&self) -> Identifier;
}
