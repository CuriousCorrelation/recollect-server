use chrono::{DateTime, Utc};
use diesel::{AsChangeset, Identifiable, Insertable, Queryable};
use serde::Serialize;

use crate::store::schema::auth;

use super::{
    auth_user::AuthUser,
    internal_identifier::{Identifier, InternalIdentifier},
    name::Name,
    provider::Provider,
    AuditField, Secret,
};

#[derive(Debug, Clone)]
pub(crate) struct AuthResponse {
    pub(crate) auth_user: AuthUser,
    pub(crate) provider: Provider,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Secret(Secret),
    Email(String),
}

#[derive(Debug, Serialize)]
pub(crate) struct Partial {
    pub(crate) id: Option<String>,
    pub(crate) name: Option<Name>,
    pub(crate) picture: Option<String>,
    pub(crate) provider: Option<Provider>,
}

#[derive(Debug, Serialize, AsChangeset)]
#[diesel(table_name = auth)]
pub(crate) struct Exclusive {
    pub(crate) secret: Secret,
    pub(crate) id: Option<String>,
    pub(crate) name: Option<Name>,
    pub(crate) email: Option<String>,
    pub(crate) picture: Option<String>,
}

#[derive(Debug, Serialize, Insertable)]
#[diesel(table_name = auth)]
pub(crate) struct Inclusive {
    pub(crate) secret: Secret,
    pub(crate) id: String,
    pub(crate) name: Name,
    pub(crate) email: String,
    pub(crate) picture: String,
    pub(crate) provider: Provider,
}

#[derive(Queryable, Identifiable)]
#[diesel(table_name = auth)]
pub(crate) struct Auth {
    pub(crate) secret: Secret,
    pub(crate) id: String,
    pub(crate) name: Name,
    pub(crate) email: String,
    pub(crate) picture: Option<String>,
    pub(crate) provider: Provider,
    pub(crate) created_at: DateTime<Utc>,
    pub(crate) updated_at: Option<DateTime<Utc>>,
    pub(crate) deleted_at: Option<DateTime<Utc>>,
}

impl std::fmt::Debug for Auth {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Auth")
            .field("secret", &"[...]")
            .field("id", &self.id)
            .field("name", &self.name)
            .field("email", &self.email)
            .field("picture", &self.picture)
            .field("provider", &self.provider)
            .field("created_at", &self.created_at)
            .field("updated_at", &self.updated_at)
            .field("deleted_at", &self.deleted_at)
            .finish()
    }
}

impl InternalIdentifier for Auth {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.secret))
    }
}
