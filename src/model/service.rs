use tower::util::BoxCloneService;
use uuid::Uuid;

use super::{
    admin::{self, Admin},
    ascribe::{self, Ascribe},
    audit::{self, Audit},
    auth::{self, Auth},
    author::{self, Author},
    blockade::{self, Blockade},
    connection::{self, Connection},
    document::{self, Document},
    entity::{self, Entity},
    measure::{self, Measure},
    member::{self, Member},
    namespace::{self, Namespace},
    object::{self, Object},
    ops,
    preferred_team::{self, PreferredTeam},
    requester::{self, Requester},
    session::{self, Session},
    team::{self, Team},
    token::{self, Token},
    visibility::{self, Visibility},
    AuditField, Handle, PrimaryKey, Request, Secret,
};

type Create<Req, Res, E> = BoxCloneService<ops::Create<Request<Req>>, Request<Res>, E>;
type Update<Req, Res, E> = BoxCloneService<ops::Update<Request<Req>>, Request<Res>, E>;
type Fetch<Req, Res, E> = BoxCloneService<ops::Fetch<Request<Req>>, Request<Res>, E>;
type Find<Req, Res, E> = BoxCloneService<ops::Find<Request<Req>>, Request<Res>, E>;
type Delete<Req, Res, E> = BoxCloneService<ops::Delete<Request<Req>>, Request<Res>, E>;
type Assure<Req, Res, E> = BoxCloneService<ops::Assure<Request<Req>>, Request<Res>, E>;
type Mask<Req, Res, E> = BoxCloneService<ops::Mask<Request<Req>>, Request<Res>, E>;
type Count<Req, Res, E> = BoxCloneService<ops::Count<Request<Req>>, Request<Res>, E>;

pub(crate) type AdminCreate<E> = Create<admin::Inclusive, Admin, E>;
// pub(crate) type AdminUpdate<E> = Update<admin::Exclusive, Admin, E>;
pub(crate) type AdminFetch<E> = Fetch<admin::Identify, Admin, E>;
pub(crate) type AdminFind<E> = Find<admin::Partial, Vec<Admin>, E>;
pub(crate) type AdminDelete<E> = Delete<admin::Identify, Admin, E>;
pub(crate) type AdminAssure<E> = Assure<admin::Identify, PrimaryKey, E>;
pub(crate) type AdminMask<E> = Mask<admin::Identify, Handle, E>;
pub(crate) type AdminCount<E> = Count<admin::Partial, i64, E>;

pub(crate) type AscribeCreate<E> = Create<ascribe::Inclusive, Ascribe, E>;
// pub(crate) type AscribeUpdate<E> = Update<ascribe::Exclusive, Ascribe, E>;
pub(crate) type AscribeFetch<E> = Fetch<ascribe::Identify, Ascribe, E>;
pub(crate) type AscribeFind<E> = Find<ascribe::Partial, Vec<Ascribe>, E>;
pub(crate) type AscribeDelete<E> = Delete<ascribe::Identify, Ascribe, E>;
pub(crate) type AscribeAssure<E> = Assure<ascribe::Identify, PrimaryKey, E>;
pub(crate) type AscribeMask<E> = Mask<ascribe::Identify, Handle, E>;
pub(crate) type AscribeCount<E> = Count<ascribe::Partial, i64, E>;

pub(crate) type AuditCreate<E> = Create<audit::Inclusive, Audit, E>;
// pub(crate) type AuditUpdate<E> = Update<audit::Exclusive, Audit, E>;
pub(crate) type AuditFetch<E> = Fetch<audit::Identify, Audit, E>;
pub(crate) type AuditFind<E> = Find<audit::Partial, Vec<Audit>, E>;
pub(crate) type AuditDelete<E> = Delete<audit::Identify, Audit, E>;
pub(crate) type AuditAssure<E> = Assure<audit::Identify, PrimaryKey, E>;
pub(crate) type AuditMask<E> = Mask<audit::Identify, Handle, E>;
pub(crate) type AuditCount<E> = Count<audit::Partial, i64, E>;

pub(crate) type AuthCreate<E> = Create<auth::Inclusive, Auth, E>;
pub(crate) type AuthUpdate<E> = Update<auth::Exclusive, Auth, E>;
pub(crate) type AuthFetch<E> = Fetch<auth::Identify, Auth, E>;
pub(crate) type AuthFind<E> = Find<auth::Partial, Vec<Auth>, E>;
pub(crate) type AuthDelete<E> = Delete<auth::Identify, Auth, E>;
pub(crate) type AuthAssure<E> = Assure<auth::Identify, Secret, E>;
pub(crate) type AuthMask<E> = Mask<auth::Identify, Handle, E>;
pub(crate) type AuthCount<E> = Count<auth::Partial, i64, E>;

pub(crate) type AuthorCreate<E> = Create<author::Inclusive, Author, E>;
// pub(crate) type AuthorUpdate<E> = Update<author::Exclusive, Author, E>;
pub(crate) type AuthorFetch<E> = Fetch<author::Identify, Author, E>;
pub(crate) type AuthorFind<E> = Find<author::Partial, Vec<Author>, E>;
pub(crate) type AuthorDelete<E> = Delete<author::Identify, Author, E>;
pub(crate) type AuthorAssure<E> = Assure<author::Identify, PrimaryKey, E>;
pub(crate) type AuthorMask<E> = Mask<author::Identify, Handle, E>;
pub(crate) type AuthorCount<E> = Count<author::Partial, i64, E>;

pub(crate) type BlockadeCreate<E> = Create<blockade::Inclusive, Blockade, E>;
pub(crate) type BlockadeUpdate<E> = Update<blockade::Exclusive, Blockade, E>;
pub(crate) type BlockadeFetch<E> = Fetch<blockade::Identify, Blockade, E>;
pub(crate) type BlockadeFind<E> = Find<blockade::Partial, Vec<Blockade>, E>;
pub(crate) type BlockadeDelete<E> = Delete<blockade::Identify, Blockade, E>;
pub(crate) type BlockadeAssure<E> = Assure<blockade::Identify, PrimaryKey, E>;
pub(crate) type BlockadeMask<E> = Mask<blockade::Identify, Handle, E>;
pub(crate) type BlockadeCount<E> = Count<blockade::Partial, i64, E>;

pub(crate) type ConnectionCreate<E> = Create<connection::Inclusive, Connection, E>;
pub(crate) type ConnectionUpdate<E> = Update<connection::Exclusive, Connection, E>;
pub(crate) type ConnectionFetch<E> = Fetch<connection::Identify, Connection, E>;
pub(crate) type ConnectionFind<E> = Find<connection::Partial, Vec<Connection>, E>;
pub(crate) type ConnectionDelete<E> = Delete<connection::Identify, Connection, E>;
pub(crate) type ConnectionAssure<E> = Assure<connection::Identify, PrimaryKey, E>;
pub(crate) type ConnectionMask<E> = Mask<connection::Identify, Handle, E>;
pub(crate) type ConnectionCount<E> = Count<connection::Partial, i64, E>;

pub(crate) type DocumentCreate<E> = Create<document::Inclusive, Document, E>;
pub(crate) type DocumentUpdate<E> = Update<document::Exclusive, Document, E>;
pub(crate) type DocumentFetch<E> = Fetch<document::Identify, Document, E>;
pub(crate) type DocumentFind<E> = Find<document::Partial, Vec<Document>, E>;
pub(crate) type DocumentDelete<E> = Delete<document::Identify, Document, E>;
pub(crate) type DocumentAssure<E> = Assure<document::Identify, PrimaryKey, E>;
pub(crate) type DocumentMask<E> = Mask<document::Identify, Handle, E>;
pub(crate) type DocumentCount<E> = Count<document::Partial, i64, E>;

pub(crate) type EntityCreate<E> = Create<entity::Inclusive, Entity, E>;
pub(crate) type EntityUpdate<E> = Update<entity::Exclusive, Entity, E>;
pub(crate) type EntityFetch<E> = Fetch<entity::Identify, Entity, E>;
pub(crate) type EntityFind<E> = Find<entity::Partial, Vec<Entity>, E>;
pub(crate) type EntityDelete<E> = Delete<entity::Identify, Entity, E>;
pub(crate) type EntityAssure<E> = Assure<entity::Identify, PrimaryKey, E>;
pub(crate) type EntityMask<E> = Mask<entity::Identify, Handle, E>;
pub(crate) type EntityCount<E> = Count<entity::Partial, i64, E>;

pub(crate) type MeasureCreate<E> = Create<measure::Inclusive, Measure, E>;
pub(crate) type MeasureUpdate<E> = Update<measure::Exclusive, Measure, E>;
pub(crate) type MeasureFetch<E> = Fetch<measure::Identify, Measure, E>;
pub(crate) type MeasureFind<E> = Find<measure::Partial, Vec<Measure>, E>;
pub(crate) type MeasureDelete<E> = Delete<measure::Identify, Measure, E>;
pub(crate) type MeasureAssure<E> = Assure<measure::Identify, PrimaryKey, E>;
pub(crate) type MeasureMask<E> = Mask<measure::Identify, Handle, E>;
pub(crate) type MeasureCount<E> = Count<measure::Partial, i64, E>;

pub(crate) type MemberCreate<E> = Create<member::Inclusive, Member, E>;
// pub(crate) type MemberUpdate<E> = Update<member::Exclusive, Member, E>;
pub(crate) type MemberFetch<E> = Fetch<member::Identify, Member, E>;
pub(crate) type MemberFind<E> = Find<member::Partial, Vec<Member>, E>;
pub(crate) type MemberDelete<E> = Delete<member::Identify, Member, E>;
pub(crate) type MemberAssure<E> = Assure<member::Identify, PrimaryKey, E>;
pub(crate) type MemberMask<E> = Mask<member::Identify, Handle, E>;
pub(crate) type MemberCount<E> = Count<member::Partial, i64, E>;

pub(crate) type NamespaceCreate<E> = Create<namespace::Inclusive, Namespace, E>;
pub(crate) type NamespaceUpdate<E> = Update<namespace::Exclusive, Namespace, E>;
pub(crate) type NamespaceFetch<E> = Fetch<namespace::Identify, Namespace, E>;
pub(crate) type NamespaceFind<E> = Find<namespace::Partial, Vec<Namespace>, E>;
pub(crate) type NamespaceDelete<E> = Delete<namespace::Identify, Namespace, E>;
pub(crate) type NamespaceAssure<E> = Assure<namespace::Identify, PrimaryKey, E>;
pub(crate) type NamespaceMask<E> = Mask<namespace::Identify, Handle, E>;
pub(crate) type NamespaceCount<E> = Count<namespace::Partial, i64, E>;

pub(crate) type ObjectCreate<E> = Create<object::Inclusive, Object, E>;
// pub(crate) type ObjectUpdate<E> = Update<object::Exclusive, Object, E>;
pub(crate) type ObjectFetch<E> = Fetch<object::Identify, Object, E>;
// pub(crate) type ObjectFind<E> = Find<object::Partial, Vec<Object>, E>;
pub(crate) type ObjectDelete<E> = Delete<object::Identify, Object, E>;
pub(crate) type ObjectAssure<E> = Assure<object::Identify, PrimaryKey, E>;
pub(crate) type ObjectMask<E> = Mask<object::Identify, Handle, E>;
// pub(crate) type ObjectCount<E> = Count<object::Partial, i64, E>;

pub(crate) type PreferredTeamCreate<E> = Create<preferred_team::Inclusive, PreferredTeam, E>;
pub(crate) type PreferredTeamUpdate<E> = Update<preferred_team::Exclusive, PreferredTeam, E>;
pub(crate) type PreferredTeamFetch<E> = Fetch<preferred_team::Identify, PreferredTeam, E>;
pub(crate) type PreferredTeamFind<E> = Find<preferred_team::Partial, Vec<PreferredTeam>, E>;
pub(crate) type PreferredTeamDelete<E> = Delete<preferred_team::Identify, PreferredTeam, E>;
pub(crate) type PreferredTeamAssure<E> = Assure<preferred_team::Identify, PrimaryKey, E>;
pub(crate) type PreferredTeamMask<E> = Mask<preferred_team::Identify, Handle, E>;
pub(crate) type PreferredTeamCount<E> = Count<preferred_team::Partial, i64, E>;

pub(crate) type RequesterCreate<E> = Create<requester::Inclusive, Requester, E>;
// pub(crate) type RequesterUpdate<E> = Update<requester::Exclusive, Requester, E>;
pub(crate) type RequesterFetch<E> = Fetch<requester::Identify, Requester, E>;
pub(crate) type RequesterFind<E> = Find<requester::Partial, Vec<Requester>, E>;
pub(crate) type RequesterDelete<E> = Delete<requester::Identify, Requester, E>;
pub(crate) type RequesterAssure<E> = Assure<requester::Identify, PrimaryKey, E>;
pub(crate) type RequesterMask<E> = Mask<requester::Identify, Handle, E>;
pub(crate) type RequesterCount<E> = Count<requester::Partial, i64, E>;

pub(crate) type SessionCreate<E> = Create<session::Inclusive, Session, E>;
// pub(crate) type SessionUpdate<E> = Update<session::Exclusive, Session, E>;
pub(crate) type SessionFetch<E> = Fetch<session::Identify, Session, E>;
// pub(crate) type SessionFind<E> = Find<session::Partial, Vec<Session>, E>;
pub(crate) type SessionDelete<E> = Delete<session::Identify, Session, E>;
pub(crate) type SessionAssure<E> = Assure<session::Identify, PrimaryKey, E>;
pub(crate) type SessionMask<E> = Mask<session::Identify, Handle, E>;
// pub(crate) type SessionCount<E> = Count<session::Partial, i64, E>;

pub(crate) type TeamCreate<E> = Create<team::Inclusive, Team, E>;
pub(crate) type TeamUpdate<E> = Update<team::Exclusive, Team, E>;
pub(crate) type TeamFetch<E> = Fetch<team::Identify, Team, E>;
pub(crate) type TeamFind<E> = Find<team::Partial, Vec<Team>, E>;
pub(crate) type TeamDelete<E> = Delete<team::Identify, Team, E>;
pub(crate) type TeamAssure<E> = Assure<team::Identify, PrimaryKey, E>;
pub(crate) type TeamMask<E> = Mask<team::Identify, Handle, E>;
pub(crate) type TeamCount<E> = Count<team::Partial, i64, E>;

pub(crate) type TokenCreate<E> = Create<token::Inclusive, Token, E>;
// pub(crate) type TokenUpdate<E> = Update<token::Exclusive, Token, E>;
pub(crate) type TokenFetch<E> = Fetch<token::Identify, Token, E>;
// pub(crate) type TokenFind<E> = Find<token::Partial, Vec<Token>, E>;
pub(crate) type TokenDelete<E> = Delete<token::Identify, Token, E>;
pub(crate) type TokenAssure<E> = Assure<token::Identify, PrimaryKey, E>;
pub(crate) type TokenMask<E> = Mask<token::Identify, Handle, E>;
// pub(crate) type TokenCount<E> = Count<token::Partial, i64, E>;

pub(crate) type VisibilityCreate<E> = Create<visibility::Inclusive, Visibility, E>;
pub(crate) type VisibilityUpdate<E> = Update<visibility::Exclusive, Visibility, E>;
pub(crate) type VisibilityFetch<E> = Fetch<visibility::Identify, Visibility, E>;
pub(crate) type VisibilityFind<E> = Find<visibility::Partial, Vec<Visibility>, E>;
pub(crate) type VisibilityDelete<E> = Delete<visibility::Identify, Visibility, E>;
pub(crate) type VisibilityAssure<E> = Assure<visibility::Identify, PrimaryKey, E>;
pub(crate) type VisibilityMask<E> = Mask<visibility::Identify, Handle, E>;
pub(crate) type VisibilityCount<E> = Count<visibility::Partial, i64, E>;
