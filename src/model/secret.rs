use arraystring::ArrayString;

use super::{
    audit_field::AuditField,
    internal_identifier::{Identifier, InternalIdentifier},
};

pub(crate) type Secret = ArrayString<22>;

impl InternalIdentifier for Secret {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate("[SECRET]"))
    }
}
