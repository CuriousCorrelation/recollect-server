use super::{api_request::ApiRequest, connection_info::ConnectionInfo};

/// Describes an API request along with auxiliary information like
/// connection information and other pre requisites.
#[derive(Debug)]
pub(crate) struct Requisite<P> {
    /// API request key, could be the actual API key
    /// or other identity gathered from cookies.
    api_request: ApiRequest,
    /// Requester's connection information used for
    /// verifying actor and detecting unusual activities.
    connection_info: ConnectionInfo,
    /// Pre requisite for `request`. Usually set in layer transit.
    pre_req: P,
}

#[derive(Debug)]
pub(crate) struct Empty;

pub(crate) type EmptyRequisite = Requisite<Empty>;

impl Clone for EmptyRequisite {
    fn clone(&self) -> Self {
        Self {
            api_request: self.api_request.clone(),
            connection_info: self.connection_info.clone(),
            pre_req: Empty,
        }
    }
}

pub(crate) struct RequisiteView<V, P> {
    pub(crate) view: V,
    pub(crate) requisite: Requisite<P>,
}

impl EmptyRequisite {
    pub(crate) fn empty(api_request: ApiRequest, connection_info: ConnectionInfo) -> Self {
        Self {
            api_request,
            connection_info,
            pre_req: Empty,
        }
    }
}

impl<P> Requisite<P> {
    pub(crate) fn new(
        api_request: ApiRequest,
        connection_info: ConnectionInfo,
        pre_req: P,
    ) -> Self {
        Self {
            api_request,
            connection_info,
            pre_req,
        }
    }
}

impl<P> Requisite<P> {
    pub(crate) fn with_pre<SwapWith>(self, pre_req: SwapWith) -> Requisite<SwapWith> {
        Requisite { pre_req, ..self }
    }

    pub(crate) fn with_view<View>(self, view: View) -> RequisiteView<View, P> {
        RequisiteView {
            view,
            requisite: self,
        }
    }
}

impl<P> Requisite<P> {
    pub(crate) fn pre_req(self) -> RequisiteView<P, Empty> {
        RequisiteView {
            view: self.pre_req,
            requisite: Requisite {
                pre_req: Empty,
                ..self
            },
        }
    }

    pub(crate) fn api_request(&self) -> ApiRequest {
        self.api_request.clone()
    }

    pub(crate) fn connection_info(&self) -> ConnectionInfo {
        self.connection_info.clone()
    }
}
