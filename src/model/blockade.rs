use chrono::{DateTime, Utc};
use diesel::{AsChangeset, Identifiable, Insertable, Queryable};
use serde::Serialize;
use typed_builder::TypedBuilder;

use crate::store::schema::blockade;

use super::{
    action::Action,
    internal_identifier::{Identifier, InternalIdentifier},
    label::Label,
    AuditField, PrimaryKey,
};

#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Id(PrimaryKey),
    Unique {
        originate_label: Label,
        originate: PrimaryKey,
        action: Option<Action>,
        destinate_label: Label,
        destinate: PrimaryKey,
    },
}

#[derive(Debug, Serialize, TypedBuilder)]
pub(crate) struct Partial {
    pub(crate) originate_label: Option<Label>,
    pub(crate) originate: Option<PrimaryKey>,
    pub(crate) action: Option<Action>,
    pub(crate) destinate_label: Option<Label>,
    pub(crate) destinate: Option<PrimaryKey>,
    pub(crate) expiry_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize, AsChangeset)]
#[diesel(table_name = blockade)]
pub(crate) struct Exclusive {
    pub(crate) id: PrimaryKey,
    pub(crate) originate_label: Option<Label>,
    pub(crate) originate: Option<PrimaryKey>,
    pub(crate) action: Option<Action>,
    pub(crate) destinate_label: Option<Label>,
    pub(crate) destinate: Option<PrimaryKey>,
    pub(crate) expiry_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize, Insertable)]
#[diesel(table_name = blockade)]
pub(crate) struct Inclusive {
    pub(crate) originate_label: Label,
    pub(crate) originate: PrimaryKey,
    pub(crate) action: Action,
    pub(crate) destinate_label: Label,
    pub(crate) destinate: PrimaryKey,
    pub(crate) expiry_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = blockade)]
#[allow(dead_code)]
pub(crate) struct BlockadeTable {
    pub(crate) id: PrimaryKey,
    pub(crate) originate_label: Label,
    pub(crate) originate: PrimaryKey,
    pub(crate) action: Action,
    pub(crate) destinate_label: Label,
    pub(crate) destinate: PrimaryKey,
    pub(crate) expiry_at: Option<DateTime<Utc>>,
    pub(crate) created_at: DateTime<Utc>,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = blockade)]
pub(crate) struct Blockade {
    pub(crate) id: PrimaryKey,
    pub(crate) originate_label: Label,
    pub(crate) originate: PrimaryKey,
    pub(crate) action: Action,
    pub(crate) destinate_label: Label,
    pub(crate) destinate: PrimaryKey,
    pub(crate) expiry_at: Option<DateTime<Utc>>,
    pub(crate) created_at: DateTime<Utc>,
}

impl InternalIdentifier for Blockade {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.id.to_string()))
    }
}
