use diesel_derive_enum::DbEnum;
use serde::Serialize;
use strum::{Display, EnumString};

#[derive(PartialEq, Eq, Debug, Serialize, EnumString, Display, Clone, Copy, DbEnum)]
#[ExistingTypePath = "crate::store::schema::sql_types::Label"]
#[DbValueStyle = "snake_case"]
#[serde(rename_all = "snake_case")]
pub(crate) enum Label {
    Team,
    Entity,
    Document,
}
