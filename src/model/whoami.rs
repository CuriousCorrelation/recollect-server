use serde::Serialize;

use super::{name::Name, Handle, Secret};

#[derive(Debug, Serialize)]
pub(crate) struct WhoAmI {
    pub(crate) session_id: Secret,
    pub(crate) entity_handle: Handle,
    pub(crate) entity_name: Name,
    pub(crate) team_handle: Handle,
    pub(crate) team_name: Name,
}
