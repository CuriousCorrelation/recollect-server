use chrono::{DateTime, Utc};
use diesel::{AsChangeset, Identifiable, Insertable, Queryable};
use serde::Serialize;

use crate::store::schema::measure;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    label::Label,
    AuditField, PrimaryKey,
};

#[derive(Debug, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Id(PrimaryKey),
    Unique { entity_id: PrimaryKey, label: Label },
}

#[derive(Debug, Serialize)]
pub(crate) struct Partial {
    pub(crate) entity_id: Option<PrimaryKey>,
    pub(crate) label: Option<Label>,
    pub(crate) current: Option<i32>,
    pub(crate) maximum: Option<i32>,
}

#[derive(Debug, Serialize, AsChangeset)]
#[diesel(table_name = measure)]
pub(crate) struct Exclusive {
    pub(crate) id: PrimaryKey,
    pub(crate) current: Option<i32>,
    pub(crate) maximum: Option<i32>,
}

#[derive(Debug, Serialize, Insertable)]
#[diesel(table_name = measure)]
pub(crate) struct Inclusive {
    pub(crate) entity_id: PrimaryKey,
    pub(crate) label: Label,
    pub(crate) current: i32,
    pub(crate) maximum: i32,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = measure)]
#[allow(dead_code)]
pub(crate) struct MeasureTable {
    pub(crate) id: PrimaryKey,
    pub(crate) entity_id: PrimaryKey,
    pub(crate) label: Label,
    pub(crate) current: i32,
    pub(crate) maximum: i32,
    pub(crate) created_at: DateTime<Utc>,
    pub(crate) updated_at: DateTime<Utc>,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = measure)]
pub(crate) struct Measure {
    pub(crate) id: PrimaryKey,
    pub(crate) current: i32,
}

impl InternalIdentifier for Measure {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.id.to_string()))
    }
}
