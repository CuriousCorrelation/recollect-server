use chrono::{DateTime, Utc};
use diesel::Queryable;
use serde::Serialize;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    AuditField, Handle,
};

#[derive(Debug, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Document(Handle),
    Entity(Handle),
    Team(Handle),
}

#[derive(Debug, Serialize)]
pub(crate) struct Partial {
    pub(crate) handle: Option<Identify>,
    pub(crate) created_at: Option<DateTime<Utc>>,
    pub(crate) updated_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize)]
pub(crate) struct Exclusive {
    pub(crate) id: Handle,
    pub(crate) handle: Handle,
}

#[derive(Debug, Serialize)]
pub(crate) struct Inclusive {}

#[derive(Debug, Queryable)]
#[diesel(table_name = namespace)]
#[allow(dead_code)]
pub(crate) struct NamespaceTable {
    pub(crate) handle: Handle,
    pub(crate) created_at: DateTime<Utc>,
    pub(crate) updated_at: DateTime<Utc>,
}

#[derive(Debug, Queryable)]
#[diesel(table_name = namespace)]
pub(crate) struct Namespace {
    pub(crate) handle: Handle,
    pub(crate) created_at: DateTime<Utc>,
    pub(crate) updated_at: DateTime<Utc>,
}

impl InternalIdentifier for Namespace {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.handle))
    }
}
