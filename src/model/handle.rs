use arraystring::ArrayString;

use super::{
    audit_field::AuditField,
    internal_identifier::{Identifier, InternalIdentifier},
};

pub(crate) type Handle = ArrayString<16>;

impl InternalIdentifier for Handle {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self))
    }
}
