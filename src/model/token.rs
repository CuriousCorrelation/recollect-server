use std::fmt::Debug;

use chrono::{DateTime, Utc};
use diesel::Queryable;
use serde::Serialize;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    AuditField, Secret,
};

#[derive(Debug, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Secret(Secret),
}

#[derive(Debug, Serialize)]
pub(crate) struct Inclusive {}

#[derive(Queryable)]
#[diesel(table_name = token)]
pub(crate) struct Token {
    pub(crate) secret: Secret,
    pub(crate) created_at: DateTime<Utc>,
}

impl Debug for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Token")
            .field("secret", &"[...]")
            .field("created_at", &self.created_at)
            .finish()
    }
}

impl InternalIdentifier for Token {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.secret))
    }
}
