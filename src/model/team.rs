use chrono::{DateTime, Utc};
use diesel::{AsChangeset, Identifiable, Insertable, Queryable};
use serde::Serialize;

use crate::store::schema::team;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    name::Name,
    AuditField, Handle, PrimaryKey,
};

#[derive(Debug, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Id(PrimaryKey),
    Handle(Handle),
}

#[derive(Debug, Serialize)]
pub(crate) struct Partial {
    pub(crate) name: Option<Name>,
}

#[derive(Debug, Serialize, AsChangeset)]
#[diesel(table_name = team)]
pub(crate) struct Exclusive {
    pub(crate) id: PrimaryKey,
    pub(crate) name: Option<Name>,
}

#[derive(Debug, Serialize, Insertable)]
#[diesel(table_name = team)]
pub(crate) struct Inclusive {
    pub(crate) id: PrimaryKey,
    pub(crate) handle: Handle,
    pub(crate) name: Name,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = team)]
#[allow(dead_code)]
pub(crate) struct TeamTable {
    pub(crate) id: PrimaryKey,
    pub(crate) handle: Handle,
    pub(crate) name: Name,
    pub(crate) created_at: DateTime<Utc>,
    pub(crate) updated_at: DateTime<Utc>,
    pub(crate) deleted_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = team)]
pub(crate) struct Team {
    pub(crate) id: PrimaryKey,
    pub(crate) handle: Handle,
    pub(crate) name: Name,
}

impl InternalIdentifier for Team {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.id.to_string()))
    }
}
