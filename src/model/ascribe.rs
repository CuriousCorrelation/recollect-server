use chrono::{DateTime, Utc};
use diesel::{Identifiable, Insertable, Queryable};
use serde::Serialize;

use crate::store::schema::ascribe;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    AuditField, PrimaryKey,
};

#[derive(Debug, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Id(PrimaryKey),
    DocumentId(PrimaryKey),
    Unique {
        document_id: PrimaryKey,
        team_id: PrimaryKey,
    },
}

#[derive(Debug, Serialize)]
pub(crate) struct Partial {
    pub(crate) document_id: Option<PrimaryKey>,
    pub(crate) team_id: Option<PrimaryKey>,
    pub(crate) created_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize, Insertable)]
#[diesel(table_name = ascribe)]
pub(crate) struct Inclusive {
    pub(crate) document_id: PrimaryKey,
    pub(crate) team_id: PrimaryKey,
}

#[derive(Debug, Serialize, Queryable, Identifiable)]
#[diesel(table_name = ascribe)]
pub(crate) struct Ascribe {
    pub(crate) id: PrimaryKey,
    pub(crate) document_id: PrimaryKey,
    pub(crate) team_id: PrimaryKey,
    pub(crate) created_at: DateTime<Utc>,
}

impl InternalIdentifier for Ascribe {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.id.to_string()))
    }
}
