use diesel_derive_enum::DbEnum;
use serde::{Deserialize, Serialize};
use strum::{Display, EnumString};

#[derive(PartialEq, Eq, Debug, Serialize, Deserialize, EnumString, Display, Clone, DbEnum)]
#[ExistingTypePath = "crate::store::schema::sql_types::Provider"]
#[DbValueStyle = "snake_case"]
#[serde(rename_all = "snake_case")]
pub(crate) enum Provider {
    Google,
    Facebook,
    Microsoft,
    Twitter,
}
