#[derive(Debug, Clone)]
pub(crate) struct ConnectionInfo {
    pub(crate) host: Option<String>,
    pub(crate) scheme: Option<String>,
    pub(crate) remote_addr: Option<String>,
}
