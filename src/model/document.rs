use chrono::{DateTime, Utc};
use diesel::{AsChangeset, Identifiable, Insertable, Queryable};
use serde::Serialize;
use serde_json::Value;

use crate::store::schema::document;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    AuditField, Handle, PrimaryKey,
};

#[derive(Debug, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Id(PrimaryKey),
    Handle(Handle),
}

#[derive(Debug, Serialize)]
pub(crate) struct Partial {
    pub(crate) content: Option<String>,
    pub(crate) created_at: Option<DateTime<Utc>>,
    pub(crate) updated_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize, AsChangeset)]
#[diesel(table_name = document)]
pub(crate) struct Exclusive {
    pub(crate) id: PrimaryKey,
    pub(crate) handle: Handle,
    pub(crate) content: Option<Value>,
}

#[derive(Debug, Serialize, Insertable)]
#[diesel(table_name = document)]
pub(crate) struct Inclusive {
    pub(crate) id: PrimaryKey,
    pub(crate) handle: Handle,
    pub(crate) content: Value,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = document)]
#[allow(dead_code)]
pub(crate) struct DocumentTable {
    pub(crate) id: PrimaryKey,
    pub(crate) handle: Handle,
    pub(crate) content: Value,
    pub(crate) created_at: DateTime<Utc>,
    pub(crate) updated_at: DateTime<Utc>,
    pub(crate) deleted_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = document)]
pub(crate) struct Document {
    pub(crate) id: PrimaryKey,
    pub(crate) handle: Handle,
    pub(crate) content: Value,
    pub(crate) created_at: DateTime<Utc>,
    pub(crate) updated_at: DateTime<Utc>,
}

impl InternalIdentifier for Document {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.id.to_string()))
    }
}
