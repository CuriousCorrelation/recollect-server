pub(crate) trait MonoStore<T> {
    fn mono_store(store: T) -> Self;
}
