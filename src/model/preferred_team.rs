use chrono::{DateTime, Utc};
use diesel::{AsChangeset, Identifiable, Insertable, Queryable};
use serde::Serialize;

use crate::store::schema::preferred_team;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    AuditField, PrimaryKey,
};

#[derive(Debug, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Id(PrimaryKey),
    EntityId(PrimaryKey),
    TeamId(PrimaryKey),
    Unique {
        team_id: PrimaryKey,
        entity_id: PrimaryKey,
    },
}

#[derive(Debug, Serialize)]
pub(crate) struct Partial {
    pub(crate) entity_id: Option<PrimaryKey>,
    pub(crate) team_id: Option<PrimaryKey>,
    pub(crate) created_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize, Insertable)]
#[diesel(table_name = preferred_team)]
pub(crate) struct Inclusive {
    pub(crate) entity_id: PrimaryKey,
    pub(crate) team_id: PrimaryKey,
}

#[derive(Debug, Serialize, AsChangeset)]
#[diesel(table_name = preferred_team)]
pub(crate) struct Exclusive {
    pub(crate) id: PrimaryKey,
    pub(crate) entity_id: Option<PrimaryKey>,
    pub(crate) team_id: Option<PrimaryKey>,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = preferred_team)]
#[allow(dead_code)]
pub(crate) struct PreferredTeamTable {
    pub(crate) id: PrimaryKey,
    pub(crate) entity_id: PrimaryKey,
    pub(crate) team_id: PrimaryKey,
    pub(crate) created_at: DateTime<Utc>,
    pub(crate) updated_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = preferred_team)]
pub(crate) struct PreferredTeam {
    pub(crate) id: PrimaryKey,
    pub(crate) entity_id: PrimaryKey,
    pub(crate) team_id: PrimaryKey,
    pub(crate) updated_at: Option<DateTime<Utc>>,
}

impl InternalIdentifier for PreferredTeam {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.id.to_string()))
    }
}
