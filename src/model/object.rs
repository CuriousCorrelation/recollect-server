use chrono::{DateTime, Utc};
use diesel::{Identifiable, Queryable};
use serde::Serialize;

use crate::store::schema::object;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    AuditField, PrimaryKey,
};

#[derive(Debug, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Id(PrimaryKey),
}

#[derive(Debug, Serialize)]
pub(crate) struct Inclusive {}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = object)]
pub(crate) struct Object {
    pub(crate) id: PrimaryKey,
    pub(crate) created_at: DateTime<Utc>,
}

impl InternalIdentifier for Object {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.id.to_string()))
    }
}
