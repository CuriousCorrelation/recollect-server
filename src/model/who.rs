use serde::Serialize;

use super::PrimaryKey;

#[derive(Debug, Clone, Copy, Serialize)]
pub(crate) struct Who {
    pub(crate) entity_id: PrimaryKey,
    pub(crate) team_id: PrimaryKey,
}

impl Who {
    pub(crate) fn is_world(Who { entity_id, team_id }: &Self) -> bool {
        0.eq(entity_id) && 0.eq(team_id)
    }
}
