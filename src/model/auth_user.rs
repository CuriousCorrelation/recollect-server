use serde::{Deserialize, Serialize};

use super::name::Name;

/// Represents authenticated user.
///
/// As per <https://openid.net/specs/openid-connect-core-1_0.html> Section 5.1.
/// It work both with `OAuth` or other authentication methods.
/// A provider must return an `AuthUser` for [crate::compose::entry::Entry] like APIs.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct AuthUser {
    pub(crate) id: String,
    pub(crate) name: Name,
    pub(crate) email: String,
    pub(crate) picture: String,
}
