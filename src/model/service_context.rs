use uuid::Uuid;

use super::AuditField;

pub(crate) trait ServiceContext {
    fn service_id(&self) -> Uuid;
    fn version(&self) -> AuditField;
    fn context(&self) -> AuditField;
}
