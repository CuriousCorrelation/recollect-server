use serde::Deserialize;

use super::Secret;

#[derive(Debug, Clone, Deserialize)]
#[serde(rename = "api_key")]
pub(crate) struct ApiKey(pub(crate) Secret);

#[derive(Debug, Clone, Default)]
pub(crate) enum ApiRequest {
    Identity(Secret),
    ApiKey(Secret),
    #[default]
    UnknownApiAccessor,
}
