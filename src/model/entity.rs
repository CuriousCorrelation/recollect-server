use chrono::{DateTime, Utc};
use diesel::{AsChangeset, Identifiable, Insertable, Queryable};
use serde::Serialize;

use crate::store::schema::entity;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    name::Name,
    AuditField, Handle, PrimaryKey, Secret,
};

pub(crate) struct EntityId(pub(crate) PrimaryKey);

#[derive(Debug, PartialEq, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Id(PrimaryKey),
    Secret(Secret),
    Handle(Handle),
}

#[derive(Debug, Serialize)]
pub(crate) struct Partial {
    pub(crate) name: Option<Name>,
}

#[derive(Debug, Serialize, AsChangeset)]
#[diesel(table_name = entity)]
pub(crate) struct Exclusive {
    pub(crate) id: PrimaryKey,
    pub(crate) handle: Option<Handle>,
    pub(crate) name: Option<Name>,
}

#[derive(Debug, Serialize, Insertable)]
#[diesel(table_name = entity)]
pub(crate) struct Inclusive {
    pub(crate) id: PrimaryKey,
    pub(crate) handle: Handle,
    pub(crate) secret: Secret,
    pub(crate) name: Name,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = entity)]
#[allow(dead_code)]
pub(crate) struct EntityTable {
    pub(crate) id: PrimaryKey,
    pub(crate) handle: Handle,
    pub(crate) secret: Secret,
    pub(crate) name: Name,
    pub(crate) created_at: DateTime<Utc>,
    pub(crate) updated_at: DateTime<Utc>,
    pub(crate) deleted_at: Option<DateTime<Utc>>,
}

#[derive(Debug, PartialEq, Queryable, Identifiable)]
#[diesel(table_name = entity)]
pub(crate) struct Entity {
    pub(crate) id: PrimaryKey,
    pub(crate) handle: Handle,
    pub(crate) secret: Secret,
    pub(crate) name: Name,
}

impl InternalIdentifier for Entity {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.id.to_string()))
    }
}
