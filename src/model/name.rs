use derive_more::Display;
use diesel_derive_newtype::DieselNewType;
use serde::{Deserialize, Serialize};

#[derive(DieselNewType, Clone, Debug, Serialize, Deserialize, Display, Hash, PartialEq, Eq)]
#[serde(transparent)]
pub struct Name(String);
