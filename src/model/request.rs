pub(crate) use serde::Serialize;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    who::Who,
};

#[derive(Debug, Serialize)]
pub(crate) struct Request<R> {
    pub(crate) who: Who,
    pub(crate) request: R,
}

impl<R> Request<R> {
    pub(crate) fn new(who: Who, request: R) -> Self {
        Self { who, request }
    }

    pub(crate) fn world(request: R) -> Self {
        Self {
            who: Who {
                entity_id: 0,
                team_id: 0,
            },
            request,
        }
    }
}

impl<R: InternalIdentifier> InternalIdentifier for Request<R> {
    fn internal_identifier(&self) -> Identifier {
        self.request.internal_identifier()
    }
}

impl<R: InternalIdentifier> InternalIdentifier for Request<Vec<R>> {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mult(
            self.request
                .iter()
                .flat_map(|request| match request.internal_identifier() {
                    Identifier::Mono(m) => vec![m].into_iter(),
                    Identifier::Mult(m) => m.into_iter(),
                })
                .collect(),
        )
    }
}
