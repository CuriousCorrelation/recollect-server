use super::{name::Name, Secret};

/// A [`EntryRecord`] is a set of minimum required information
/// to create a new entry, including an [`super::entity::Entity`], a [`super::team::Team`]
/// and several measures.
#[derive(Debug, Clone)]
pub(crate) struct EntryRecord {
    pub(crate) name: Name,
    pub(crate) secret: Secret,
}
