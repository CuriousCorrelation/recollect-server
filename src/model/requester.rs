use chrono::{DateTime, Utc};
use diesel::{Identifiable, Insertable, Queryable};
use serde::Serialize;

use crate::store::schema::requester;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    AuditField, PrimaryKey, Secret,
};

#[derive(Debug, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Id(PrimaryKey),
    Secret(Secret),
}

#[derive(Debug, Serialize, Insertable)]
#[diesel(table_name = requester)]
pub(crate) struct Inclusive {
    pub(crate) secret: Secret,
    pub(crate) host: Option<String>,
    pub(crate) scheme: Option<String>,
    pub(crate) remote_addr: Option<String>,
}

#[derive(Debug, Serialize)]
pub(crate) struct Partial {
    pub(crate) secret: Option<Secret>,
    pub(crate) host: Option<String>,
    pub(crate) scheme: Option<String>,
    pub(crate) remote_addr: Option<String>,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = requester)]
#[allow(dead_code)]
pub(crate) struct RequesterTable {
    pub(crate) id: PrimaryKey,
    pub(crate) secret: Secret,
    pub(crate) host: Option<String>,
    pub(crate) scheme: Option<String>,
    pub(crate) remote_addr: Option<String>,
    pub(crate) created_at: DateTime<Utc>,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = requester)]
pub(crate) struct Requester {
    pub(crate) id: PrimaryKey,
    pub(crate) secret: Secret,
    pub(crate) host: Option<String>,
    pub(crate) scheme: Option<String>,
    pub(crate) remote_addr: Option<String>,
}

impl InternalIdentifier for Requester {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.id.to_string()))
    }
}
