use std::sync::{Arc, Mutex};

use axum::extract::FromRef;
use axum_extra::extract::cookie::Key;

/// Represents one [`Send`] + [`Sync`] service.
#[derive(Clone)]
pub(crate) struct SyncService<Service> {
    service: Arc<Mutex<Service>>,
}

impl<Service> SyncService<Service>
where
    Service: Clone,
{
    pub(crate) fn new(service: Service) -> Self {
        Self {
            service: Arc::new(Mutex::new(service)),
        }
    }

    pub(crate) fn service(&self) -> Service {
        self.service.lock().unwrap().clone()
    }
}

/// Represents loosely coupled one state and one [`Send`] + [`Sync`] service.
#[derive(Clone)]
pub(crate) struct MonoStateSyncService<State, Service> {
    state: State,
    service: Arc<Mutex<Service>>,
}

impl<State, Service> MonoStateSyncService<State, Service>
where
    Service: Clone,
{
    pub(crate) fn new(state: State, service: Service) -> Self {
        Self {
            state,
            service: Arc::new(Mutex::new(service)),
        }
    }

    pub(crate) fn state(&self) -> &State {
        &self.state
    }

    pub(crate) fn service(&self) -> Service {
        self.service.lock().unwrap().clone()
    }
}

impl<Service> FromRef<MonoStateSyncService<Key, Service>> for Key {
    fn from_ref(input: &MonoStateSyncService<Key, Service>) -> Self {
        input.state.clone()
    }
}
