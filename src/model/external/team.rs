use serde::{Deserialize, Serialize};

use crate::model::{name::Name, Handle};

#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Handle(Handle),
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Partial {
    pub(crate) name: Option<Name>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Exclusive {
    pub(crate) identify: Identify,
    pub(crate) name: Option<Name>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Inclusive {
    pub(crate) name: Name,
}

#[derive(Debug, PartialEq, Clone, Serialize)]
pub(crate) struct Team {
    pub(crate) handle: Handle,
    pub(crate) name: Name,
}
