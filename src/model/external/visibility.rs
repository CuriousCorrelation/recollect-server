use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use crate::model::scope::Scope;

use super::composite_identify::CompositeIdentify;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Object(CompositeIdentify),
    Unique {
        object: CompositeIdentify,
        scope: Scope,
    },
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Partial {
    pub(crate) scope: Option<Scope>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Exclusive {
    pub(crate) object: CompositeIdentify,
    pub(crate) scope: Option<Scope>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Inclusive {
    pub(crate) object: CompositeIdentify,
    pub(crate) scope: Scope,
}

#[derive(Debug, Serialize)]
pub(crate) struct Visibility {
    pub(crate) object: CompositeIdentify,
    pub(crate) scope: Scope,
    pub(crate) updated_at: DateTime<Utc>,
}
