use diesel::Queryable;
use serde::{Deserialize, Serialize};

use crate::model::{name::Name, Handle};

#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Handle(Handle),
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Partial {
    pub(crate) name: Name,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Exclusive {
    pub(crate) identify: Identify,
    pub(crate) handle: Option<Handle>,
    pub(crate) name: Option<Name>,
}

#[derive(Debug, PartialEq, Serialize, Queryable)]
pub(crate) struct Entity {
    pub(crate) handle: Handle,
    pub(crate) name: Name,
}
