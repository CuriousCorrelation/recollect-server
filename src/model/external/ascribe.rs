use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use crate::model::Handle;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Unique { document: Handle, team: Handle },
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Partial {
    pub(crate) document: Option<Handle>,
    pub(crate) team: Option<Handle>,
    pub(crate) created_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Inclusive {
    pub(crate) document: Handle,
    pub(crate) team: Handle,
}

#[derive(Debug, Serialize)]
pub(crate) struct Ascribe {
    pub(crate) document: Handle,
    pub(crate) team: Handle,
    pub(crate) created_at: DateTime<Utc>,
}
