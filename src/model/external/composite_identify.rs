use serde::{Deserialize, Serialize};

use crate::model;

#[derive(Debug, PartialEq, Clone, Copy, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum CompositeIdentify {
    Entity(model::external::entity::Identify),
    Team(model::external::team::Identify),
    Document(model::external::document::Identify),
}
