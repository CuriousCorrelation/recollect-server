use chrono::{DateTime, Utc};
use diesel::Queryable;
use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::model::Handle;

#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Handle(Handle),
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Partial {
    pub(crate) content: Option<String>,
    pub(crate) created_at: Option<DateTime<Utc>>,
    pub(crate) updated_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Exclusive {
    pub(crate) identify: Identify,
    pub(crate) content: Option<Value>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Inclusive {
    pub(crate) content: Value,
}

#[derive(PartialEq, Serialize, Queryable)]
pub(crate) struct Document {
    pub(crate) handle: Handle,
    pub(crate) content: Value,
    pub(crate) created_at: DateTime<Utc>,
    pub(crate) updated_at: DateTime<Utc>,
}

impl std::fmt::Debug for Document {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Document")
            .field("handle", &self.handle)
            .field("content", &"[...]")
            .field("created_at", &self.created_at)
            .field("updated_at", &self.updated_at)
            .finish()
    }
}
