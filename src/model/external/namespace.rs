use serde::{Deserialize, Serialize};

use crate::model::Handle;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Document(Handle),
    Entity(Handle),
    Team(Handle),
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Partial {
    pub(crate) handle: Option<Identify>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Exclusive {
    pub(crate) id: Identify,
    pub(crate) handle: Handle,
}

#[derive(Debug, Serialize)]
pub(crate) struct Namespace {
    pub(crate) handle: Handle,
}
