use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use crate::model::action::Action;

use super::composite_identify::CompositeIdentify;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Unique {
        originate: CompositeIdentify,
        destinate: CompositeIdentify,
    },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct Partial {
    pub(crate) originate: Option<CompositeIdentify>,
    pub(crate) action: Option<Action>,
    pub(crate) destinate: Option<CompositeIdentify>,
    pub(crate) expiry_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Exclusive {
    pub(crate) identify: Identify,
    pub(crate) action: Option<Action>,
    pub(crate) expiry_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Inclusive {
    pub(crate) originate: CompositeIdentify,
    pub(crate) action: Action,
    pub(crate) destinate: CompositeIdentify,
    pub(crate) expiry_at: Option<DateTime<Utc>>,
}

#[derive(Debug, PartialEq, Serialize)]
pub(crate) struct Blockade {
    pub(crate) originate: CompositeIdentify,
    pub(crate) action: Action,
    pub(crate) destinate: CompositeIdentify,
    pub(crate) expiry_at: Option<DateTime<Utc>>,
    pub(crate) created_at: DateTime<Utc>,
}
