use std::fmt::Debug;

use chrono::{DateTime, Utc};
use diesel::Queryable;
use serde::Serialize;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    AuditField, Secret,
};

#[derive(Debug, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Id(Secret),
}

#[derive(Debug, Serialize)]
pub(crate) struct Inclusive {
    pub(crate) secret: Secret,
}

#[derive(Debug, Queryable)]
#[diesel(table_name = session)]
#[allow(dead_code)]
pub(crate) struct SessionTable {
    pub(crate) id: Secret,
    pub(crate) secret: Secret,
    pub(crate) fetched_at: DateTime<Utc>,
    pub(crate) created_at: DateTime<Utc>,
}

#[derive(Debug, Queryable)]
#[diesel(table_name = session)]
pub(crate) struct Session {
    pub(crate) id: Secret,
    pub(crate) secret: Secret,
}

impl InternalIdentifier for Session {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.id))
    }
}
