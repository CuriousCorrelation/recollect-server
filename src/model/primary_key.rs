use super::{
    audit_field::AuditField,
    internal_identifier::{Identifier, InternalIdentifier},
};

pub(crate) type PrimaryKey = i64;

impl InternalIdentifier for PrimaryKey {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.to_string()))
    }
}
