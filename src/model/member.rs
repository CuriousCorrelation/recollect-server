use chrono::{DateTime, Utc};
use diesel::{Identifiable, Insertable, Queryable};
use serde::Serialize;

use crate::store::schema::member;

use super::{
    internal_identifier::{Identifier, InternalIdentifier},
    AuditField, PrimaryKey,
};

#[derive(Debug, Serialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Identify {
    Id(PrimaryKey),
    Unique {
        entity_id: PrimaryKey,
        team_id: PrimaryKey,
    },
}

#[derive(Debug, Serialize)]
pub(crate) struct Partial {
    pub(crate) entity_id: Option<PrimaryKey>,
    pub(crate) team_id: Option<PrimaryKey>,
    pub(crate) created_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize, Insertable)]
#[diesel(table_name = member)]
pub(crate) struct Inclusive {
    pub(crate) entity_id: PrimaryKey,
    pub(crate) team_id: PrimaryKey,
}

#[derive(Debug, Queryable, Identifiable)]
#[diesel(table_name = member)]
pub(crate) struct Member {
    pub(crate) id: PrimaryKey,
    pub(crate) entity_id: PrimaryKey,
    pub(crate) team_id: PrimaryKey,
    pub(crate) created_at: DateTime<Utc>,
}

impl InternalIdentifier for Member {
    fn internal_identifier(&self) -> Identifier {
        Identifier::Mono(AuditField::from_str_truncate(self.id.to_string()))
    }
}
