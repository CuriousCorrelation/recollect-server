use thiserror::Error;

use crate::compose;

use super::oauth;

#[derive(Debug, Error)]
pub enum AuthError {
    #[error("OAuth - {0}")]
    OAuth(#[from] oauth::error::OAuthError),
}

pub(crate) type AuthResult<T> = std::result::Result<T, AuthError>;
