use thiserror::Error;

use crate::compose;

#[derive(Debug, Error)]
pub enum OAuthError {
    #[error("Error - Failed to load config file - {0}")]
    ConfigIo(#[from] std::io::Error),

    #[error("Error - Failed to parse config file - {0}")]
    ConfigParse(#[from] toml::de::Error),

    #[error("Error - Failed to request token from OAuth provider")]
    TokenRequest,

    #[error("Error - CSRF verification failed")]
    CsrfVerify,

    #[error("Error - PKCE code missing")]
    MissingPkce,

    #[error("Error - Invalid auth URL - {0}")]
    AuthUrl(url::ParseError),

    #[error("Error - Invalid token URL - {0}")]
    TokenUrl(url::ParseError),

    #[error("Error - Invalid redirect URL - {0}")]
    RedirectUrl(url::ParseError),

    #[error("Error - Invalid revocation URL - {0}")]
    RevocationUrl(url::ParseError),

    #[error("Error - Invalid userinfo URL - {0}")]
    UserinfoUrl(url::ParseError),

    #[error("Error - Failed to send oauth request - {0}")]
    Request(reqwest::Error),

    #[error("Error - Failed to deserialize oauth provider response - {0}")]
    JsonPayload(reqwest::Error),

    #[error("Error - Missing client_id")]
    ClientIdMissing,

    #[error("Error - Missing client_secret")]
    ClientSecretMissing,

    #[error("Error - Missing auth_url")]
    AuthUrlMissing,

    #[error("Error - Missing token_url")]
    TokenUrlMissing,

    #[error("Error - Missing redirect_url")]
    RedirectUrlMissing,

    #[error("Error - Missing revocation_url")]
    RevocationUrlMissing,

    #[error("Error - Missing userinfo_url")]
    UserinfoUrlMissing,

    #[error("Error - Missing scopes")]
    ScopesMissing,

    #[error("Error - Missing provider")]
    ProviderMissing,
}

pub(crate) type OAuthResult<T> = std::result::Result<T, OAuthError>;
