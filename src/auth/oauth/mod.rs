pub(crate) mod error;
pub(crate) mod facebook;
pub(crate) mod google;
pub(crate) mod microsoft;
pub(crate) mod twitter;

pub(crate) use facebook::{Facebook, FacebookLayer};
pub(crate) use google::{Google, GoogleLayer};
pub(crate) use microsoft::{Microsoft, MicrosoftLayer};
pub(crate) use twitter::{Twitter, TwitterLayer};

use std::task::{ready, Context, Poll};

use futures::FutureExt;
use oauth2::{
    basic::BasicClient, AuthUrl, AuthorizationRequest, ClientId, ClientSecret, CsrfToken,
    PkceCodeChallenge, PkceCodeVerifier, RedirectUrl, RevocationUrl, Scope, TokenResponse,
    TokenUrl,
};
use reqwest::{Client, ClientBuilder};
use tower::{BoxError, Layer, Service};
use typed_builder::TypedBuilder;
use url::Url;

use crate::model::{
    auth::AuthResponse,
    auth_user::AuthUser,
    oauth::{OAuthRequest, SecurityTokens},
    provider::Provider,
    requisite::{Requisite, RequisiteView},
    BoxFuture,
};

use self::error::OAuthError;

use super::error::AuthError;

#[derive(Clone)]
pub(crate) struct OAuthProvider<S> {
    client: BasicClient,
    provider: Provider,
    userinfo_url: Url,
    scopes: Vec<Scope>,
    inner: S,
}

impl<S> OAuthProvider<S> {
    pub(crate) fn authorization_request(&self) -> (AuthorizationRequest, PkceCodeVerifier) {
        // Create a PKCE (Proof Key for Code Exchange (PKCE - https://oauth.net/2/pkce/))
        // code verifier and SHA-256 encode it as a code challenge.
        let (pkce_challenge, pkce_verifier) = PkceCodeChallenge::new_random_sha256();

        let auth_request = self.scopes.iter().fold(
            self.client
                .authorize_url(CsrfToken::new_random)
                .set_pkce_challenge(pkce_challenge),
            |acc, scope| acc.add_scope(scope.clone()),
        );

        (auth_request, pkce_verifier)
    }
}

impl<S> Service<Requisite<OAuthRequest>> for OAuthProvider<S>
where
    S: Service<Requisite<AuthResponse>> + Clone + Send + 'static,
    S::Future: Send,
    S::Error: From<AuthError>,
{
    type Response = S::Response;

    type Error = S::Error;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx));

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, requisite: Requisite<OAuthRequest>) -> Self::Future {
        let client = self.client.clone();
        let provider = self.provider.clone();
        let userinfo_url = self.userinfo_url.clone();

        let clone = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone);

        let RequisiteView {
            view:
                OAuthRequest {
                    security_tokens: SecurityTokens { state, code },
                    pkce_verifier,
                },
            requisite,
        } = requisite.pre_req();

        async move {
            let token_response = client
                .exchange_code(
                    code.ok_or_else(|| OAuthError::MissingPkce)
                        .map_err(Into::into)?,
                )
                .set_pkce_verifier(pkce_verifier)
                .request_async(oauth2::reqwest::async_http_client)
                .await
                .map_err(|_| OAuthError::TokenRequest)
                .map_err(Into::into)?;

            let token = token_response.access_token().secret();

            let auth_user = Client::new()
                .get(userinfo_url)
                .bearer_auth(token)
                .send()
                .await
                .map_err(OAuthError::Request)
                .map_err(Into::into)?
                .json::<AuthUser>()
                .await
                .map_err(OAuthError::JsonPayload)
                .map_err(Into::into)?;

            let requisite = requisite.with_pre(AuthResponse {
                auth_user,
                provider,
            });

            inner.call(requisite).await
        }
        .boxed()
    }
}
