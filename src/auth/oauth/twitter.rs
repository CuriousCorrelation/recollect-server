use std::{
    path::Path,
    task::{ready, Context, Poll},
};

use oauth2::{
    basic::BasicClient, AuthUrl, AuthorizationRequest, ClientId, ClientSecret, PkceCodeVerifier,
    RedirectUrl, RevocationUrl, Scope, TokenUrl,
};
use serde::Deserialize;
use tower::{BoxError, Layer, Service};
use typed_builder::TypedBuilder;
use url::Url;

use crate::{
    auth::error::AuthError,
    model::{
        auth::AuthResponse, oauth::OAuthRequest, provider::Provider, requisite::Requisite,
        BoxFuture,
    },
};

use super::{
    error::{OAuthError, OAuthResult},
    OAuthProvider,
};

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct TwitterLayer {
    client_id: ClientId,
    client_secret: Option<ClientSecret>,
    redirect_url: RedirectUrl,
    auth_url: AuthUrl,
    token_url: Option<TokenUrl>,
    revocation_url: RevocationUrl,
    userinfo_url: Url,
    scopes: Vec<Scope>,
    provider: Provider,
}

impl<S> Layer<S> for TwitterLayer {
    type Service = Twitter<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Twitter(OAuthProvider {
            client: BasicClient::new(
                self.client_id.clone(),
                self.client_secret.clone(),
                self.auth_url.clone(),
                self.token_url.clone(),
            )
            .set_redirect_uri(self.redirect_url.clone())
            .set_revocation_uri(self.revocation_url.clone()),
            provider: self.provider.clone(),
            userinfo_url: self.userinfo_url.clone(),
            scopes: self.scopes.clone(),
            inner,
        })
    }
}

#[derive(Clone)]
pub(crate) struct Twitter<S>(pub(crate) OAuthProvider<S>);

impl<S> Twitter<S> {
    pub(crate) fn authorization_request(&self) -> (AuthorizationRequest, PkceCodeVerifier) {
        self.0.authorization_request()
    }
}

impl<S> Service<Requisite<OAuthRequest>> for Twitter<S>
where
    S: Service<Requisite<AuthResponse>> + Clone + Send + 'static,
    S::Future: Send,
    S::Error: From<AuthError>,
{
    type Response = S::Response;

    type Error = S::Error;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.0.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, requisite: Requisite<OAuthRequest>) -> Self::Future {
        self.0.call(requisite)
    }
}

impl TwitterLayer {
    pub(crate) fn new(path: &Path) -> OAuthResult<Self> {
        #[derive(Deserialize)]
        struct Config {
            oauth: TwitterOAuthConfig,
        }

        #[derive(Deserialize)]
        struct TwitterOAuthConfig {
            twitter: TwitterPartial,
        }

        #[derive(Deserialize)]
        struct TwitterPartial {
            client_id: ClientId,
            client_secret: Option<ClientSecret>,
            redirect_url: RedirectUrl,
            auth_url: AuthUrl,
            token_url: Option<TokenUrl>,
            revocation_url: RevocationUrl,
            userinfo_url: Url,
            scopes: Vec<Scope>,
        }

        let TwitterPartial {
            client_id,
            client_secret,
            redirect_url,
            auth_url,
            token_url,
            revocation_url,
            userinfo_url,
            scopes,
        } = toml::from_str::<Config>(&std::fs::read_to_string(path)?)?
            .oauth
            .twitter;

        Ok(Self::builder()
            .client_id(client_id)
            .client_secret(client_secret)
            .redirect_url(redirect_url)
            .auth_url(auth_url)
            .token_url(token_url)
            .revocation_url(revocation_url)
            .userinfo_url(userinfo_url)
            .scopes(scopes)
            .provider(Provider::Twitter)
            .build())
    }
}
