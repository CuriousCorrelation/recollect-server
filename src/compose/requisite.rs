use std::{
    convert::Infallible,
    net::SocketAddr,
    task::{ready, Context, Poll},
};

use arraystring::MaxString;
use axum::{
    extract::{rejection::FailedToDeserializeQueryString, ConnectInfo, Query},
    Extension, RequestExt,
};
use axum_client_ip::SecureClientIp;
use axum_extra::extract::{cookie::Key, PrivateCookieJar};
use futures::FutureExt;
use http::{header::COOKIE, Uri};
use tower::{BoxError, Layer, Service};
use tracing::info;
use typed_builder::TypedBuilder;

use crate::{
    error::AccessViolation,
    global::COOKIE_NAME,
    model::{
        api_request::{ApiKey, ApiRequest},
        connection_info::ConnectionInfo,
        requisite::{self, EmptyRequisite},
        who::Who,
        BoxFuture, Request, Secret,
    },
};

use super::error::ComposeError;

#[derive(Clone)]
pub(crate) struct RequisiteLayer {
    key: Key,
}

impl RequisiteLayer {
    pub(crate) fn new(key: Key) -> Self {
        Self { key }
    }
}

impl<S> Layer<S> for RequisiteLayer {
    type Service = Requisite<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Requisite {
            inner,
            key: self.key.clone(),
        }
    }
}

#[derive(Clone, TypedBuilder)]
pub(crate) struct Requisite<S> {
    key: Key,
    inner: S,
}

impl<S, B> Service<http::Request<B>> for Requisite<S>
where
    S: Service<http::Request<B>> + Clone + Send + 'static,
    B: Send + 'static,
    S::Future: Send,
{
    type Response = S::Response;
    type Error = S::Error;
    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx).map_err(Into::into))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, mut req: http::Request<B>) -> Self::Future {
        let inner = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, inner);
        let key = self.key.clone();

        async move {
            // Get `api_key` from query, if it exists, otherwise check if there's
            // a cookie with value for `COOKIE_NAME`, otherwise default to `ApiRequest::None`.
            // If both (`api_key` and cookie `identity`) are present,
            // `api_key` will have priority over cookie `identity`.
            // This also means if current `identity` has access to other's `api_key`,
            // `identity` will have full access.
            //
            // TODO: Check for `identity` & `api_key` belonging to same `Entity`.
            let api_request = if let Ok(Query(ApiKey(api_key))) = req.extract_parts().await {
                ApiRequest::ApiKey(Secret::from_str_truncate(api_key))
            } else if let Some(identity) = PrivateCookieJar::from_headers(req.headers(), key)
                .get(COOKIE_NAME)
                .map(|cookie| cookie.value().to_owned())
            {
                ApiRequest::Identity(Secret::from_str_truncate(identity))
            } else {
                ApiRequest::UnknownApiAccessor
            };

            let host = req.uri().host().map(String::from);
            let scheme = req.uri().scheme_str().map(String::from);
            let remote_addr = req
                .extract_parts::<Extension<ConnectInfo<SocketAddr>>>()
                .await
                .ok()
                .map(|addr| addr.to_string());

            let connection_info = ConnectionInfo {
                host,
                scheme,
                remote_addr,
            };

            let requisite = requisite::Requisite::empty(api_request, connection_info);

            req.extensions_mut().insert(requisite);

            inner.call(req).await
        }
        .boxed()
    }
}
