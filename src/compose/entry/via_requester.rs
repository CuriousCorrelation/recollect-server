use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service};
use typed_builder::TypedBuilder;

use crate::{
    compose::error::ComposeError,
    model::{
        connection_info::ConnectionInfo,
        entity::EntityId,
        entry_record::EntryRecord,
        member, ops, requester,
        requisite::{Requisite, RequisiteView},
        service::{RequesterAssure, RequesterCreate},
        BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Clone, TypedBuilder)]
pub(crate) struct ViaRequesterLayer {
    requester_assure: RequesterAssure<StoreError>,
    requester_create: RequesterCreate<StoreError>,
}

impl<S> Layer<S> for ViaRequesterLayer {
    type Service = ViaRequester<S>;

    fn layer(&self, inner: S) -> Self::Service {
        ViaRequester {
            inner,
            requester_assure: self.requester_assure.clone(),
            requester_create: self.requester_create.clone(),
        }
    }
}

#[derive(Clone)]
pub(crate) struct ViaRequester<S> {
    inner: S,
    requester_assure: RequesterAssure<StoreError>,
    requester_create: RequesterCreate<StoreError>,
}

impl<S> Service<RequisiteView<EntryRecord, EntityId>> for ViaRequester<S>
where
    S: Service<RequisiteView<EntryRecord, EntityId>> + Clone + Send + 'static,
    ComposeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ComposeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.requester_assure.poll_ready(cx))?;
        ready!(self.requester_create.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        RequisiteView {
            view: EntryRecord { name, secret },
            requisite,
        }: RequisiteView<EntryRecord, EntityId>,
    ) -> Self::Future {
        let requester_assure = self.requester_assure.clone();
        let mut requester_assure = std::mem::replace(&mut self.requester_assure, requester_assure);

        let requester_create = self.requester_create.clone();
        let mut requester_create = std::mem::replace(&mut self.requester_create, requester_create);

        let inner = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, inner);

        async move {
            let ConnectionInfo {
                host,
                scheme,
                remote_addr,
            } = requisite.connection_info();

            let _requester_id = match requester_assure
                .call(ops::Assure(Request::world(requester::Identify::Secret(
                    secret,
                ))))
                .await
            {
                Ok(requester_id) => Ok(requester_id.request),
                Err(StoreError::NotFound) => {
                    let requester = requester_create
                        .call(ops::Create(Request::world(requester::Inclusive {
                            secret,
                            host,
                            scheme,
                            remote_addr,
                        })))
                        .await?
                        .request;

                    let id = requester.id;

                    Ok(id)
                }
                Err(e) => Err(ComposeError::Store(e)),
            }?;

            let requisite = requisite.with_view(EntryRecord { name, secret });

            inner.call(requisite).await.map_err(Into::into)
        }
        .boxed()
    }
}
