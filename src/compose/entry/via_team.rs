use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service};
use typed_builder::TypedBuilder;

use crate::{
    compose::error::ComposeError,
    model::{
        entity::Entity,
        entry_record::EntryRecord,
        member, ops, preferred_team,
        requisite::{Requisite, RequisiteView},
        service::{PreferredTeamFetch, TeamCreate},
        team, BoxFuture, Request,
    },
    store::error::StoreError,
};

use super::via_id_handle::ViaIdHandle;

#[derive(Clone, TypedBuilder)]
pub(crate) struct ViaTeamLayer {
    preferred_team_fetch: PreferredTeamFetch<StoreError>,
    via_id_handle: ViaIdHandle,
    team_create: TeamCreate<StoreError>,
}

impl<S> Layer<S> for ViaTeamLayer {
    type Service = ViaTeam<S>;

    fn layer(&self, inner: S) -> Self::Service {
        ViaTeam {
            inner,
            preferred_team_fetch: self.preferred_team_fetch.clone(),
            via_id_handle: self.via_id_handle.clone(),
            team_create: self.team_create.clone(),
        }
    }
}

#[derive(Clone)]
pub(crate) struct ViaTeam<S> {
    inner: S,
    preferred_team_fetch: PreferredTeamFetch<StoreError>,
    via_id_handle: ViaIdHandle,
    team_create: TeamCreate<StoreError>,
}

impl<S> Service<RequisiteView<EntryRecord, Entity>> for ViaTeam<S>
where
    S: Service<RequisiteView<EntryRecord, member::Inclusive>> + Clone + Send + 'static,
    ComposeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ComposeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.preferred_team_fetch.poll_ready(cx))?;
        ready!(self.via_id_handle.poll_ready(cx))?;
        ready!(self.team_create.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        RequisiteView { view, requisite }: RequisiteView<EntryRecord, Entity>,
    ) -> Self::Future {
        let preferred_team_fetch = self.preferred_team_fetch.clone();
        let mut preferred_team_fetch =
            std::mem::replace(&mut self.preferred_team_fetch, preferred_team_fetch);

        let team_create = self.team_create.clone();
        let mut team_create = std::mem::replace(&mut self.team_create, team_create);

        let via_id_handle = self.via_id_handle.clone();
        let mut via_id_handle = std::mem::replace(&mut self.via_id_handle, via_id_handle);

        let inner = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, inner);

        async move {
            let RequisiteView {
                view:
                    Entity {
                        id: entity_id,
                        handle,
                        secret,
                        name,
                    },
                requisite,
            } = requisite.pre_req();

            let team_id = match preferred_team_fetch
                .call(ops::Fetch(Request::world(
                    preferred_team::Identify::EntityId(entity_id),
                )))
                .await
            {
                Ok(preferred_team) => Ok(preferred_team.request.team_id),
                Err(StoreError::NotFound) => {
                    let (id, handle) = via_id_handle.call(()).await?;

                    let team = team_create
                        .call(ops::Create(Request::world(team::Inclusive {
                            id,
                            handle,
                            name,
                        })))
                        .await?
                        .request;

                    let id = team.id;

                    Ok(id)
                }
                Err(e) => Err(ComposeError::Store(e)),
            }?;

            let requisite = requisite
                .with_pre(member::Inclusive { entity_id, team_id })
                .with_view(view);

            inner.call(requisite).await.map_err(Into::into)
        }
        .boxed()
    }
}
