use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service};
use typed_builder::TypedBuilder;

use crate::{
    compose::error::ComposeError,
    model::{
        entity::{self, Entity},
        entry_record::EntryRecord,
        ops,
        requisite::{self, Requisite, RequisiteView},
        service::{EntityCreate, EntityFetch},
        BoxFuture, Request,
    },
    store::error::StoreError,
};

use super::via_id_handle::ViaIdHandle;

#[derive(Clone, TypedBuilder)]
pub(crate) struct ViaEntityLayer {
    entity_fetch: EntityFetch<StoreError>,
    via_id_handle: ViaIdHandle,
    entity_create: EntityCreate<StoreError>,
}

impl<S> Layer<S> for ViaEntityLayer {
    type Service = ViaEntity<S>;

    fn layer(&self, inner: S) -> Self::Service {
        ViaEntity {
            inner,
            entity_fetch: self.entity_fetch.clone(),
            via_id_handle: self.via_id_handle.clone(),
            entity_create: self.entity_create.clone(),
        }
    }
}

#[derive(Clone)]
pub(crate) struct ViaEntity<S> {
    inner: S,
    entity_fetch: EntityFetch<StoreError>,
    via_id_handle: ViaIdHandle,
    entity_create: EntityCreate<StoreError>,
}

impl<S> Service<Requisite<EntryRecord>> for ViaEntity<S>
where
    S: Service<RequisiteView<EntryRecord, Entity>> + Clone + Send + 'static,
    ComposeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ComposeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.entity_fetch.poll_ready(cx))?;
        ready!(self.via_id_handle.poll_ready(cx))?;
        ready!(self.entity_create.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, requisite: Requisite<EntryRecord>) -> Self::Future {
        let entity_fetch = self.entity_fetch.clone();
        let mut entity_fetch = std::mem::replace(&mut self.entity_fetch, entity_fetch);

        let entity_create = self.entity_create.clone();
        let mut entity_create = std::mem::replace(&mut self.entity_create, entity_create);

        let via_id_handle = self.via_id_handle.clone();
        let mut via_id_handle = std::mem::replace(&mut self.via_id_handle, via_id_handle);

        let inner = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, inner);

        async move {
            let RequisiteView { view, requisite } = requisite.pre_req();

            let EntryRecord { name, secret } = view.clone();

            let entity = match entity_fetch
                .call(ops::Fetch(Request::world(entity::Identify::Secret(secret))))
                .await
            {
                Ok(entity) => Ok(entity.request),
                Err(StoreError::NotFound) => {
                    let (id, handle) = via_id_handle.call(()).await?;

                    let entity = entity_create
                        .call(ops::Create(Request::world(entity::Inclusive {
                            name,
                            id,
                            handle,
                            secret,
                        })))
                        .await?
                        .request;

                    Ok(entity)
                }
                Err(e) => Err(ComposeError::Store(e)),
            }?;

            let requisite = requisite.with_pre(entity).with_view(view);

            inner.call(requisite).await.map_err(Into::into)
        }
        .boxed()
    }
}
