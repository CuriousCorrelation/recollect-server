use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service};
use typed_builder::TypedBuilder;

use crate::{
    compose::error::ComposeError,
    model::{
        entity::EntityId,
        entry_record::EntryRecord,
        label::Label,
        measure, member, ops,
        requisite::{Requisite, RequisiteView},
        service::{MeasureAssure, MeasureCreate},
        BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Clone, TypedBuilder)]
pub(crate) struct ViaEntityMeasureLayer {
    measure_assure: MeasureAssure<StoreError>,
    measure_create: MeasureCreate<StoreError>,
}

impl<S> Layer<S> for ViaEntityMeasureLayer {
    type Service = ViaEntityMeasure<S>;

    fn layer(&self, inner: S) -> Self::Service {
        ViaEntityMeasure {
            inner,
            measure_assure: self.measure_assure.clone(),
            measure_create: self.measure_create.clone(),
        }
    }
}

#[derive(Clone)]
pub(crate) struct ViaEntityMeasure<S> {
    inner: S,
    measure_assure: MeasureAssure<StoreError>,
    measure_create: MeasureCreate<StoreError>,
}

impl<S> Service<RequisiteView<EntryRecord, EntityId>> for ViaEntityMeasure<S>
where
    S: Service<RequisiteView<EntryRecord, EntityId>> + Clone + Send + 'static,
    ComposeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ComposeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.measure_assure.poll_ready(cx))?;
        ready!(self.measure_create.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        RequisiteView { view, requisite }: RequisiteView<EntryRecord, EntityId>,
    ) -> Self::Future {
        let measure_assure = self.measure_assure.clone();
        let mut measure_assure = std::mem::replace(&mut self.measure_assure, measure_assure);

        let measure_create = self.measure_create.clone();
        let mut measure_create = std::mem::replace(&mut self.measure_create, measure_create);

        let inner = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, inner);

        async move {
            let RequisiteView {
                view: EntityId(entity_id),
                requisite,
            } = requisite.pre_req();

            let _measure_id = match measure_assure
                .call(ops::Assure(Request::world(measure::Identify::Unique {
                    entity_id,
                    label: Label::Entity,
                })))
                .await
            {
                Ok(entity_measure_id) => Ok(entity_measure_id.request),
                Err(StoreError::NotFound) => {
                    let measure = measure_create
                        .call(ops::Create(Request::world(measure::Inclusive {
                            entity_id,
                            label: Label::Entity,
                            current: 1,
                            maximum: 1,
                        })))
                        .await?
                        .request;

                    let id = measure.id;

                    Ok(id)
                }
                Err(e) => Err(ComposeError::Store(e)),
            }?;

            let requisite = requisite.with_pre(EntityId(entity_id)).with_view(view);

            inner.call(requisite).await.map_err(Into::into)
        }
        .boxed()
    }
}
