use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::Service;
use typed_builder::TypedBuilder;

use crate::{
    compose::error::ComposeError,
    model::{
        entity::EntityId,
        entry_record::EntryRecord,
        member, ops,
        requisite::{Requisite, RequisiteView},
        service::SessionCreate,
        session::{Inclusive, Session},
        BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Clone, TypedBuilder)]
pub(crate) struct ViaSession {
    session_create: SessionCreate<StoreError>,
}

impl Service<RequisiteView<EntryRecord, EntityId>> for ViaSession {
    type Response = Session;

    type Error = ComposeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.session_create.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        RequisiteView {
            view: EntryRecord { name, secret },
            requisite,
        }: RequisiteView<EntryRecord, EntityId>,
    ) -> Self::Future {
        let session_create = self.session_create.clone();
        let mut session_create = std::mem::replace(&mut self.session_create, session_create);

        async move {
            let session = session_create
                .call(ops::Create(Request::world(Inclusive { secret })))
                .await?
                .request;

            // Login retrieves `id` via `session.id`.
            // This is not the `id` returned by `Assure` which often returns `PrimaryKey`.
            // This is `Secret` type.
            Ok(session)
        }
        .boxed()
    }
}
