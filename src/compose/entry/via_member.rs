use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service};
use typed_builder::TypedBuilder;

use crate::{
    compose::error::ComposeError,
    model::{
        entity::EntityId,
        entry_record::EntryRecord,
        member, ops,
        requisite::{Requisite, RequisiteView},
        service::{MemberAssure, MemberCreate},
        BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Clone, TypedBuilder)]
pub(crate) struct ViaMemberLayer {
    member_assure: MemberAssure<StoreError>,
    member_create: MemberCreate<StoreError>,
}

impl<S> Layer<S> for ViaMemberLayer {
    type Service = ViaMember<S>;

    fn layer(&self, inner: S) -> Self::Service {
        ViaMember {
            inner,
            member_assure: self.member_assure.clone(),
            member_create: self.member_create.clone(),
        }
    }
}

#[derive(Clone)]
pub(crate) struct ViaMember<S> {
    inner: S,
    member_assure: MemberAssure<StoreError>,
    member_create: MemberCreate<StoreError>,
}

impl<S> Service<RequisiteView<EntryRecord, member::Inclusive>> for ViaMember<S>
where
    S: Service<RequisiteView<EntryRecord, EntityId>> + Clone + Send + 'static,
    ComposeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ComposeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.member_assure.poll_ready(cx))?;
        ready!(self.member_create.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(
        &mut self,
        RequisiteView { view, requisite }: RequisiteView<EntryRecord, member::Inclusive>,
    ) -> Self::Future {
        let member_assure = self.member_assure.clone();
        let mut member_assure = std::mem::replace(&mut self.member_assure, member_assure);

        let member_create = self.member_create.clone();
        let mut member_create = std::mem::replace(&mut self.member_create, member_create);

        let inner = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, inner);

        async move {
            let RequisiteView {
                view: member::Inclusive { entity_id, team_id },
                requisite,
            } = requisite.pre_req();

            let _member_id = match member_assure
                .call(ops::Assure(Request::world(member::Identify::Unique {
                    entity_id,
                    team_id,
                })))
                .await
            {
                Ok(member_id) => Ok(member_id.request),
                Err(StoreError::NotFound) => {
                    let member = member_create
                        .call(ops::Create(Request::world(member::Inclusive {
                            entity_id,
                            team_id,
                        })))
                        .await?
                        .request;

                    let id = member.id;

                    Ok(id)
                }
                Err(e) => Err(ComposeError::Store(e)),
            }?;

            let requisite = requisite.with_pre(EntityId(entity_id)).with_view(view);

            inner.call(requisite).await.map_err(Into::into)
        }
        .boxed()
    }
}
