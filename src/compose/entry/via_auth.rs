use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service};
use typed_builder::TypedBuilder;

use crate::{
    compose::error::ComposeError,
    model::{
        auth::{self, AuthResponse},
        auth_user::AuthUser,
        entry_record::EntryRecord,
        ops,
        requisite::{self, Requisite, RequisiteView},
        service::{AuthAssure, AuthCreate, TokenCreate},
        token, BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Clone, TypedBuilder)]
pub(crate) struct ViaAuthLayer {
    auth_assure: AuthAssure<StoreError>,
    auth_create: AuthCreate<StoreError>,
    token_create: TokenCreate<StoreError>,
}

impl<S> Layer<S> for ViaAuthLayer {
    type Service = ViaAuth<S>;

    fn layer(&self, inner: S) -> Self::Service {
        ViaAuth {
            inner,
            auth_assure: self.auth_assure.clone(),
            auth_create: self.auth_create.clone(),
            token_create: self.token_create.clone(),
        }
    }
}

#[derive(Clone)]
pub(crate) struct ViaAuth<S> {
    inner: S,
    auth_assure: AuthAssure<StoreError>,
    auth_create: AuthCreate<StoreError>,
    token_create: TokenCreate<StoreError>,
}

impl<S> Service<Requisite<AuthResponse>> for ViaAuth<S>
where
    S: Service<Requisite<EntryRecord>> + Clone + Send + 'static,
    ComposeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ComposeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.auth_assure.poll_ready(cx))?;
        ready!(self.auth_create.poll_ready(cx))?;
        ready!(self.token_create.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, requisite: Requisite<AuthResponse>) -> Self::Future {
        let auth_assure = self.auth_assure.clone();
        let mut auth_assure = std::mem::replace(&mut self.auth_assure, auth_assure);

        let token_create = self.token_create.clone();
        let mut token_create = std::mem::replace(&mut self.token_create, token_create);

        let auth_create = self.auth_create.clone();
        let mut auth_create = std::mem::replace(&mut self.auth_create, auth_create);

        let inner = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, inner);

        async move {
            let RequisiteView {
                view:
                    AuthResponse {
                        auth_user:
                            AuthUser {
                                id,
                                name,
                                email,
                                picture,
                            },
                        provider,
                    },
                requisite,
            } = requisite.pre_req();

            let secret = match auth_assure
                .call(ops::Assure(Request::world(auth::Identify::Email(
                    email.clone(),
                ))))
                .await
            {
                Ok(secret) => Ok(secret.request),
                Err(StoreError::NotFound) => {
                    let token = token_create
                        .call(ops::Create(Request::world(token::Inclusive {})))
                        .await?
                        .request;

                    let secret = token.secret;

                    let auth = auth_create
                        .call(ops::Create(Request::world(auth::Inclusive {
                            name: name.clone(),
                            secret,
                            id,
                            email,
                            picture,
                            provider,
                        })))
                        .await?
                        .request;

                    let secret = auth.secret;

                    Ok(secret)
                }
                Err(e) => Err(ComposeError::Store(e)),
            }?;

            let requisite = requisite.with_pre(EntryRecord { name, secret });

            inner.call(requisite).await.map_err(Into::into)
        }
        .boxed()
    }
}
