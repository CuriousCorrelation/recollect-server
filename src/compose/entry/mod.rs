pub(crate) mod via_auth;
pub(crate) mod via_document_measure;
pub(crate) mod via_entity;
pub(crate) mod via_entity_measure;
pub(crate) mod via_id_handle;
pub(crate) mod via_member;
pub(crate) mod via_requester;
pub(crate) mod via_session;
pub(crate) mod via_team;
pub(crate) mod via_team_measure;

pub(crate) use std::task::{ready, Context, Poll};

use tower::Service;
use typed_builder::TypedBuilder;

use crate::model::{
    auth::AuthResponse,
    requisite::{self, Requisite},
    session::Session,
    BoxFuture,
};

use self::{
    via_auth::ViaAuth, via_document_measure::ViaDocumentMeasure, via_entity::ViaEntity,
    via_entity_measure::ViaEntityMeasure, via_member::ViaMember, via_requester::ViaRequester,
    via_session::ViaSession, via_team::ViaTeam, via_team_measure::ViaTeamMeasure,
};

use super::error::ComposeError;

#[derive(TypedBuilder, Clone)]
pub(crate) struct Entry {
    inner: ViaAuth<
        ViaEntity<
            ViaTeam<
                ViaMember<
                    ViaRequester<ViaEntityMeasure<ViaTeamMeasure<ViaDocumentMeasure<ViaSession>>>>,
                >,
            >,
        >,
    >,
}

impl Service<Requisite<AuthResponse>> for Entry {
    type Response = Session;

    type Error = ComposeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, requisite: Requisite<AuthResponse>) -> Self::Future {
        self.inner.call(requisite)
    }
}
