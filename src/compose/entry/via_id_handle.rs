use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::Service;
use typed_builder::TypedBuilder;

use crate::{
    compose::error::ComposeError,
    model::{
        namespace, object, ops,
        service::{NamespaceCreate, ObjectCreate},
        BoxFuture, Handle, PrimaryKey, Request,
    },
    store::error::StoreError,
};

#[derive(Clone, TypedBuilder)]
pub(crate) struct ViaIdHandle {
    object_create: ObjectCreate<StoreError>,
    namespace_create: NamespaceCreate<StoreError>,
}

impl Service<()> for ViaIdHandle {
    type Response = (PrimaryKey, Handle);

    type Error = ComposeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.object_create.poll_ready(cx))?;
        ready!(self.namespace_create.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, _req: ()) -> Self::Future {
        let namespace_create = self.namespace_create.clone();
        let mut namespace_create = std::mem::replace(&mut self.namespace_create, namespace_create);

        let object_create = self.object_create.clone();
        let mut object_create = std::mem::replace(&mut self.object_create, object_create);

        async move {
            let object = object_create
                .call(ops::Create(Request::world(object::Inclusive {})))
                .await?
                .request;

            let id = object.id;

            let namespace = namespace_create
                .call(ops::Create(Request::world(namespace::Inclusive {})))
                .await?
                .request;

            let handle = namespace.handle;

            Ok((id, handle))
        }
        .boxed()
    }
}
