use thiserror::Error;

use crate::{auth, error, exchange, service, store};

#[derive(Debug, Error)]
pub(crate) enum ComposeError {
    #[error("Access violation - {0}")]
    AccessViolation(error::AccessViolation),

    #[error("Auth - {0}")]
    Auth(#[from] auth::error::AuthError),

    #[error("Service - {0}")]
    Service(#[from] service::error::ServiceError),

    #[error("Exchange - {0}")]
    Exchange(#[from] exchange::error::ExchangeError),

    #[error("Store - {0}")]
    Store(#[from] store::error::StoreError),

    #[error("Missing requisite")]
    MissingRequisite,
}

pub(super) type ComposeResult<T> = std::result::Result<T, ComposeError>;
