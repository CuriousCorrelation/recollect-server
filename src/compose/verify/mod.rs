pub(crate) mod verify_entity;
pub(crate) mod verify_requester;
pub(crate) mod verify_session;
pub(crate) mod verify_who;

use std::{
    convert::Infallible,
    task::{ready, Context, Poll},
};

use axum::{
    extract::{rejection::FailedToDeserializeQueryString, Query},
    Extension, RequestExt,
};
use axum_client_ip::SecureClientIp;
use futures::FutureExt;
use http::Uri;
use tower::{BoxError, Layer, Service};
use tracing::info;
use typed_builder::TypedBuilder;

use crate::model::{
    connection_info::ConnectionInfo,
    requisite::{self, EmptyRequisite, Requisite, RequisiteView},
    who::Who,
    BoxFuture, Request,
};

use self::{
    verify_entity::VerifyEntity, verify_requester::VerifyRequester, verify_session::VerifySession,
    verify_who::VerifyWho,
};

use super::error::ComposeError;

#[derive(Clone, TypedBuilder)]
pub(crate) struct VerifyLayer {
    verify: VerifySession<VerifyRequester<VerifyEntity<VerifyWho>>>,
}

impl<S> Layer<S> for VerifyLayer {
    type Service = Verify<S>;

    fn layer(&self, inner: S) -> Self::Service {
        Verify {
            inner,
            verify: self.verify.clone(),
        }
    }
}

#[derive(Clone, TypedBuilder)]
pub(crate) struct Verify<S> {
    inner: S,
    verify: VerifySession<VerifyRequester<VerifyEntity<VerifyWho>>>,
}

impl<S, B> Service<http::Request<B>> for Verify<S>
where
    S: Service<http::Request<B>> + Clone + Send + 'static,
    S::Error: Into<BoxError>,
    S::Future: Send,
    B: Send + 'static,
{
    type Response = S::Response;
    type Error = BoxError;
    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx).map_err(Into::into))?;
        ready!(self.verify.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, mut req: http::Request<B>) -> Self::Future {
        let verify = self.verify.clone();
        let mut verify = std::mem::replace(&mut self.verify, verify);

        let inner = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, inner);

        async move {
            let Extension(requisite) = req
                .extract_parts()
                .await
                .expect("`Requisite` extension missing. Required by `VerifyLayer`, provided by `RequisiteLayer`");

            info!(requisite = ?requisite);

            let who = verify.call(requisite).await?;

            info!(who = ?who);

            req.extensions_mut().insert(who);

            inner.call(req).await.map_err(Into::into)
        }
        .boxed()
    }
}
