use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::Service;
use typed_builder::TypedBuilder;

use crate::{
    compose::error::ComposeError,
    model::{
        entity::EntityId,
        ops, preferred_team,
        requisite::{Requisite, RequisiteView},
        service::PreferredTeamFetch,
        who::Who,
        BoxFuture, PrimaryKey, Request,
    },
    store::error::StoreError,
};

#[derive(Clone, TypedBuilder)]
pub(crate) struct VerifyWho {
    preferred_team_fetch: PreferredTeamFetch<StoreError>,
}

impl Service<Requisite<EntityId>> for VerifyWho {
    type Response = Who;

    type Error = ComposeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.preferred_team_fetch.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, requisite: Requisite<EntityId>) -> Self::Future {
        let preferred_team_fetch = self.preferred_team_fetch.clone();
        let mut preferred_team_fetch =
            std::mem::replace(&mut self.preferred_team_fetch, preferred_team_fetch);

        async move {
            let RequisiteView {
                view: EntityId(entity_id),
                requisite,
            } = requisite.pre_req();

            let preferred_team = preferred_team_fetch
                .call(ops::Fetch(Request::world(
                    preferred_team::Identify::EntityId(entity_id),
                )))
                .await?
                .request;

            let who = Who {
                entity_id: preferred_team.entity_id,
                team_id: preferred_team.team_id,
            };

            Ok(who)
        }
        .boxed()
    }
}
