use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service};
use typed_builder::TypedBuilder;

use crate::{
    compose::error::ComposeError,
    error::AccessViolation,
    model::{
        api_request::ApiRequest,
        ops,
        requisite::{EmptyRequisite, Requisite},
        service::SessionFetch,
        session, BoxFuture, Request, Secret,
    },
    store::error::StoreError,
};

#[derive(Clone, TypedBuilder)]
pub(crate) struct VerifySessionLayer {
    session_fetch: SessionFetch<StoreError>,
}

impl<S> Layer<S> for VerifySessionLayer {
    type Service = VerifySession<S>;

    fn layer(&self, inner: S) -> Self::Service {
        VerifySession {
            inner,
            session_fetch: self.session_fetch.clone(),
        }
    }
}

#[derive(Clone)]
pub(crate) struct VerifySession<S> {
    inner: S,
    session_fetch: SessionFetch<StoreError>,
}

impl<S> Service<EmptyRequisite> for VerifySession<S>
where
    S: Service<Requisite<Secret>> + Clone + Send + 'static,
    ComposeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ComposeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.session_fetch.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, requisite: EmptyRequisite) -> Self::Future {
        let session_fetch = self.session_fetch.clone();
        let mut session_fetch = std::mem::replace(&mut self.session_fetch, session_fetch);

        let inner = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, inner);

        async move {
            let secret = {
                let session_id = match requisite.api_request() {
                    ApiRequest::Identity(identity) => Secret::from_str_truncate(identity),
                    ApiRequest::ApiKey(api_key) => Secret::from_str_truncate(api_key),
                    ApiRequest::UnknownApiAccessor => {
                        return Err(ComposeError::AccessViolation(AccessViolation::Unauthorized))
                    }
                };

                // CAUTION: Using `Request::world`.
                let session = session_fetch
                    .call(ops::Fetch(Request::world(session::Identify::Id(
                        session_id,
                    ))))
                    .await?
                    .request;

                let secret = session.secret;

                Ok(secret)
            }?;

            let requisite = requisite.with_pre(secret);

            inner.call(requisite).await.map_err(Into::into)
        }
        .boxed()
    }
}
