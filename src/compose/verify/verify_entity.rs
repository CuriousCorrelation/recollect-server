use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service};
use typed_builder::TypedBuilder;

use crate::{
    compose::error::ComposeError,
    model::{
        entity::{self, EntityId},
        ops,
        requisite::{Requisite, RequisiteView},
        service::EntityAssure,
        BoxFuture, PrimaryKey, Request, Secret,
    },
    store::error::StoreError,
};

#[derive(Clone, TypedBuilder)]
pub(crate) struct VerifyEntityLayer {
    entity_assure: EntityAssure<StoreError>,
}

impl<S> Layer<S> for VerifyEntityLayer {
    type Service = VerifyEntity<S>;

    fn layer(&self, inner: S) -> Self::Service {
        VerifyEntity {
            inner,
            entity_assure: self.entity_assure.clone(),
        }
    }
}

#[derive(Clone)]
pub(crate) struct VerifyEntity<S> {
    inner: S,
    entity_assure: EntityAssure<StoreError>,
}

impl<S> Service<Requisite<Secret>> for VerifyEntity<S>
where
    S: Service<Requisite<EntityId>> + Clone + Send + 'static,
    ComposeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ComposeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.entity_assure.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, requisite: Requisite<Secret>) -> Self::Future {
        let entity_assure = self.entity_assure.clone();
        let mut entity_assure = std::mem::replace(&mut self.entity_assure, entity_assure);

        let inner = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, inner);

        async move {
            let RequisiteView {
                view: secret,
                requisite,
            } = requisite.pre_req();

            let entity_id = entity_assure
                .call(ops::Assure(Request::world(entity::Identify::Secret(
                    secret,
                ))))
                .await?
                .request;

            let requisite = requisite.with_pre(EntityId(entity_id));

            inner.call(requisite).await.map_err(Into::into)
        }
        .boxed()
    }
}
