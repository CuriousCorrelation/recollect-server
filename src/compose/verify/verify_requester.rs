use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::{Layer, Service};
use typed_builder::TypedBuilder;

use crate::{
    compose::error::ComposeError,
    error::AccessViolation,
    model::{
        connection_info::ConnectionInfo,
        ops, requester,
        requisite::{Requisite, RequisiteView},
        service::RequesterFetch,
        BoxFuture, Request, Secret,
    },
    store::error::StoreError,
};

#[derive(Clone, TypedBuilder)]
pub(crate) struct VerifyRequesterLayer {
    requester_fetch: RequesterFetch<StoreError>,
}

impl<S> Layer<S> for VerifyRequesterLayer {
    type Service = VerifyRequester<S>;

    fn layer(&self, inner: S) -> Self::Service {
        VerifyRequester {
            inner,
            requester_fetch: self.requester_fetch.clone(),
        }
    }
}

#[derive(Clone)]
pub(crate) struct VerifyRequester<S> {
    inner: S,
    requester_fetch: RequesterFetch<StoreError>,
}

impl<S> Service<Requisite<Secret>> for VerifyRequester<S>
where
    S: Service<Requisite<Secret>> + Clone + Send + 'static,
    ComposeError: From<S::Error>,
    S::Future: Send,
{
    type Response = S::Response;

    type Error = ComposeError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.requester_fetch.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, requisite: Requisite<Secret>) -> Self::Future {
        let requester_fetch = self.requester_fetch.clone();
        let mut requester_fetch = std::mem::replace(&mut self.requester_fetch, requester_fetch);

        let inner = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, inner);

        async move {
            let RequisiteView {
                view: secret,
                requisite,
            } = requisite.pre_req();

            let requester = requester_fetch
                .call(ops::Fetch(Request::world(requester::Identify::Secret(
                    secret,
                ))))
                .await?
                .request;

            let ConnectionInfo {
                host,
                scheme,
                remote_addr,
            } = requisite.connection_info();

            // Ask for login if any of these info has changed over the short session duration.
            if requester.host == host
                && requester.scheme == scheme
                && requester.remote_addr == remote_addr
                // Also make sure `secret` match.
                && requester.secret == secret
            {
                let requisite = requisite.with_pre(secret);

                inner.call(requisite).await.map_err(Into::into)
            } else {
                Err(ComposeError::AccessViolation(AccessViolation::Unauthorized))
            }
        }
        .boxed()
    }
}
