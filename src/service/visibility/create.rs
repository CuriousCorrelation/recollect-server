use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::Service;

use crate::{
    exchange,
    model::{external, BoxFuture, Request},
    service::{audit, error::ServiceError},
    store,
};

struct Create {
    create: audit::Create<exchange::visibility::Create<store::Postgres>>,
    exchange: exchange::visibility::Exchange,
}

impl Service<Request<external::visibility::Inclusive>> for Create {
    type Response = external::visibility::Visibility;

    type Error = ServiceError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.exchange.poll_ready(cx))?;
        ready!(self.create.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: Request<external::visibility::Inclusive>) -> Self::Future {
        let create = self.create.clone();
        let mut create = std::mem::replace(&mut self.create, create);

        let exchange = self.exchange.clone();
        let mut exchange = std::mem::replace(&mut self.exchange, exchange);

        async move {
            exchange
                .call(create.call(req).await?)
                .await
                .map_err(ServiceError::Exchange)
        }
        .boxed()
    }
}
