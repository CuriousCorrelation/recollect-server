use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::Service;
use typed_builder::TypedBuilder;

use crate::{
    exchange,
    model::{external, BoxFuture, Request},
    service::{audit, error::ServiceError},
    store,
};

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct Fetch {
    fetch: audit::Create<exchange::document::Fetch<store::Postgres>>,
    exchange: exchange::document::Exchange,
}

impl Service<Request<external::document::Identify>> for Fetch {
    type Response = external::document::Document;

    type Error = ServiceError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.exchange.poll_ready(cx))?;
        ready!(self.fetch.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: Request<external::document::Identify>) -> Self::Future {
        let fetch = self.fetch.clone();
        let mut fetch = std::mem::replace(&mut self.fetch, fetch);

        let exchange = self.exchange.clone();
        let mut exchange = std::mem::replace(&mut self.exchange, exchange);

        async move {
            exchange
                .call(fetch.call(req).await?)
                .await
                .map_err(ServiceError::Exchange)
        }
        .boxed()
    }
}
