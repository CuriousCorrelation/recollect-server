use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::Service;
use typed_builder::TypedBuilder;

use crate::{
    exchange,
    model::{external, BoxFuture, Request},
    service::{audit, error::ServiceError},
    store,
};

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct Create {
    create: audit::Create<exchange::document::Create<store::Postgres>>,
    exchange: exchange::document::Exchange,
}

impl Service<Request<external::document::Inclusive>> for Create {
    type Response = external::document::Document;

    type Error = ServiceError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.exchange.poll_ready(cx))?;
        ready!(self.create.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: Request<external::document::Inclusive>) -> Self::Future {
        let create = self.create.clone();
        let mut create = std::mem::replace(&mut self.create, create);

        let exchange = self.exchange.clone();
        let mut exchange = std::mem::replace(&mut self.exchange, exchange);

        async move {
            exchange
                .call(create.call(req).await?)
                .await
                .map_err(ServiceError::Exchange)
        }
        .boxed()
    }
}
