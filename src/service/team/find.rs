use std::task::{ready, Context, Poll};

use futures::{future::try_join_all, FutureExt};
use tower::Service;
use typed_builder::TypedBuilder;

use crate::{
    exchange,
    model::{external, BoxFuture, Request},
    service::{audit, error::ServiceError},
    store,
};

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct Find {
    find: audit::Create<exchange::team::Find<store::Postgres>>,
    exchange: exchange::team::Exchange,
}

impl Service<Request<external::team::Partial>> for Find {
    type Response = Vec<external::team::Team>;

    type Error = ServiceError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.exchange.poll_ready(cx))?;
        ready!(self.find.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: Request<external::team::Partial>) -> Self::Future {
        let find = self.find.clone();
        let mut find = std::mem::replace(&mut self.find, find);

        let exchange = self.exchange.clone();
        let mut exchange = std::mem::replace(&mut self.exchange, exchange);

        async move {
            let Request { who, request } = find.call(req).await?;

            let reponse = try_join_all({
                request.into_iter().map(|request| {
                    let mut exchange = exchange.clone();

                    async move {
                        exchange
                            .call(Request { who, request })
                            .await
                            .map_err(ServiceError::Exchange)
                    }
                })
            });

            reponse.await
        }
        .boxed()
    }
}
