use std::task::{ready, Poll};

use futures::FutureExt;
use tower::{Layer, Service, ServiceExt};
use tracing::warn;
use typed_builder::TypedBuilder;

use crate::{
    model::{
        audit::{self, InclusiveContext},
        internal_identifier::InternalIdentifier,
        ops,
        service::AuditCreate,
        service_context::ServiceContext,
        BoxFuture, Request,
    },
    store::error::StoreError,
};

#[derive(Debug, TypedBuilder)]
pub(crate) struct AuditLayer {
    audit_create: AuditCreate<StoreError>,
}

#[derive(Debug, Clone, TypedBuilder)]
pub(crate) struct Create<S> {
    inner: S,
    audit_context: InclusiveContext,
    audit_create: AuditCreate<StoreError>,
}

impl<S> Layer<S> for AuditLayer
where
    S: ServiceContext,
{
    type Service = Create<S>;

    fn layer(&self, inner: S) -> Self::Service {
        let audit_context = InclusiveContext::builder()
            .context(inner.context())
            .version(inner.version())
            .service_id(inner.service_id())
            .build();

        let audit_create = self.audit_create.clone();

        Self::Service::builder()
            .inner(inner)
            .audit_context(audit_context)
            .audit_create(audit_create)
            .build()
    }
}

impl<S, Req> Service<Request<Req>> for Create<S>
where
    Request<Req>: serde::Serialize,
    S: Service<Request<Req>>,
    S::Response: InternalIdentifier + Send,
    S::Error: serde::Serialize + Send,
    S::Future: Send + 'static,
{
    type Response = S::Response;

    type Error = S::Error;

    type Future = BoxFuture<S::Response, S::Error>;

    fn poll_ready(
        &mut self,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), Self::Error>> {
        ready!(self.inner.poll_ready(cx))?;
        ready!(self.audit_create.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: Request<Req>) -> Self::Future {
        let who = req.who;

        let InclusiveContext {
            context,
            version,
            service_id,
        } = self.audit_context;

        let audit = audit::Inclusive::builder()
            .context(context)
            .version(version)
            .service_id(service_id);

        let enc_req = serde_json::to_value(&req);
        let future = self.inner.call(req);

        let audit_create = self.audit_create.clone();
        let mut audit_create = std::mem::replace(&mut self.audit_create, audit_create);

        async move {
            let res = future.await;
            let enc_res = serde_json::to_value(res.as_ref().map(|r| r.internal_identifier()));

            if let Ok(enc_req) = enc_req {
                if let Ok(enc_res) = enc_res {
                    if audit_create
                        .call(ops::Create(Request::new(
                            who,
                            audit.source(enc_req).response(enc_res).build(),
                        )))
                        .await
                        .is_ok()
                    {
                        res
                    } else {
                        warn!("Audit entry using `audit_create` failed.");

                        res
                    }
                } else {
                    warn!("Failed to json encode `res` `id`");

                    res
                }
            } else {
                warn!("Failed to json encode `req`");

                res
            }
        }
        .boxed()
    }
}
