use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::Service;

use crate::{
    exchange,
    model::{external, BoxFuture, Request},
    service::{audit, error::ServiceError},
    store,
};

struct Fetch {
    fetch: audit::Create<exchange::preferred_team::Fetch<store::Postgres>>,
    exchange: exchange::preferred_team::Exchange,
}

impl Service<Request<external::preferred_team::Identify>> for Fetch {
    type Response = external::preferred_team::PreferredTeam;

    type Error = ServiceError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.exchange.poll_ready(cx))?;
        ready!(self.fetch.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: Request<external::preferred_team::Identify>) -> Self::Future {
        let fetch = self.fetch.clone();
        let mut fetch = std::mem::replace(&mut self.fetch, fetch);

        let exchange = self.exchange.clone();
        let mut exchange = std::mem::replace(&mut self.exchange, exchange);

        async move {
            exchange
                .call(fetch.call(req).await?)
                .await
                .map_err(ServiceError::Exchange)
        }
        .boxed()
    }
}
