use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::Service;

use crate::{
    exchange,
    model::{external, BoxFuture, Request},
    service::{audit, error::ServiceError},
    store,
};

struct Delete {
    delete: audit::Create<exchange::admin::Delete<store::Postgres>>,
    exchange: exchange::admin::Exchange,
}

impl Service<Request<external::admin::Identify>> for Delete {
    type Response = external::admin::Admin;

    type Error = ServiceError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.exchange.poll_ready(cx))?;
        ready!(self.delete.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: Request<external::admin::Identify>) -> Self::Future {
        let delete = self.delete.clone();
        let mut delete = std::mem::replace(&mut self.delete, delete);

        let exchange = self.exchange.clone();
        let mut exchange = std::mem::replace(&mut self.exchange, exchange);

        async move {
            exchange
                .call(delete.call(req).await?)
                .await
                .map_err(ServiceError::Exchange)
        }
        .boxed()
    }
}
