use std::task::{ready, Context, Poll};

use futures::FutureExt;
use tower::Service;

use crate::{
    exchange,
    model::{external, BoxFuture, Request},
    service::{audit, error::ServiceError},
    store,
};

struct Update {
    update: audit::Create<exchange::blockade::Update<store::Postgres>>,
    exchange: exchange::blockade::Exchange,
}

impl Service<Request<external::blockade::Exclusive>> for Update {
    type Response = external::blockade::Blockade;

    type Error = ServiceError;

    type Future = BoxFuture<Self::Response, Self::Error>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        ready!(self.exchange.poll_ready(cx))?;
        ready!(self.update.poll_ready(cx))?;

        Poll::Ready(Ok(()))
    }

    fn call(&mut self, req: Request<external::blockade::Exclusive>) -> Self::Future {
        let update = self.update.clone();
        let mut update = std::mem::replace(&mut self.update, update);

        let exchange = self.exchange.clone();
        let mut exchange = std::mem::replace(&mut self.exchange, exchange);

        async move {
            exchange
                .call(update.call(req).await?)
                .await
                .map_err(ServiceError::Exchange)
        }
        .boxed()
    }
}
