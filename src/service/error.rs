use serde::Serialize;
use thiserror::Error;

use crate::{exchange::error::ExchangeError, store::error::StoreError};

#[derive(Debug, Serialize, Error)]
pub(crate) enum ServiceError {
    #[error("Store - {0}")]
    Store(#[from] StoreError),

    #[error("Exchange - {0}")]
    Exchange(#[from] ExchangeError),

    #[error("SerDe - {0}")]
    #[serde(serialize_with = "serde_json_error")]
    Serde(#[from] serde_json::Error),
}

fn serde_json_error<S>(serde: &serde_json::Error, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    serializer.serialize_str(&serde.to_string())
}

pub(super) type ServiceResult<T> = std::result::Result<T, ServiceError>;
