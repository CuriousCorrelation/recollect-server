# Recollect Server

Recollect, a WYSIWYG collaboration and productivity engine

## Start up docker

``` sh
sudo rc-service docker start
```

## Load `.env` variables

``` sh
set -a && source .env
```

## Clean start docker containers

``` sh
docker-compose down && sudo rm -rf postgres-data && docker-compose up -d
```

## Run migration

``` sh
cd server
diesel migration run
```
