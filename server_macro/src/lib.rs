use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, Data, DeriveInput};

#[proc_macro_derive(ServiceContext)]
pub fn service_context_derive(input: TokenStream) -> TokenStream {
    let ast: DeriveInput = parse_macro_input!(input);

    let struct_name = &ast.ident;

    let generics = &ast.generics;

    let trait_import = quote! {
        use crate::model::service_context::ServiceContext;
    };

    // Generate the implementation
    let expanded = match &ast.data {
        Data::Struct(_) => {
            quote! {
                #trait_import

                impl #generics ServiceContext for #struct_name #generics {
                    fn service_id(&self) -> uuid::Uuid {
                        self.id
                    }

                    fn version(&self) -> AuditField {
                        self.version.clone()
                    }

                    fn context(&self) -> AuditField {
                        self.context.clone()
                    }
                }
            }
        }
        _ => {
            quote! {}
        }
    };

    TokenStream::from(expanded)
}
